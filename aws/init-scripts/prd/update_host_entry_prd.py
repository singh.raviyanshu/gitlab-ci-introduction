############################################################
# init script for datalake02 in prod environment
# Updates the host file on databricks nodes to send
# ADLS traffic over private link.

import socket

lookup_result = socket.gethostbyname_ex('datalake02.heb-dsol-core.hebdigital-prd.com') ### The endpoint hostname
addresses = lookup_result[2]
  
f = open("/etc/hosts", "a")
f.write('\n')
print('Adding these entries to /etc/hosts:')
for address in addresses:
  print(address + ' hebdatalake02.dfs.core.windows.net')
  f.write(address + ' hebdatalake02.dfs.core.windows.net\n')
f.close()
