// Databricks notebook source
// MAGIC %python
// MAGIC # Grant Cancel, view permissions on job run
// MAGIC dbutils.library.installPyPI('databricks_cli')
// MAGIC from databricks_cli.sdk.api_client import ApiClient
// MAGIC import json
// MAGIC apiclient = ApiClient(token = dbutils.entry_point.getDbutils().notebook().getContext().apiToken().get(),
// MAGIC                    host = 'https://southcentralus.azuredatabricks.net')
// MAGIC cluster_id = spark.conf.get("spark.databricks.clusterUsageTags.clusterId")
// MAGIC notebook_info = json.loads(dbutils.notebook.entry_point.getDbutils().notebook().getContext().toJson())
// MAGIC jobId = notebook_info["tags"]["jobId"]
// MAGIC
// MAGIC apiclient.perform_query("PATCH","/preview/permissions/clusters/"+cluster_id,{
// MAGIC   "access_control_list":
// MAGIC         {"group_name": "AZ-Production-SP-PromoFcst",
// MAGIC          "permission_level": "CAN_MANAGE"
// MAGIC         }
// MAGIC     })
// MAGIC
// MAGIC apiclient.perform_query("PATCH","/preview/permissions/jobs/"+jobId,{
// MAGIC   "access_control_list":
// MAGIC         {"group_name": "AZ-Production-SP-PromoFcst",
// MAGIC          "permission_level": "CAN_MANAGE_RUN"
// MAGIC         }
// MAGIC     })

// COMMAND ----------

import spark.implicits._
import java.text.SimpleDateFormat
import org.joda.time.DateTime
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats

val configBasePath = "dbfs:/"
val promoPath="/pfp"

val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")
/*
Reading the env from env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
if(currentEnv.size == 0){ 
  print("Required environment not available")
  throw new Exception("Required environment not available")
}
else if(currentEnv.size > 1) {
  print("More than one environment detected")
  throw new Exception("More than one environment detected")
}
else currentEnv(0)
}

val storageConfigFile = configBasePath+env+promoPath+"/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))
val adlsPrefix = (storageJson\"adlsPrefix").extract[String]

val fd=spark.read.option("header","true").csv(adlsPrefix+ "/" + env + promoPath + "/conf/forecast_domain.csv")
val sdf=new SimpleDateFormat("yyyy-MM-dd")
var outputString=""
fd.take(fd.count.toInt).map(r=>{
  val d1=sdf.format(new java.util.Date(new DateTime(sdf.parse(r.getString(0)).getTime).plusDays(7).getMillis))
  val d2=sdf.format(new java.util.Date(new DateTime(sdf.parse(r.getString(1)).getTime).plusDays(7).getMillis))
  val d3=r.getString(2)
  outputString=d1+","+d2+","+d3
})
dbutils.fs.rm(adlsPrefix+ "/" + env + promoPath + "/conf/forecast_domain.csv")
dbutils.fs.put(adlsPrefix+ "/" + env + promoPath + "/conf/forecast_domain.csv","hist_end,fcst_start,fcst_duration\n"+outputString)
val fd1=spark.read.option("header","true").csv(adlsPrefix+ "/" + env + promoPath +"/conf/forecast_domain.csv")
fd1.show
