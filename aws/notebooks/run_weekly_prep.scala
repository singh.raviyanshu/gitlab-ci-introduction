// Databricks notebook source
// MAGIC %python
// MAGIC ###########################################################################
// MAGIC ## Writing data to directories with datetime stamps
// MAGIC ## This prevents us from:
// MAGIC ## -deleting files in DBFS during weekly prep
// MAGIC ## -archiving R data which has been a point of failure a couple times
// MAGIC ###########################################################################
// MAGIC
// MAGIC import datetime
// MAGIC import json
// MAGIC
// MAGIC # get env
// MAGIC notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
// MAGIC pathParts = notebookPath.split("/")
// MAGIC
// MAGIC def readJSONFile(pt):
// MAGIC   with open(pt) as f:
// MAGIC     o = json.load(f)
// MAGIC   return o
// MAGIC
// MAGIC # Reading env from env_conf.json
// MAGIC envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
// MAGIC envJson = readJSONFile(envConfigFile)
// MAGIC environments = envJson['env']
// MAGIC currentEnv = [env for env in environments if env in pathParts]
// MAGIC if len(currentEnv) == 0:
// MAGIC   print("Required environment not available")
// MAGIC   raise ValueError("Required environment not available")
// MAGIC elif len(currentEnv) > 1:
// MAGIC   print("More than one environment detected")
// MAGIC   raise ValueError("More than one environment detected")
// MAGIC else: env = currentEnv[0]
// MAGIC print(env)
// MAGIC
// MAGIC # Get the adlsPrefix
// MAGIC
// MAGIC configBasePath = "/dbfs/"
// MAGIC promoPath="/pfp"
// MAGIC
// MAGIC xdatetag = datetime.date.today().strftime("%Y%m%d")
// MAGIC
// MAGIC dbutils.fs.rm('/'+ env+promoPath+"/r_run/conf/conf_weekly_all.json")
// MAGIC storageConfigFile = readJSONFile(configBasePath+env+promoPath+'/conf/conf_storage_prefix.json')
// MAGIC adlsPath = storageConfigFile['adlsPrefix']+"/"+env+promoPath+"/"
// MAGIC
// MAGIC dbutils.fs.cp(adlsPath+"/conf/conf_weekly_all.json","/"+env+promoPath+"/r_run/conf/conf_weekly_all.json")
// MAGIC weeklyConf = readJSONFile(configBasePath+env+promoPath+'/r_run/conf/conf_weekly_all.json')
// MAGIC rconf = readJSONFile(configBasePath+env+promoPath+'/r_run/conf/r_forecasting_conf.json')
// MAGIC dbutils.fs.rm('/'+env+promoPath+'/r_run/conf/r_forecasting_conf.json')
// MAGIC weeklyConf['prepareRDataPrepPath']=weeklyConf['prepareRDataPrepPath'][0:len(weeklyConf['prepareRDataPrepPath'])-10]+"_"+xdatetag+"/"
// MAGIC rconf['fcst_local_path']=rconf['fcst_local_path'][0:len(rconf['fcst_local_path'])-10]+"_"+xdatetag+"/"
// MAGIC rconf['tmp_local_path']="/dbfs"+weeklyConf['prepareRDataPrepPath']
// MAGIC rconf['userpath']="/dbfs"+weeklyConf['prepareRDataPrepPath']
// MAGIC
// MAGIC with open(configBasePath+env+promoPath+'/r_run/conf/conf_weekly_all.json', 'w') as outfile:
// MAGIC   outfile.write(json.dumps(weeklyConf, indent=4, sort_keys=True))
// MAGIC with open(configBasePath+env+promoPath+'/r_run/conf/r_forecasting_conf.json', 'w') as outfile:
// MAGIC   outfile.write(json.dumps(rconf, indent=4, sort_keys=True))
// MAGIC
// MAGIC dbutils.fs.rm(adlsPath+"/conf/conf_weekly_all.json")
// MAGIC dbutils.fs.cp("/"+env+promoPath+"/r_run/conf/conf_weekly_all.json",adlsPath+"/conf/")
// MAGIC
// MAGIC cat_partition_result = dbutils.notebook.run("./generate_cat_partition",7*24*24,{"prepareRDataPrepPath":str(weeklyConf['prepareRDataPrepPath']),"adlsEnv":(adlsPath +"/all_data/")})
// MAGIC
// MAGIC if cat_partition_result != "0":
// MAGIC   raise ValueError("Error generating cat_partition.csv")
// MAGIC # Has happened twice now that files are not written
// MAGIC # so check to make sure they are written
// MAGIC
// MAGIC categories = spark.read.option("header",True).option("sep","|").csv(str(weeklyConf['prepareRDataPrepPath'])+"cat_partition.csv").count()
// MAGIC categories_final = spark.read.option("header",True).option("sep","|").csv(str(weeklyConf['prepareRDataPrepPath'])+"cat_partition_final.csv").count()
// MAGIC print(str(categories) + " " + str(categories_final))

// COMMAND ----------

import com.databricks.backend.daemon.dbutils.FileInfo
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats
import scala.io.Source
import org.apache.spark.sql.types._
import org.apache.log4j.{Level, Logger, PropertyConfigurator}
import scala.util.{Success, Failure, Try}
//import scala.concurrent._
//import scala.concurrent.duration._
//import scala.concurrent.ExecutionContext.Implicits.global
//import cats.implicits._

/**

e131988@heb.com
This notebook:
-runs WeeklyPrep to prepare data for r_forecasting PFM code
-removes files in global_static dir
-then copies most recent files from ADLS global_static

Removes existing RShiny images (to prevent .RDataTmp to .RData overwrite errors from occurring)

 **/
val start=System.currentTimeMillis()
private val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
private val LOGGER = Logger.getLogger(s"notebook.$NOTEBOOK_NAME")

val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")

val configBasePath = "dbfs:/"
val promoPath="/pfp"
/*
Reading the env from env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
println(environments)
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
	if(currentEnv.size == 0){
		LOGGER.error("Required environment not available")
		throw new Exception("Required environment not available")
	}
	else if(currentEnv.size > 1) {
		LOGGER.error("More than one environment detected")
		throw new Exception("More than one environment detected")
	}
	else currentEnv(0)
}

// Get the adlsPrefix
val storageConfigFile = configBasePath+env+promoPath+"/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))
val adlsPrefix = (storageJson\"adlsPrefix").extract[String]

var c=""
val configFile=adlsPrefix+"/"+env+promoPath+"/conf/conf_weekly_all.json"
var json: JValue = parse(dbutils.fs.head(configFile))

private val log4jPath = (json\"log4jPath").extract[String]

Try {
	PropertyConfigurator.configure(log4jPath)
} match {
	case Success(_) => LOGGER.info(s"Using Log configuration file from dbfs location: $log4jPath for the installed library.")
	case Failure(_) => LOGGER.warn("No Log Configuration file found.")
}

LOGGER.info("Running Weekly Preparation.")

if(dbutils.fs.ls(adlsPrefix+"/"+env+promoPath+"/all_data/").length<4) {
	LOGGER.error("Some directories may be missing, please check...")
	dbutils.notebook.exit("Some directories may be missing, please check...")
}

// Argument parsing vals
LOGGER.info("Parsing Argument values.")
val argsToKeys=Map("userpath"->"userpath","database"->"database","fcstdomainpartitions"->"fcstDomainPartitions","loglevel"->"logLevel","sleeptime"->"sleepTime","maxproductcount"->"max_product_count","maxpromostartdate"->"max_promo_start_date","printSchemas"->"printSchemas","generateDotFiles"->"generateDotFiles","dotFilesPath"->"dotFilesPath","weeksToExport"->"weeksToExport","zone"->"zone","clean"->"clean","overwrite"->"overwrite","steps"->"steps","stage"->"stage","globalstaticdir"->"global_static","fcstdomain"->"forecast_domain","database"->"database","prepareRDataPrepPath"->"prepareRDataPrepPath")
var dirs=Map("staticdir"->"static/","mergedir"->"merge_all/","streammergedir"->"stream_merge_all/","filterdir"->"merge_all/","aggdir"->"agg_all/","fcstdir"->"fcst_all/","preparerfcstdatadir"->"prepare_fcst_R_all/","fmdatadir"->"fm_ui/","fcstoverridedir"->"fcst_override/","preparerdataprep"->"prepareRDataPrep_all/", "pdsExportDir"->"pds_ui/")
val updatedDirs=scala.collection.mutable.Map[String,String]()
val userpath=(json\argsToKeys("userpath")).extractOrElse("")
val zone=(json\argsToKeys("zone")).extractOrElse("")
val globalStatic=(json\argsToKeys("globalstaticdir")).extractOrElse("")
val forecastDomain=(json\argsToKeys("fcstdomain")).extractOrElse("")
val thePath=(json\argsToKeys("prepareRDataPrepPath")).extractOrElse("")
var filtering=false
val deleteFiles=true
val deleteRFiles=true

dbutils.fs.mkdirs(thePath)

//  Deletes to be safe and prevent 403 or timeout errors during spark overwrite
LOGGER.info("Deleting files to be safe and prevent 403 or timeout errors during spark overwrite.")
if(deleteFiles){
	scala.util.Try {
		LOGGER.debug(s"Removing files from:  $thePath")
		for(c <- dbutils.fs.ls(thePath)) {
			if(!c.name.contains("cat_partition")) {
				LOGGER.debug(s"Removing:  ${c.name}")
				dbutils.fs.rm(c.path,true)
			}
		}
	} recover {
		case e => LOGGER.warn("File doesn't exist...")
	}
}


if(!userpath.contains(env)) {
	LOGGER.error(s"$env not detected in $userpath")
	dbutils.notebook.exit(env + " not detected in "+userpath)
}

if(!argsToKeys.contains("userpath")) {
	LOGGER.error(s"Please set userpath in $configFile")
	dbutils.notebook.exit("Please set userpath in "+configFile)
}

for((k,v)<-dirs){
	updatedDirs += (k -> (userpath+v))
}
if(argsToKeys.contains("zone")&&zone.equals("")){
	LOGGER.debug("Running on full data")
}else if(argsToKeys.contains("zone") && !zone.equals("")){
	LOGGER.debug("Filtering on zone")
	filtering=true
	updatedDirs += ("filterdir" -> (userpath+"merge_zone"+zone+"/"))
	updatedDirs += ("aggdir" -> (userpath+"agg_zone"+zone+"/"))
	updatedDirs += ("fcstdir" -> (userpath+"fcst_zone"+zone+"/"))
	updatedDirs += ("preparerfcstdatadir" -> (userpath+"prepare_fcst_R_zone"+zone+"/"))
	updatedDirs += ("fmdatadir" -> (userpath+"fm_ui_zone"+zone+"/"))
}
updatedDirs += ("preparerdataprep" -> thePath)

// copy static files to userpath
LOGGER.info(s"Copying static files to $userpath.")
c=""
for(c <- Array("BAD_COUPON_FIXED.csv","calendar.csv","categories_master.csv","region_to_dc.csv","threshold.csv","treatment_mapping.csv","mexico_stores.csv","valid_fmui_cat.csv","active_dept.csv")){
	dbutils.fs.rm(updatedDirs("staticdir")+c,true)
	dbutils.fs.cp(globalStatic+c,updatedDirs("staticdir"))
}
dbutils.fs.cp(globalStatic+"active_dept.csv",updatedDirs("preparerdataprep"))

// set right dir and copy forecast domain
LOGGER.info("Setting right dir and copy forecast domain.")
var rightDir=""
if(filtering){
	rightDir=updatedDirs("filterdir")
}else{
	rightDir=updatedDirs("mergedir")
}
dbutils.fs.rm(updatedDirs("filterdir")+"forecast_domain.csv",true)
dbutils.fs.cp(forecastDomain,updatedDirs("filterdir"))
dbutils.fs.cp(forecastDomain,updatedDirs("mergedir"))

//  copyStaticFilesToDirs
LOGGER.info("Copying static files to Directories.")
for(c <- Array("calendar.csv","treatment_mapping.csv","BAD_COUPON_FIXED.csv","region_to_dc.csv")) {
	LOGGER.debug(s"Copying ${updatedDirs("staticdir")}$c to $rightDir")
	dbutils.fs.rm(rightDir+c,true)
	dbutils.fs.cp(updatedDirs("staticdir")+c,rightDir)
}
for(c <- Array("categories_master.csv","mexico_stores.csv")) {
	LOGGER.debug(s"Copying ${updatedDirs("staticdir")}$c to ${updatedDirs("mergedir")}")
	dbutils.fs.rm(updatedDirs("mergedir")+c,true)
	dbutils.fs.cp(updatedDirs("staticdir")+c,updatedDirs("mergedir"))
}
for(c <- Array("threshold.csv","valid_fmui_cat.csv")) {
	LOGGER.debug(s"Copying ${updatedDirs("staticdir")}$c to ${updatedDirs("fmdatadir")}")
	dbutils.fs.rm(updatedDirs("fmdatadir")+c,true)
	dbutils.fs.cp(updatedDirs("staticdir")+c,updatedDirs("fmdatadir"))
}

//  construct batch args
LOGGER.info("Constructing Batch Arguments.")
var finalArgs="-batchName WeeklyBatch -mergeDir ".concat(updatedDirs("mergedir")).concat(" -pdsExportDir ").concat(updatedDirs("pdsExportDir")).concat(" -streamMergeDir ").concat(updatedDirs("streammergedir")).concat(" -aggDir ").concat(updatedDirs("aggdir")).concat(" -fcstDir ").concat(updatedDirs("fcstdir")).concat(" -prepareRFcstDataDir ").concat(updatedDirs("preparerfcstdatadir")).concat(" -fmDataDir ").concat(updatedDirs("fmdatadir")).concat(" -fcstOverrideDir ").concat(updatedDirs("fcstoverridedir")).concat(" -prepareRDataPrepDir ").concat(updatedDirs("preparerdataprep")).concat(" -staticDir ").concat(updatedDirs("staticdir"))
if(filtering) {
	finalArgs=finalArgs+" -filterDir "+updatedDirs("filterdir")+" -zone "+zone
}
if(!(json\argsToKeys("stage")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -stage "+(json\argsToKeys("stage")).extractOrElse("")
}
if((json\argsToKeys("overwrite")).extractOrElse("").equals("true")) {
	finalArgs=finalArgs+" -overwrite"
}
if(!(json\argsToKeys("steps")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -steps "+(json\argsToKeys("steps")).extractOrElse("")+" "
}
if(!(json\argsToKeys("database")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -database "+(json\argsToKeys("database")).extractOrElse("")+" "
}
if(!(json\argsToKeys("fcstdomainpartitions")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -fcstDomainPartitions "+(json\argsToKeys("fcstdomainpartitions")).extractOrElse("")+" "
}
if(!(json\argsToKeys("loglevel")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -logLevel "+(json\argsToKeys("logLevel")).extractOrElse("")+" "
}
if(!(json\argsToKeys("sleeptime")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -sleepTime "+(json\argsToKeys("sleeptime")).extractOrElse("")+" "
}
if(!(json\argsToKeys("maxproductcount")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -maxProductCount "+(json\argsToKeys("maxproductcount")).extractOrElse("")+" "
}
if(!(json\argsToKeys("maxpromostartdate")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -maxPromoStartDate "+(json\argsToKeys("maxpromostartdate")).extractOrElse("")+" "
}
if(!(json\argsToKeys("printSchemas")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -printSchemas "+(json\argsToKeys("printSchemas")).extractOrElse("")+" "
}
if(!(json\argsToKeys("generateDotFiles")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -generateDotFiles "+(json\argsToKeys("generateDotFiles")).extractOrElse("")+" "
}
if(!(json\argsToKeys("dotFilesPath")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -dotFilesPath "+(json\argsToKeys("dotFilesPath")).extractOrElse("")+" "
}
if(!(json\argsToKeys("weeksToExport")).extractOrElse("").equals("")) {
	finalArgs=finalArgs+" -weeksToExport "+(json\argsToKeys("weeksToExport")).extractOrElse("")+" "
}
finalArgs=finalArgs+" "
println(finalArgs)

//finalArgs=finalArgs+" -steps prepareRDataPrep"
LOGGER.debug(finalArgs)

for(c <- finalArgs.split(" ")){
	LOGGER.debug(c)
}

// call main class
//spark.conf.set("spark.sql.autoBroadcastJoinThreshold","-1")
//spark.conf.set("spark.driver.maxResultSize","6G")
LOGGER.info("Calling main class.")
val slackUrl=(json\"slackUrl").extract[String]

// call file_check.scala
dbutils.notebook.run("./file_check",7*24,Map("path" -> (adlsPrefix+"/"+env+promoPath+"/"),"slackUrl" -> slackUrl))

org.cognira.CommControl.setSlackUrl(slackUrl)
org.cognira.WeeklyBatch.main(((finalArgs+" -steps loadInventory,aggInventory,aggChainPrice").split(" ")))

val dupCheck1=spark.read.parquet(adlsPrefix+"/"+env+promoPath+"/all_data/merge_all/fact_inven_daily_str_prod.parq").groupBy("id_date","key_store","id_prod").count.where($"count">1).count
val dupCheck2=spark.read.parquet(adlsPrefix+"/"+env+promoPath+"/all_data/agg_all/dailyInventory.parq").groupBy("day","store","sku","dsc_cat").count.where($"count">1).count
if(dupCheck1==0 && dupCheck2==0){
	//  Proceed
} else {
	val errStr="Duplicates detected in merge_all/fact_inven_daily_str_prod.parq or agg_all/dailyInventory.parq, please check!"
	org.cognira.CommControl.sendSlack(errStr)
	throw new Exception(errStr)
}
org.cognira.WeeklyBatch.main(((finalArgs+" -steps prepareRDataPrep,genRedemptionRate").split(" ")))
org.cognira.CommControl.sendSlack(("weekly prep spark finished in "+((System.currentTimeMillis-start)/1000/60)+" min(s)"))

// clean dirs and generate all_tasks.txt
LOGGER.info("Cleaning directories and generating all_tasks.txt.")
LOGGER.info("ENVPATH: " + adlsPrefix+"/"+env+promoPath)
val conf_file = "/"+env+promoPath+"/r_run/conf/r_forecasting_conf.json"
json = parse(dbutils.fs.head(conf_file))
LOGGER.debug(s"Parsing $conf_file")
val rshinyrepo=((json\"image_directory").extractOrElse("")+"/thin/").replaceAll("\\/dbfs","")
val gsp=((json\"global_static").extractOrElse("")).replaceAll("\\/dbfs","")
val logBase=((json\"log_file").extractOrElse("")).replaceAll("\\/dbfs","")
val fcstLocalPath=((json\"fcst_local_path").extractOrElse("")).replaceAll("\\/dbfs","")
val srttskpath=(json\"sorttask_path").extractOrElse("").replaceAll("\\/dbfs","")
dbutils.fs.mkdirs(fcstLocalPath)
if(deleteRFiles){
	LOGGER.debug(s"Updating global_static files on path:  $gsp")

	for(i <- dbutils.fs.ls(gsp)) {
		LOGGER.debug(s"Removing ${i.path}")
		dbutils.fs.rm(i.path,true)
	}

	val envPath=adlsPrefix+"/"+env+promoPath

	for(i <- List("calendar.csv","forecast_domain.csv","init_HEB.R","mexico_stores.csv","category_override_blacklist.csv","category_override_level.csv","category_override_outlier.csv","doh-class.csv","seasonsDates.csv","seasonsProducts.csv","task_denylist.csv")) {
		if(i.equals("forecast_domain.csv")){
			val tc=spark.read.csv(envPath+"/conf/"+i).count
			if(tc==2){
				dbutils.fs.cp(envPath+"/conf/"+i,gsp)
			} else {
				org.cognira.CommControl.sendSlack("forecast_domain.csv detected as corrupt, please check immediately.")
			}
		} else if(i.equals("seasonsDates.csv") || i.equals("seasonsProducts.csv")) {
			dbutils.fs.mkdirs(gsp+i)
			dbutils.fs.cp(envPath+"/global_static/"+i,gsp+i,true)
		} else if(i.contains("task_denylist")){
			dbutils.fs.rm(srttskpath+i, true)
			dbutils.fs.cp(envPath+"/global_static/"+i, srttskpath+i, true)
		} else {
			dbutils.fs.cp(envPath+"/global_static/"+i,gsp)
		}
	}

	LOGGER.info(s"Removing existing rshiny image files on path:  $rshinyrepo")

	if(!rshinyrepo.equals("")){
		for(i <- dbutils.fs.ls(rshinyrepo)){
			LOGGER.debug(s"Removing ${i.path}")
			dbutils.fs.rm(i.path)
		}
	}

	LOGGER.info(s"Removing log files on path:  $logBase")
	for(i <- dbutils.fs.ls(logBase)) {
		if(i.isDir){
			LOGGER.debug(s"Removing files under ${i.path}")
			for(j <- dbutils.fs.ls(i.path)){
				dbutils.fs.rm(j.path)
			}
		}else{
			LOGGER.debug(s"Removing ${i.path}")
			dbutils.fs.rm(i.path)
		}
	}
	LOGGER.info(s"Removing files on path:  $fcstLocalPath")
	for(i <- dbutils.fs.ls(fcstLocalPath)) {
		if(i.isDir){
			LOGGER.debug(s"Removing files under ${i.path}")
			for(j <- dbutils.fs.ls(i.path)){
				dbutils.fs.rm(j.path,true)
			}
		}else{
			LOGGER.debug(s"Removing ${i.path}")
			dbutils.fs.rm(i.path)
		}
	}
}

// COMMAND ----------

// MAGIC %python
// MAGIC
// MAGIC ## Initialize a number of variables
// MAGIC srcpath = configBasePath + "/" + env + promoPath + '/r_run/data/sorttask/'
// MAGIC
// MAGIC DENYLIST = '%s/%s' % (srcpath, 'task_denylist.csv')
// MAGIC CATSTATS = '%s/%s' % (srcpath, 'MemTimeByCat.csv')
// MAGIC OUTFILE = '%s/%s' % (srcpath, 'TaskByCluster.csv')
// MAGIC
// MAGIC preppath = '/dbfs' + weeklyConf['prepareRDataPrepPath']
// MAGIC TASKLIST = '%s/%s' % (preppath, 'tasks_list.csv')
// MAGIC
// MAGIC sizeD = {'small': 41.0,  'medium': 200.0, 'large': 434 * .9}
// MAGIC coreD = {'small': 16, 'medium': 20, 'large': 10}
// MAGIC
// MAGIC # Process optional Checks (True/False)
// MAGIC verbose = False
// MAGIC verbose = True
// MAGIC
// MAGIC # Imports
// MAGIC import os
// MAGIC import sys
// MAGIC import re
// MAGIC import csv
// MAGIC import copy
// MAGIC import re
// MAGIC from operator import itemgetter
// MAGIC from collections import defaultdict as ddict
// MAGIC from random import sample as samp
// MAGIC
// MAGIC from pyspark.sql import SparkSession
// MAGIC sc = SparkSession.builder.getOrCreate()
// MAGIC
// MAGIC ## Functions and Lambdas
// MAGIC def sparkgetlist(f):
// MAGIC     fixname = lambda x: re.sub('^/dbfs', '', x)
// MAGIC     df = sc.read.csv(fixname(f), sep = '|', header = True)
// MAGIC     return (list(x) for x in df.collect())
// MAGIC
// MAGIC def assign_cluster(data):
// MAGIC     print('=====Assigning tasks to initial cluster:')
// MAGIC     pickClus = {0: 'small', 1: 'medium', 2: 'large'}
// MAGIC     reorgD = ddict(list)
// MAGIC     memL = []
// MAGIC     for r in data:
// MAGIC         p = (float(r[1]) > sizeD['small']) + (float(r[1]) > sizeD['medium'])
// MAGIC         reorgD[pickClus[p]].append(r)
// MAGIC
// MAGIC     for k,v in reorgD.items():
// MAGIC             print('Cluster %s has %s categories' % (k, len(v)))
// MAGIC
// MAGIC
// MAGIC     return(reorgD)
// MAGIC
// MAGIC def reassign(reorgD):
// MAGIC     ## It is most efficient if clusters have have the full complement of tasks
// MAGIC     ##    such that the number of assigned tasks per run == run of workers
// MAGIC     ## This is is accomplished by upsizing the slowest categories of medium and
// MAGIC     ##     small to the next larger cluster
// MAGIC     print('=====Reorganizing Clusters')
// MAGIC     clusterL = ('large', 'medium', 'small')
// MAGIC     for i,k in enumerate(clusterL[:-1]):
// MAGIC         # print(k)
// MAGIC         size = coreD[k]
// MAGIC         add = (size - len(reorgD[k]) % size) % size
// MAGIC         print('Moving %s categories to %s from %s' % (add, k, clusterL[i+1]))
// MAGIC         L = reorgD[clusterL[i+1]]
// MAGIC         L.sort(key = itemgetter(2))
// MAGIC         for x in range(add):
// MAGIC             task = L.pop()
// MAGIC             # print(task)
// MAGIC             reorgD[k].append(task)
// MAGIC
// MAGIC     print('=====Task Assignment after optimization:')
// MAGIC     for k,v in reorgD.items():
// MAGIC             print('Cluster %s now has %s categories' % (k, len(v)))
// MAGIC
// MAGIC     return reorgD
// MAGIC
// MAGIC toGB = lambda x: float(x)/1024**2
// MAGIC
// MAGIC #########################################
// MAGIC #########################################
// MAGIC
// MAGIC # defines splits for run groups based on number of workers and number of tasks
// MAGIC makesplit = lambda x,y: zip(range(0,y,x),range(x,x+y,x))
// MAGIC
// MAGIC ## Read Input Files
// MAGIC
// MAGIC # denylist is split by column and turned into a dictionary
// MAGIC #   no header
// MAGIC denylistD = spark.read.option("sep","|").option("header",True).csv(adlsPath+"global_static/task_denylist.csv").select("dsc_cat").rdd.map(lambda x:x[0]).collect()
// MAGIC
// MAGIC # tasklist (produced by R data prep) is split by column and turned into a dictionary
// MAGIC #    the file has a header
// MAGIC #    includes all possibles "tasks" (partition_id; partition_dsc) in produc2.csv
// MAGIC #    tasks NOT to be run are in denylist file
// MAGIC tasklistD = {x[0]:x for x in sparkgetlist(TASKLIST) if x[0] not in denylistD}
// MAGIC
// MAGIC # Read memory/time stat file listing run attributes for recently run tasks (known)
// MAGIC #   file has no header and 3 attributes
// MAGIC #      task name,
// MAGIC #      mem size in kb,
// MAGIC #      time in seconds
// MAGIC reader = csv.reader(open (CATSTATS))
// MAGIC memL = [(x, toGB(y), float(z)) for x,y,z in reader if x in tasklistD]
// MAGIC
// MAGIC # Get list of tasks
// MAGIC knowntasks = set(list(zip(*memL))[0])
// MAGIC # memD = {x for x in memL}
// MAGIC
// MAGIC # Discover missing tasks (in tasklist but not in knowntasks)
// MAGIC #     add them to the large category setting the memory size > medium maximum size
// MAGIC newtaskL = []
// MAGIC for i,k in enumerate(tasklistD.keys() ^ knowntasks,1):
// MAGIC     print('New Task %s' % k)
// MAGIC     newtaskL.append(k)
// MAGIC     memL.append((k,sizeD['medium'] + 1, float(i)))
// MAGIC
// MAGIC # upsize tasks to next larger cluster so that number of tasks == number of workers
// MAGIC byclusterD = reassign(assign_cluster(memL))
// MAGIC
// MAGIC # add results to sorted list
// MAGIC clusterL = ('small', 'medium', 'large')
// MAGIC outL = [('cluster','category','group','sizeGB','time')]
// MAGIC for k in clusterL:
// MAGIC     L = byclusterD[k]
// MAGIC     L.sort(key=itemgetter(2), reverse = True)
// MAGIC     glist = reversed([(k, r[0], int(i/coreD[k]), '%.2f' % r[1], r[2]) for i,r in enumerate(L)])
// MAGIC     # reversed(L)
// MAGIC     outL.extend(glist)
// MAGIC
// MAGIC # Write to outfile (TasksByCluster.csv in outdir)
// MAGIC
// MAGIC writer = csv.writer(open(OUTFILE,'w'), lineterminator = '\n')
// MAGIC writer.writerows(outL)
// MAGIC del writer
// MAGIC
// MAGIC checkD = ddict(lambda: ddict(list))
// MAGIC for cluster, b, grp, d, ptime in outL[1:]:
// MAGIC     checkD[cluster][grp].append(ptime)
// MAGIC print("=====Runtime estimation")
// MAGIC for k,D in checkD.items():
// MAGIC     L = []
// MAGIC     L.extend(max(x) for x in D.values())
// MAGIC     print('Cluster %s with %s groups has estimated processing time of %.2f hours' % (k, len(L), sum(L)/3600))
// MAGIC
// MAGIC dbutils.fs.rm("/"+env+promoPath+"/r_run/conf/all_tasks.txt")
// MAGIC dbutils.fs.rm("/"+env+promoPath+"/r_run/conf/all_tasks_failed.txt")
// MAGIC dbutils.fs.cp("/"+env+promoPath+"/r_run/data/sorttask/TaskByCluster.csv","/"+env+promoPath+"/r_run/conf/all_tasks.txt")
// MAGIC
// MAGIC # Checking to make sure hard-assigned categories are put in place
// MAGIC dbutils.notebook.run("./category_assignments",5*60,{"adlsEnv":adlsPath,"rPath":(configBasePath+env+promoPath),"prepDataPath":str(weeklyConf['prepareRDataPrepPath'])})

// COMMAND ----------

val decompPath=fcstLocalPath+"r_forecasting_decomp/"
val copyDecomp=false
scala.util.Try{
	if(copyDecomp) {
		dbutils.fs.mkdirs(decompPath)
		spark.read.parquet(adlsPrefix+"/"+env+promoPath+"/all_data/fcstmetrics/PFP-METRICS/fcst_override/decomp-fcst.parquet").write.partitionBy("partition_id").option("header",true).format("csv").save(decompPath+"decomp-fcst.csv")
	} else {
		dbutils.fs.mkdirs(decompPath+"decomp-fcst.csv")
	}
} recover {
	case e=>org.cognira.CommControl.sendSlack("Failed to write decomp-fcst file to "+decompPath+", please check.")
}
scala.util.Try{
	dbutils.fs.mkdirs(decompPath)
	spark.read.parquet(adlsPrefix+"/"+env+promoPath+"/all_data/fcstmetrics/PFP-METRICS/dow/seasonality/dow_sku_day.parq").write.partitionBy("partition_id").option("header",true).format("csv").save(decompPath+"dow_sku_day.csv")
} recover {
	case e=>org.cognira.CommControl.sendSlack("Failed to write dow_sku_day file to "+decompPath+", please check.")
}

// COMMAND ----------


