# Databricks notebook source
# MAGIC %md
# MAGIC Notebook to export FM_UI files from the daily run to oracle STG tables<br/>
# MAGIC Example Usage:
# MAGIC ```
# MAGIC dbutils.notebook.run(
# MAGIC   "./daily_export", 
# MAGIC   2000, 
# MAGIC   Map("JDBCURL" -> "jdbc:oracle:thin:@//wcx4-scan.heb-dsol-core.hebdigital-stg.com:1521/dafcst3_rw.heb.com",
# MAGIC       "ENVPATH" -> "s3://heb-spme-far-promo-stg-serve/PHAS1/pfp",
# MAGIC       "USER" -> dbutils.secrets.get(scope = "far-oracledb-svcacct", key = "s-appdb-username"),
# MAGIC       "PASSWORD" -> dbutils.secrets.get(scope = "far-oracledb-svcacct", key = "s-appdb-password"),
# MAGIC       "SLACKURL" -> "https://hooks.slack.com/services/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"))
# MAGIC ```

# COMMAND ----------

jdbcUrl = dbutils.widgets.get("JDBCURL") #jdbc:oracle:thin:@//wcx4-scan.heb-dsol-core.hebdigital-stg.com:1521/dafcst3_rw.heb.com
envPath = dbutils.widgets.get("ENVPATH") #s3://heb-spme-far-promo-stg-serve/PHAS1/pfp
user = dbutils.widgets.get("USER") #dbutils.secrets.get(scope = "far-oracledb-svcacct", key = "s-appdb-username")
password = dbutils.widgets.get("PASSWORD") #dbutils.secrets.get(scope = "far-oracledb-svcacct", key = "s-appdb-password")
slackUrl = dbutils.widgets.get("SLACKURL") 

# COMMAND ----------

dbutils.library.installPyPI('slack-webhook')
from pyspark.sql.types import IntegerType,BooleanType,DateType,TimestampType,StringType
from pyspark.sql.functions import *
from pyspark.sql import Row 
from slack_webhook import Slack
import os

slack = Slack(url=slackUrl)
driver = "oracle.jdbc.driver.OracleDriver"
dailyCountFile = envPath+"/all_data/fm_ui/daily_counts.parq"
message = ""

try:
  spark.read \
    .format("jdbc") \
    .option("url", jdbc_url) \
    .option("dbtable", "FM.STG_SYSTEM_DATE_TABLE") \
    .option("user", user) \
    .option("password", password) \
    .option("driver", driver) \
    .load().show()
  driver_manager = spark._sc._gateway.jvm.java.sql.DriverManager
  con = driver_manager.getConnection(jdbc_url, user, password)
except:
  dbutils.notebook.exit("Connection Error")

def castSchema(df, tableName):
  if(tableName == "STG_CALENDAR"):
    return df.withColumn("WEEK", to_timestamp("week")) \
             .withColumn("DAY",  to_timestamp("day")) \
             .withColumn("CAL_WEEK",  to_timestamp("cal_week")) \
             .withColumn("AD_WEEK",  to_timestamp("ad_week"))
  if tableName == "STG_SYSTEM_DATE_TABLE":
    return df.withColumn("FORECAST_START_DATE",  to_timestamp("forecast_start_date")) \
           .withColumn("LAST_LOAD_DATE", to_timestamp("last_load_date"))
  elif(tableName == "STG_PROMOLIST"):
    return df.withColumn("BEGIN_DAY", to_timestamp("begin_day")) \
     .withColumn("END_DAY", to_timestamp("end_day"))
  elif(tableName == "STG_LOCATION"):
    return df.drop("retl_loc_frmat_key", "retl_loc_segm_key", "loc_nm", "loc_abb", "dist_key", "dist_nm", "financial_div_key", "financial_div_nm", "lin_of_bus_key", "lin_of_bus_nm")
  elif(tableName == "STG_PRODDCWEEK" or tableName == "STG_EXCEPTION"):
    return df.withColumn("WEEK", to_timestamp("week"))
  else:
    return df

fmUIFiles = ["threshold.parq", "system_date_table.parq", "promolist.parq", "location.parq", "productlist.parq", "proddcweek.parq", "proddcpromo.parq", "exception.parq", "dclist.parq", "calendar.parq"]

fmUIRowCounts=[]
for f in fmUIFiles:
  filePath=envPath+"/all_data/fm_ui/"+f
  tableName="STG_"+os.path.splitext(f)[0].upper()
  df = castSchema(spark.read.parquet(filePath).distinct(), tableName)
  while True:
    try:
      print("Truncating {}".format(tableName))
      con.prepareCall("call fm.trunc_table('" + tableName + "')").execute()
      print("Writing {} to {} ".format(filePath, tableName))
      df.write.format("jdbc") \
        .option("url", jdbc_url) \
        .option("driver", driver) \
        .option("dbtable", "FM." + tableName) \
        .option("user", user) \
        .option("password", password).mode("append").save()
    except:
      print("Error connecting to Oracle. Retrying.")
      continue
    break
  fmUIRowCounts.append(Row(source=filePath, target=tableName, count=df.count()))
  message += "{} = {} \n".format(tableName, df.count())
    
con.close()   
dbutils.fs.rm(dailyCountFile, True)
countsDF = spark.createDataFrame(fmUIRowCounts)
countsDF.write.mode("OVERWRITE").format("parquet").option("header","true").save(dailyCountFile)

display(countsDF)
print(message)
slack.post(text= "Daily Counts: \n" + "```" + message + "```")
dbutils.notebook.exit("SUCCESS")

# COMMAND ----------


