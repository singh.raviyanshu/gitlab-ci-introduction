// Databricks notebook source
//Script to setup notebook configuration file, logging properties, and copy files needed to run job from Azure for specified notebook
import java.nio.file.Paths
import org.apache.log4j.Logger

val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
val LOGGER = Logger.getLogger(s"notebook.$NOTEBOOK_NAME")

val env = dbutils.widgets.get("ENV") // vn06685/PHAS1
val bucket = dbutils.widgets.get("BUCKET") //s3://heb-spme-far-promo-prd-serve
val notebook = dbutils.widgets.get("NOTEBOOK") //daily | weekly

// val env = "PHAS1" // /vn06685/PHAS1
// val bucket = "s3://heb-spme-far-promo-prd-serve" //s3://heb-spme-far-promo-prd-serve
// val notebook = "fcstacc" //daily | weekly | fcstacc

val bucketPath = bucket + "/" +env + "/pfp"
val dbfsPath = "dbfs:/" + env + "/pfp"

LOGGER.info("PFP S3 directory: " + bucketPath)
LOGGER.info("PFP dbfs directory: " + dbfsPath)

// COMMAND ----------

dbutils.fs.put(dbfsPath + "/conf/conf_storage_prefix.json", raw"""
{
    "adlsPrefix": "$bucket",
    "runCopy":false,
    "runCopyNotebook":"run_copy"
}
""", true)

LOGGER.info("conf_storage_prefix.json created in the following path: " + dbfsPath + "/conf/conf_storage_prefix.json")

// COMMAND ----------

val adlsPrefix = "abfss://dsfar@hebdatalake02.dfs.core.windows.net/encdata/data/promo/promo_forecast/cert"

def copy(files:Seq[String],path:String){
  files.foreach(f => {
    var format = f.toString.split("\\.").last  
    var containsHeader = false
    
    val source = if(path == "/global_static/") "s3://heb-spme-far-promo-prd-serve/PHAS1/pfp/global_static/" + f else adlsPrefix + path + f
    val destination = bucketPath + path + f

    if(format == "parq") { 
      format = "parquet" 
    } else if (format == "csv") {
      containsHeader = true
      format = format.replace("/","")
    }

    LOGGER.info("Data format: " + format)
    LOGGER.info("Data Source: " + source)
    LOGGER.info("Data Destination: " + destination)

    val start=System.currentTimeMillis()
    if(format == "json") {
      dbutils.fs.cp(source,
                  destination,
                  true)
    } else {
      val df = spark.read.format(format).load(source)
      df.write.format(format).option("header", containsHeader).save(destination)
    }
     LOGGER.info("Finished copying " + source + " in " + ((System.currentTimeMillis-start)/1000/60)+" min(s)")
  })
}

// COMMAND ----------

val fcst_override_files = List()
val fcst_all_files = List()
val pds_ui_files = List()
val fm_ui_files = List()
val global_static_files = List()

if(notebook == "daily"){
  val fcst_override_files = List("closed_stores.csv","dim_product_all.parq","new_item.csv","scn_cd_types.csv","season_dates.csv","dow_sku_day.parq")
  val fcst_all_files = List("baseline_mode.csv","store_demand.parq", "ForecastCoeffs.parq", "store_forecast_domain.parq", "reward_demand_share.parq", "StoreLevel.parq", "store_fcst_adjust.parq" ,"DOW_profile.parq" ,"SkuDayWICCoeffs.parq","SkuDayDOHCoeffs.parq" ,"old_weekly_baseline.parq" ,"old_dow_percent.parq" ,"SkuStoreFitError.parq" ,"SubclassStoreFitError.parq" ,"historical_baseline.parq" ,"one_week_fcst.parq" ,"weekly_history.parq" ,"discount_linear_fit.parq" ,"category_scaling.parq", "first_promotion_exception.parq", "new_brandNew_promo_exception.parq", "discount_range_exception.parq", "new_discount_exception.parq", "extreme_forecast_exception.parq", "new_attr_exception.parq", "forecast_coeffs_loader.parq", "store_fcst_adjust_loader.parq", "weekly_baseline_changes.parq", "final_weights.parq")
  val pds_ui_files = List("AZ-PSG-PDS-App-Gen.csv","AZ-Production-FAR-PDS-Admin.csv","smmry_grp.parq","smmry_grp_couplvl.parq","smmry_non_grp.parq","all_weeks_stores_sales.parq", "v_offr_match_panel_category.parq")
  val fm_ui_files = List("productlist.parq")
  val global_static_files = dbutils.fs.ls("abfss://dsfar@hebdatalake02.dfs.core.windows.net/encdata/data/promo/promo_forecast/cert/global_static").map(_.name)

  copy(fcst_override_files, "/all_data/fcst_override/")
  copy(fcst_all_files, "/all_data/fcst_all/")
  copy(pds_ui_files, "/all_data/pds_ui/")
  copy(fm_ui_files, "/all_data/fm_ui/")
  copy(global_static_files, "/global_static/")
  
  dbutils.fs.put(bucketPath + "/conf/conf_daily_all.json", raw"""
    {
            "userpath": "$bucketPath/all_data/",
            "database": "",
            "dotFilesPath": "",
            "generateDotFiles": "",
            "maxProductCount": "",
            "maxPromoStartDate": "",
            "printSchemas": "",
            "sleepTime": "",
            "weeksToExport": "",
            "zone": "",
            "clean": "false",
            "overwrite": "true",
            "steps": "",
            "stage":"",
            "global_static":"$bucketPath/global_static/",
            "forecast_domain":"$bucketPath/conf/forecast_domain.csv",
            "logLevel": "",
            "fcstDomainPartitions": "1000",
            "slackUrl":"https://hooks.slack.com/services/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", 
            "log4jPath":"/dbfs/$env/pfp/log/log4j-daily.properties",
            "pdsLoaderPath":"$bucketPath/pds_ui/",
            "zeroPromoLiftAlert":{
                    "failureThreshold":50,
                    "warningThreshold":5
            }
    }
    """, true)
  LOGGER.info("conf_daily_all.json created in the following path: " + bucketPath + "/conf/conf_daily_all.json")
  
  dbutils.fs.cp("dbfs:/ml/encdata/data/promo/promo_forecast/PHAS1/log/batch/log4j-daily.properties", dbfsPath + "/log/log4j-daily.properties")
  LOGGER.info("log4j-daily.properties copied to the following path: " + dbfsPath + "/log/log4j-daily.properties")
} else if(notebook == "weekly"){
    dbutils.fs.put(bucketPath + "/conf/conf_weekly_all.json", raw"""
      {
        "clean": "false",
        "database": "",
        "dotFilesPath": "",
        "forecast_domain": "$bucketPath/conf/forecast_domain.csv",
        "generateDotFiles": "",
        "global_static": "$bucketPath/global_static/",
        "log4jPath": "/dbfs/$env/pfp/log/log4j-weeklyprep.properties",
        "logLevel": "",
        "maxProductCount": "",
        "maxPromoStartDate": "",
        "overwrite": "true",
        "prepareRDataPrepPath": "/$env/pfp/all_data/prepareRDataPrep_all/",
        "printSchemas": "",
        "slackUrl": "",
        "sleepTime": "",
        "stage": "",
        "steps": "",
        "userpath": "$bucketPath/all_data/",
        "weeksToExport": "",
        "zone": ""
    }
    """, true)
    LOGGER.info("conf_weekly_all.json created in the following path: " + bucketPath + "/conf/conf_weekly_all.json")
    dbutils.fs.put(dbfsPath + "/r_run/conf/r_forecasting_conf.json", raw"""
    {
      "DOH_override": "/dbfs/$env/pfp/r_run/global_static/doh-class.csv",
      "RNG": "Rejection",
      "altfcst": "/dbfs/$env/pfp/r_run/data/fcstdata/r_forecasting_decomp/decomp-fcst.csv",
      "fcst_local_path": "/dbfs/$env/pfp/r_run/data/fcstdata_all/",
      "forecast_config": "/dbfs/$env/pfp/r_run/global_static/init_HEB.R",
      "forecast_domain": "/dbfs/$env/pfp/r_run/global_static/forecast_domain.csv",
      "global_static": "/dbfs/$env/pfp/r_run/global_static/",
      "image_directory": "/dbfs/$env/pfp/r_run/shiny",
      "log4r_path": "/dbfs/$env/pfp/log/weekly/log4r/",
      "log_file": "/dbfs/$env/pfp/log/weekly/",
      "log_level": "DEBUG",
      "overwriteImage": true,
      "prefix": "r_forecasting",
      "repotype": "dbfs",
      "slack_url": "",
      "sorttask_path": "/dbfs/$env/pfp/r_run/data/sorttask",
      "tmp_local_path": "/dbfs/$env/pfp/all_data/prepareRDataPrep_all/",
      "use_digital": true,
      "userpath": "/dbfs/$env/pfp/all_data/prepareRDataPrep_all/",
      "zone": ""
  }
    """, true)
  LOGGER.info("r_forecasting_conf.json created in the following path: " + dbfsPath + "/r_run/conf/r_forecasting_conf.json")
  
  dbutils.fs.mkdirs(s"dbfs:/$env/pfp/r_run/shiny/thin")
  LOGGER.info(s"r shiny repo dir created: dbfs:/$env/pfp/r_run/shiny/thin")
  
  dbutils.fs.mkdirs(s"dbfs:/$env/pfp/log/weekly/")
  LOGGER.info(s"log file dir created: dbfs:/$env/pfp/log/weekly/")
  
  dbutils.fs.mkdirs(s"dbfs:/$env/pfp/r_run/data/fcstdata_all/")
  LOGGER.info(s"fsct data dir created: dbfs:/$env/pfp/r_run/data/fcstdata_all/")

  dbutils.fs.cp("dbfs:/ml/encdata/data/promo/promo_forecast/PHAS1/log/batch/log4j-weeklyprep.properties", dbfsPath + "/log/log4j-weeklyprep.properties")
  LOGGER.info("log4j-weeklyprep.properties copied to the following path: " + dbfsPath + "/log/log4j-weeklyprep.properties")
  
  val categories = spark.read.option("header",true).csv("dbfs:/PHAS1/pfp/r_run/conf/all_tasks.txt").select("category").collect().map(_(0)).toList
  categories.foreach{c=>
    val category = c.toString.replace("/","-")
    dbutils.fs.put(s"dbfs:/$env/pfp/log/weekly/small/rforecasting_$category.txt","")
    dbutils.fs.put(s"dbfs:/$env/pfp/log/weekly/medium/rforecasting_$category.txt","")
    dbutils.fs.put(s"dbfs:/$env/pfp/log/weekly/large/rforecasting_$category.txt","")
  }
}

// COMMAND ----------

if(notebook == "fcstacc"){
  val fcst_all_files = List("one_week_fcst.parq","one_week_fcst_old.parq","four_week_fcst.parq","four_week_fcst_old.parq","eight_week_fcst.parq","eight_week_fcst_old.parq")
  copy(fcst_all_files, "/all_data/fcst_all/")
  dbutils.fs.put(bucketPath + "/conf/conf_fcstacc.json", raw"""
      {
              "userpath": "s3://heb-spme-far-promo-prd-serve/PHAS1/pfp/all_data/",
              "database": "",
              "zone": "",
              "clean": "false",
              "overwrite": "true",
              "steps": "",
              "stage":"gen",
              "global_static":"s3://heb-spme-far-promo-prd-serve/PHAS1/pfp/global_static/",
              "forecast_domain":"s3://heb-spme-far-promo-prd-serve/PHAS1/pfp/conf/forecast_domain.csv",
              "slackUrl":"",
              "log4jPath":"/dbfs/$env/pfp/log/log4j-fcstacc.properties"
      }
    """, true)
  LOGGER.info("conf_fcstacc.json created in the following path: " + bucketPath + "/conf/conf_fcstacc.json")
  
  dbutils.fs.cp("dbfs:/ml/encdata/data/promo/promo_forecast/PHAS1/log/batch/log4j-fcstacc.properties", dbfsPath + "/log/log4j-fcstacc.properties")
  LOGGER.info("log4j-fcstacc.properties copied to the following path: " + dbfsPath + "/log/log4j-fcstacc.properties")
}


// COMMAND ----------

 if(notebook == "approvals"){
 // val eom_export_files = List("previous_dc_approvals_20220324.parq","previous_base_approvals_20220324.parq")
 // copy(eom_export_files, "/all_data/eom_export/")
  dbutils.fs.put(bucketPath + "/conf/conf_EOMExport_all.json", raw"""
    {
            "userpath": "$bucketPath/all_data/",
            "database": "",
            "zone": "",
            "clean": "false",
            "overwrite": "true",
            "steps": "",
            "stage":"EomExports",
            "global_static":"$bucketPath/global_static/",
            "forecast_domain":"$bucketPath/conf/forecast_domain.csv",
            "slackUrl":"https://hooks.slack.com/services/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",
            "log4jPath":"/dbfs/PHAS1/pfp/log/log4j-approvals.properties"
    }
  """, true)
  LOGGER.info("conf_EOMExport_all.json created in the following path: " + bucketPath + "/conf/conf_EOMExport_all.json")
  
}

