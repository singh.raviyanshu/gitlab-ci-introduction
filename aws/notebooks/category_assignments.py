# Databricks notebook source
import os
import sys
import re
import csv
import copy
import csv
import re
from operator import itemgetter
from collections import defaultdict as ddict
from random import sample as samp
import pandas

from pyspark.sql.functions import *

envPath = dbutils.widgets.get("adlsEnv")
rPath = dbutils.widgets.get("rPath")+"/r_run/conf/all_tasks.txt"
rPrepDataPath = dbutils.widgets.get("prepDataPath")+"cat_partition_final.csv"
tgtPath = dbutils.widgets.get("rPath")+"/r_run/conf/all_tasks.txt"

if rPath.startswith("/dbfs"):
  rPath = rPath.replace("/dbfs","dbfs:")
if rPrepDataPath.startswith("/dbfs"):
  rPrepDataPath = rPrepDataPath.replace("/dbfs","dbfs:")

taskAssignments = spark.read.option("header",True).csv(envPath+"/global_static/task.assignment")
print(rPath)
tasks = spark.read.option("header",True).csv(rPath)
partitionData = spark.read.option("header",True).option("sep","|").csv(rPrepDataPath)

joinedPartitionData = taskAssignments.join(partitionData,["dsc_sub_comm","cat_id"],"left").withColumn("grouped",concat(col("dsc_cat"),col("group"))).select("grouped","assignment").withColumnRenamed("grouped","category")

display(tasks)
tasks = tasks.join(joinedPartitionData,["category"],"left").withColumn("cluster",when(col("cluster") != col("assignment"), col("assignment")).otherwise(col("cluster"))).select("cluster","category","assignment")

dbutils.fs.rm(tgtPath,True)
tasks.toPandas().to_csv(tgtPath, index=False, quoting=csv.QUOTE_NONNUMERIC)

# COMMAND ----------


