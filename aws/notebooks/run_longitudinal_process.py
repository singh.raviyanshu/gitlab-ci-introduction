# Databricks notebook source
#imports

dbutils.library.installPyPI('slack-webhook')

import datetime
import time
from pyspark.sql.functions import *
import json
from slack_webhook import Slack

## Get environment
notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
pathParts = notebookPath.split("/")

def readJSONFile(pt):
  with open(pt) as f:
    o = json.load(f)
  return o

# Reading the env from env_conf.json
envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
envJson = readJSONFile(envConfigFile)
environments = envJson['env']
currentEnv = [env for env in environments if env in pathParts]
if len(currentEnv) == 0:
  print("Required environment not available")
  raise ValueError("Required environment not available")
elif len(currentEnv) > 1:
  print("More than one environment detected")
  raise ValueError("More than one environment detected")
else: env = currentEnv[0]

## Get ADLS Prefix
promoPath = "/pfp"
storageConfigFile = "/dbfs/"+env+promoPath+"/conf/conf_storage_prefix.json"

storageConf = readJSONFile(storageConfigFile)

#universal variable
weekly_post_date = dbutils.widgets.get("date")     #getting date from weekly_post
now = datetime.strptime(weekly_post_date, "%Y%m%d").date()
date_dash_format = now.strftime('%Y-%m-%d')
now = now.strftime('%Y%m%d')

rConfFile = "/dbfs/"+env+promoPath+"/r_run/conf/r_forecasting_conf.json"

rConf = readJSONFile(rConfFile)
source_directory = rConf['fcst_local_path'].replace("/dbfs","")
target_directory_raw = "s3://heb-spme-far-promo-prd-serve/" + env + "/pfp/pna/scm/sup_chain_scratch_data/pfp_raw/"

# Instantiate Slack
slackUrl=rConf['slack_url']
slack = Slack(url=slackUrl)

#datadog settup
# ddtitle ='DSSC-PFP-WEEKLY-FORECAST'
# ddtags=['DSSC', 'env:prd', 'pfp:weeklyforecasthistory']
# ddapp_key = dbutils.secrets.get(scope="DSSC", key="datadog_app_key")
# ddapi_key = dbutils.secrets.get(scope="DSSC", key="datadog_api_key")
# dd_log = get_dd_logger(ddapi_key, ddapp_key, title=ddtitle, tags=ddtags)
# ddmetric_template='DSSCMETRIC:[{{"metric": "{}", "points": [[{},{}]]}}]'

# source files
csvs= {"*_ForecastCoeffs.csv" : f"fcst_coefficients/forecast_coeffs/run_date_{now}",
       "*_DOW_profile.csv": "fcst_coefficients/dow_profile/run_date_" + str(now),
       "*_store_fcst_adjust.csv" : "store_fcst_adjust/run_date_" + str(now),
       "*_r2fitting.csv" : "/wkly_backcast/run_date_" + str(now),
       "r_forecasting_exports/*_weekly_forecast.csv" : "wkly_forecast/run_date_" + str(now),
       "*_SkuDayWICCoeffs.csv" : "SkuDayWICCoeffs/run_date_" + str(now),
       "*_SkuDayDOHCoeffs.csv" : "SkuDayDOHCoeffs/run_date_" + str(now)}                                                                                 
  
wkly_fcst_parq_src = storageConf['adlsPrefix'] +"/"+env+ "/pfp"+"/all_data/fcst_all/weekly_forecast.parq"   
wkly_fcst_parq_raw_store = "s3://heb-spme-far-promo-prd-serve/" + env + "/pfp/pna/scm/sup_chain_scratch_data/pfp_raw/daily_wkly_forecast/run_date_" + str(now)

# COMMAND ----------

#save copy of source files into raw parquet, overwriting if it already exists
for filename in csvs.keys():
      print(f"Creating df of {filename}")
      # creation of the dataframe using the source file path
      try:
        if filename != "r_forecasting_exports/*_weekly_forecast.csv":
          source_df = spark.read.format("com.databricks.spark.csv")\
                      .options(inferSchema = "true", header = "true", delimiter = "|")\
                      .load(source_directory + filename)
        else:
          source_df = spark.read.load(source_directory + filename, inferSchema = "true", header = "true", format = "csv")
        # coalescing into 1000 equally sized partitions and overwriting the target directory if exists
        source_df.coalesce(1000).write.format("parquet").mode("overwrite").save(target_directory_raw + csvs[filename])
      except:
        error_message = f"{filename} is missing"
        print(error_message)
        #dd_log.error(error_message)
        slack.post(text=str(error_message))
        dbutils.notebook.exit()

# COMMAND ----------

#store parquet file
#creation of the dataframe using the source file path
try:
  df = sqlContext.read.parquet(wkly_fcst_parq_src)
# coalescing into 1000 equally sized partitions and overwriting the target directory if exists
  df.coalesce(1000).write.format("parquet").mode("overwrite").save(wkly_fcst_parq_raw_store)
except:
    error_message = f"{wkly_fcst_parq_src} is missing"
    print(error_message)
    #dd_log.error(error_message)
    slack.post(text=str(error_message))
    dbutils.notebook.exit()

# COMMAND ----------

#DOW Profile
 
dow_profile = source_directory + "*_DOW_profile.csv"

#create df
dowDF = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter='|')\
    .load(dow_profile)
 
dowDF.show()

dowDFCount = dowDF.count()
print(f"DowProfile: {dowDFCount} file rows")
# t_now=time.time()
# metric='dssc.pfp.dowprofile.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, dowDF.count()))


#select specifics
dowDF = dowDF.selectExpr(["`DOW_profile.subclass` as dow_subclass",
                  "`DOW_profile.dow` as dow_dow",
                  "`DOW_profile.ttl_reg_profile` as ttl_reg_profile",
                  "`DOW_profile.ttl_Easter_profile` as ttl_Easter_profile",
                  "`DOW_profile.ttl_MEM_profile` as ttl_MEM_profile",
                  "`DOW_profile.ttl_FOURTH_profile` as ttl_FOURTH_profile",
                  "`DOW_profile.ttl_XMAS_profile` as ttl_XMAS_profile",
                  "`DOW_profile.ttl_THANK_profile` as ttl_THANK_profile",
                  "`DOW_profile.ttl_XMAS_current_profile` as ttl_XMAS_current_profile",
                  "`DOW_profile.ttl_XMAS_new_profile` as ttl_XMAS_new_profile",
                  "`DOW_profile.t_XMAS` as t_XMAS"])
 
dowDF.show()
 
dowDF = dowDF.select([regexp_extract('dow_subclass', '\d+', 0).alias('dow_subclass'),
                     'dow_dow',
                     'ttl_reg_profile',
                     'ttl_Easter_profile',
                     'ttl_MEM_profile',
                     'ttl_FOURTH_profile',
                     'ttl_XMAS_profile',
                     'ttl_THANK_profile',
                     'ttl_XMAS_current_profile',
                     'ttl_XMAS_new_profile',
                     't_XMAS'])
 
dowDF.show()
 
#add run_date column
runDate = dowDF.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
 
runDate.registerTempTable("dow_profile_temp")
 
runDate = runDate.dropDuplicates() # any or all
 
runDate.show()
 
run_date_count = runDate.count()
print(f"DowProfile Stage: {run_date_count} raw DOW")

# t_now=time.time()
# metric='dssc.pfp.dowprofile.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, runDate.count()))
 
sqlContext.sql("drop table if exists sup_chain_stage.dow_profile_stage")
 
# store the data into stage table so we're not directly coupled
sqlContext.sql("""
create table
if not exists
sup_chain_stage.dow_profile_stage
using parquet
partitioned by (run_date)
as
select 
dow_subclass,
dow_dow,
ttl_reg_profile,
ttl_easter_profile,
ttl_mem_profile,
ttl_fourth_profile,
ttl_xmas_profile,
ttl_thank_profile,
'null' ttl_XMAS_2017_profile,
ttl_XMAS_current_profile,
ttl_xmas_new_profile,
t_xmas,
run_date as run_date
from
dow_profile_temp
""")
 
#insert into serve
sqlContext.sql("""
insert into table 
sup_chain_serve.dow_profile_serve
partition(run_date)
select
dow_subclass,
dow_dow,
ttl_reg_profile,
ttl_easter_profile,
ttl_mem_profile,
ttl_fourth_profile,
ttl_xmas_profile,
ttl_thank_profile,
ttl_XMAS_2017_profile,
ttl_XMAS_current_profile,
ttl_xmas_new_profile,
t_xmas,
cast(run_date as date) as run_date
from
sup_chain_stage.dow_profile_stage
""")
sqlContext.sql("select max(run_date) from sup_chain_serve.dow_profile_serve").show()
sqlContext.sql("select count(*) from sup_chain_serve.dow_profile_serve").show()

# COMMAND ----------

# #forecast coefficients join with sku-day-wic coeffs and sku-day-doh coeffs into one data-set and stored to our promo-forecast-coeffs table
 
forecastCoeffs = source_directory + "*_ForecastCoeffs.csv"

forecastDF = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter='|')\
    .load(forecastCoeffs)
 
forecastDF_count = forecastDF.count()
print(f"ForecastCoeffs: {forecastDF_count} file rows")
# t_now=time.time()
# metric='dssc.pfp.forecastcoeffs.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, forecastDF.count()))
 
forecastDF = forecastDF.selectExpr(["`ForecastCoeffs.sku` as sku",
                           "`ForecastCoeffs.factor` as factor",
                           "`ForecastCoeffs.control` as control",
                           "`ForecastCoeffs.value` as value",
                           "`ForecastCoeffs.mult` as mult",
                           "`ForecastCoeffs.norm_seas` as norm_seas",
                           "`ForecastCoeffs.source` as source",
                           "`ForecastCoeffs.discount_type` as discount_type"])
 
forecastDF = forecastDF.select([regexp_extract('sku', '\d+', 0).alias('sku'),
                       'factor',
                       'control',
                       'value',
                       'mult',
                       'norm_seas',
                       'source',
                       'discount_type'])
 
forecastDF.registerTempTable("forecast_temp")

forecastDF_count = forecastDF.count()
print(f"ForecastCoeffs Stage: {forecastDF_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.forecastcoeffs.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, forecastDF.count()))
 
skuDayDOHcoeffs = source_directory + "*_SkuDayDOHCoeffs.csv"
 
skuDayDOHdf = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter='|')\
    .load(skuDayDOHcoeffs)
 
skuDayDOHdf = skuDayDOHdf.selectExpr(["`SkuDayDOHCoeffs.sku` as sku",
                              "`SkuDayDOHCoeffs.factor` as factor",
                              "`SkuDayDOHCoeffs.value` as value",
                              "`SkuDayDOHCoeffs.mult` as mult"])
 
skuDayDOHdf = skuDayDOHdf.withColumn("source", lit("doh"))
skuDayDOHdf = skuDayDOHdf.withColumn("control", lit("null"))
 
skuDayDOHdf = skuDayDOHdf.select([regexp_extract('sku', '\d+', 0)
                                 .alias('sku'), 'factor', 'control', 'value', 'mult', 'source'])
 
skuDayDOHdf.registerTempTable("skuDayDOH_temp")

skuDayDOHdf_count = skuDayDOHdf.count()
print(f"skudaydoh Stage: {skuDayDOHdf_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.skudaydoh.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, skuDayDOHdf.count()))
 
skuDayWicCoeffs = source_directory + "*_SkuDayWICCoeffs.csv"

# back up
# skuDayWicCoeffs = source_directory + "*_SkuDayWICCoeffs.csv"

skuDayWicDF = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter='|')\
    .load(skuDayWicCoeffs)
 
skuDayWicDF = skuDayWicDF.selectExpr(["`SkuDayWICCoeffs.sku` as sku", "`SkuDayWICCoeffs.factor` as factor",
                                            "`SkuDayWICCoeffs.value` as value", "`SkuDayWICCoeffs.mult` as mult"])
 
skuDayWicDF = skuDayWicDF.withColumn("source", lit("wic"))
skuDayWicDF = skuDayWicDF.withColumn("control", lit("null"))
 
skuDayWicDF = skuDayWicDF.select([regexp_extract('sku', '\d+', 0)
                                 .alias('sku'), 'factor', 'control', 'value', 'mult', 'source'])
 
skuDayWicDF.registerTempTable("skuDayWic_temp")

skuDayWicDF_count = skuDayWicDF.count()
print(f"skudaywic Stage: {skuDayWicDF_count} table rows")
# t_now=time.time()
#  metric='dssc.pfp.skudaywic.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, skuDayWicDF.count()))
 
unionDF = sqlContext.sql("""
    select
        cast(sku as int)as sku,
        factor,
        null as control,
        cast(value as float)as value,
        cast(mult as float)as mult,
        null as norm_seas,
        null as discount_type,
        'wic' as source
        from
    skuDayWic_temp
union all
    select
        cast(sku as int)as sku,
        factor,
        null as control,
        cast(value as float)as value,
        cast(mult as float)as mult,
        null as norm_seas,
        null as discount_type,
        'doh' as source
        from
    skuDayDOH_temp
union all
    select
        cast(sku as int)as sku,
        factor,
        control,
        cast(value as float)as value,
        cast(mult as float)as mult,
        norm_seas,
        discount_type,
        source
    from
forecast_temp
""")
 
unionDF = unionDF.dropDuplicates()
 
unionDF.printSchema()
 
runDate = unionDF.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
runDate.printSchema()
 
runDate.registerTempTable("promo_coeffs_temp")
 
union_count = runDate.count()
print(f"promocoeffs stage: {union_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.promocoeffs.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, runDate.count()))
 
sqlContext.sql("drop table if exists sup_chain_stage.promo_forecast_coeffs_stage")
 
#create stage table
sqlContext.sql("""
create table
if not exists
sup_chain_stage.promo_forecast_coeffs_stage
using parquet
partitioned by (run_date)
as
select
*
from
promo_coeffs_temp
""")
 
sqlContext.sql("""
insert into table
sup_chain_serve.promo_forecast_coeffs
partition(run_date)
select
sku,
factor,
control,
value,
mult,
norm_seas,
discount_type,
source,
cast(run_date as date) as run_date
from
sup_chain_stage.promo_forecast_coeffs_stage
""")
 
sqlContext.sql("select max(run_date) from sup_chain_serve.promo_forecast_coeffs").show()


# COMMAND ----------

#store level joins to store forecast adjust with some other transformations and stored to our store_promo_forecast table
store_fcst_adjust = source_directory + "*_store_fcst_adjust.csv"
store_level = source_directory + "*_StoreLevel.csv"
 
#load tables
storeForecastAdjust = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter="|")\
            .load(store_fcst_adjust)
 
storeForecastAdjust.show()
 
count_storeForecastAdjust = storeForecastAdjust.count()
print(f"storeForecastAdjust: {count_storeForecastAdjust} file rows")
# t_now=time.time()
# metric='dssc.pfp.storeforecastadjust.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, storeForecastAdjust.count()))
 
storeLvlDF = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter="|")\
            .load(store_level)
storeLvlDF.show()

storeLvlDF_count = storeLvlDF.count()
print(f"storelevel: {storeLvlDF_count} file rows")
# t_now=time.time()
# metric='dssc.pfp.storelevel.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, storeLvlDF.count()))
 
#select fields from each table
storeFadjustDF = storeForecastAdjust.selectExpr(["`store_fcst_adjust.sku` as store_fcst_adjust_sku", 
                                                 "`store_fcst_adjust.store` as store_fcst_adjust_store", 
                                                 "`store_fcst_adjust.discount_type` as store_fcst_adjust_discount_type",
                                                 "`store_fcst_adjust.depromoted_factor` as store_fcst_adjust_depromoted_factor"])
 
storeFadjustDF.show()

storeFadjustDF_count = storeFadjustDF.count()
print(f"storeLvlDF: {storeLvlDF_count} raw")
# t_now=time.time()
# metric='dssc.pfp.storeforecastadjust.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, storeFadjustDF.count()))
 
storeLevelDF = storeLvlDF.selectExpr(["`StoreLevel.sku` as strlvl_sku",
                       "`StoreLevel.store` as strlvl_store",
                       "`StoreLevel.sc_level` as strlvl_sc_level",
                       "`StoreLevel.adjusted_level` as strlvl_adjusted_level"])
 
storeLevelDF.show()

storeLevelDF_count = storeLevelDF.count()
print(f"storeLevel: {storeLevelDF_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.storelevel.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, storeLevelDF.count()))
 
#join tables
storeDF = storeFadjustDF.join(storeLevelDF, (storeFadjustDF.store_fcst_adjust_sku == storeLevelDF.strlvl_sku)
                    & (storeFadjustDF.store_fcst_adjust_store == storeLevelDF.strlvl_store), 'inner')
 
#rename columns from joined table
storeDF = storeDF.selectExpr(["store_fcst_adjust_sku as sku",
                              "store_fcst_adjust_store as store",
                              "store_fcst_adjust_discount_type as discount_type",
                              "store_fcst_adjust_depromoted_factor as depromoted_factor",
                              "strlvl_sc_level as sc_level",
                              "strlvl_adjusted_level as adjusted_level"])
 
storeDF.show()

storeDF_count = storeDF.count()
print(f"storeDF: {storeDF_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.storepromoforecast.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, storeDF.count()))
 
#add run date
runDate = storeDF.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
 
runDate.registerTempTable("store_temp")
 
runDate = runDate.dropDuplicates()
 
runDate.show()
 
# Drop stage table
sqlContext.sql("drop table if exists sup_chain_stage.store_promo_forecast_stage")
 
# create volatile stage table or staging table
spark.sql("""
create table
if not exists
sup_chain_stage.store_promo_forecast_stage
using parquet
partitioned by (run_date)
as
select
*
from
store_temp
""")
 
sqlContext.sql("""
insert into table
sup_chain_serve.store_promo_forecast_serve
partition(run_date)
select
sku,
store,
discount_type,
depromoted_factor,
sc_level,
adjusted_level,
cast(run_date as date) as run_date
from
sup_chain_stage.store_promo_forecast_stage
""")
 
sqlContext.sql("select max(run_date) from sup_chain_serve.store_promo_forecast_serve").show()
sqlContext.sql("select count(*) from sup_chain_serve.store_promo_forecast_serve where run_date = (select max(run_date) from sup_chain_serve.store_promo_forecast_serve)").show()

# COMMAND ----------

# daily-weekly forecast storage with some other transformations
target = "s3://heb-spme-far-promo-prd-serve/" + env + "/pfp/pna/scm/sup_chain_scratch_data/pfp_raw/daily_wkly_forecast_csv_ad_hoc/run_date_" + str(now)

weeklyForecast = storageConf['adlsPrefix'] +"/" + env + "/pfp" + "/all_data/fcst_all/weekly_forecast.parq"
 
weeklyForecastparq = sqlContext.read.parquet(weeklyForecast)
weeklyForecastparq.write.format('csv').option('header', 'true').mode('overwrite').save(target)


weeklyForecastDF = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter=",")\
            .load("s3://heb-spme-far-promo-prd-serve/" + env + "/pfp/pna/scm/sup_chain_scratch_data/pfp_raw/daily_wkly_forecast_csv_ad_hoc/run_date_" + str(now))

weeklyForecastDF_count = weeklyForecastDF.count()
print(f"dailyweeklyforecast: {weeklyForecastDF_count} file rows")
# t_now=time.time()
# metric='dssc.pfp.dailyweeklyforecast.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, weeklyForecastDF.count()))
 

weeklyForecastDF_selected = weeklyForecastDF.selectExpr(["sku",
                                               "store",
                                               "to_date(week, 'yyyy-MM-dd')as week",
                                               "baseline_forecast",
                                               "forecast",
                                               "discount_type"])
 
addRunDate = weeklyForecastDF_selected.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
print(type(addRunDate.head()["run_date"]))

addRunDate.show()
# TODO: for Sangeetha to add...does she have access???
addRunDate = addRunDate.dropDuplicates()

addRunDate_count = addRunDate.count()
print(f"weeklyforecasthistory stage: {addRunDate_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.weeklyforecasthistory.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, addRunDate.count()))
 
addRunDate.registerTempTable("forecast_temp")
 
# checking the source record count and sum of the forecast

spark.sql("""
select 
count(*) as source_record_count,
week,
sum(forecast) as source_forecast_sum,
cast(run_date as date) as source_run_date
from
forecast_temp
group by
week,
cast(run_date as date)
""")

sqlContext.sql("drop table if exists sup_chain_stage.weekly_forecast_temp_nov_29")
 
# create temp table or staging table
sqlContext.sql("""
create table
if not exists
sup_chain_stage.weekly_forecast_temp_nov_29
using parquet
partitioned by (run_date)
as
select
sku, 
store, 
week, 
baseline_forecast, 
forecast, 
discount_type,
cast(run_date as date) as run_date 
from 
forecast_temp
""")
 
# insert into base or serve table
sqlContext.sql("""
insert into table 
sup_chain_serve.weekly_forecast_history
partition(run_date) 
select 
sku, 
store, 
week, 
baseline_forecast, 
forecast, 
discount_type, 
cast(run_date as date) as run_date 
from 
sup_chain_stage.weekly_forecast_temp_nov_29
where run_date = (select 
max(run_date) 
from sup_chain_stage.weekly_forecast_temp_nov_29)
""")
 
sqlContext.sql("select max(run_date) from sup_chain_serve.weekly_forecast_history").show()

# COMMAND ----------

#store weekly forecast CSVs to wkly_fcst_hist table
forecast = source_directory + "r_forecasting_exports/*_weekly_forecast.csv"

weeklyFC = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter=',')\
    .load(forecast)

# TODO: forecast_alt and baseline_alt
weeklyFC = weeklyFC.selectExpr(["sku",
                               "store",
                               "to_date(week, 'yyyy-MM-dd')as week",
                               "baseline_forecast",
                               "forecast",
                               "discount_type",
                               "`fcst_alt` as forecast_alt",
                               "`baseline_alt` as baseline_alt"])
 
# add Run_date or date we run the job
addRunDate = weeklyFC.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
 
addRunDate = addRunDate.dropDuplicates()
 
addRunDate.registerTempTable("wkly_fcst_temp")
 
# drop the table to
spark.sql("drop table if exists sup_chain_stage.wkly_fcst_temp_jan_29")
 
# create temp table or staging table
spark.sql("""
create table
if not exists
sup_chain_stage.wkly_fcst_temp_jan_29
using parquet
partitioned by (run_date)
as
select
*
from
wkly_fcst_temp
""")
 
#check the record count and sum of temp table 
spark.sql("""
select 
count(*) as source_count,
cast(sum(forecast)as int) as source_fcst_sum,
max(run_date) as run_date
from 
sup_chain_stage.wkly_fcst_temp_jan_29
where
run_date = 
(select 
max(run_date)
from
sup_chain_stage.wkly_fcst_temp_jan_29)
""").show()
 
#insert into base or serve table
spark.sql("""
insert into table
sup_chain_serve.wkly_fcst_hist
select
sku, 
store, 
week, 
baseline_forecast, 
forecast, 
discount_type,
forecast_alt,
baseline_alt,
cast(run_date as date) as run_date 
from 
sup_chain_stage.wkly_fcst_temp_jan_29
""")
 
# check the record count on the weekly_weekly from r_forecasting_exports directory
spark.sql("""
select 
count(*) as weekly_weekly_count,
cast(sum(forecast) as int)as target_fcst_sum,
max(run_date) as target_run_date
from 
sup_chain_serve.wkly_fcst_hist
where 
run_date = 
(select 
max(run_date) 
from 
sup_chain_serve.wkly_fcst_hist)""").show()
 

# check the record count on the weekly_forecast or daily_weekly from forecast_all directory
spark.sql("""
select 
count(*) as weekly_daily_count,
cast(sum(forecast) as int) as weekly_daily_sum,
max(run_date) as weekly_run_date
from 
sup_chain_serve.weekly_forecast_history 
where 
run_date = 
(select max(run_date) 
from 
sup_chain_serve.weekly_forecast_history)""").show()

# COMMAND ----------

#previously in Cesar's notebook - work using the same source file as above daily-weekly forecast storage
weeklyFC = weeklyForecastDF.selectExpr(["sku",
                               "store",
                               "to_date(week, 'yyyy-MM-dd')as week",
                               "baseline_forecast",
                               "forecast",
                               "discount_type"])
 
addRunDate = weeklyFC.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
 
addRunDate = addRunDate.dropDuplicates()

addRunDate_count = addRunDate.count()
print(f"wkly_fcst_hist stage.: {addRunDate_count}  rows")
# t_now=time.time()
# metric='dssc.pfp.wkly_fcst_hist.stage.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, addRunDate.count()))
 
addRunDate.registerTempTable("daily_weekly_temp")
 
# check if table exist when ran, if so delete
spark.sql("drop table if exists sup_chain_stage.daily_weekly_fcst_temp_nov_29")
 
# create temp table or staging table
spark.sql("""
create table
if not exists
sup_chain_stage.daily_weekly_fcst_temp_nov_29
using parquet
partitioned by (run_date)
as
select
sku, 
store, 
week, 
baseline_forecast, 
forecast, 
discount_type, 
cast(run_date as date) as run_date 
from
daily_weekly_temp
""")
 
# check the record count on the weekly_weekly from r_forecasting_exports directory
spark.sql("""
select 
count(*) as source_count,
cast(sum(forecast)as int) as source_fcst_sum,
max(run_date) as run_date
from 
sup_chain_serve.wkly_fcst_hist
where
run_date = 
(select 
max(run_date)
from
sup_chain_serve.wkly_fcst_hist)
""").show()
 
# insert into base or serve table
spark.sql("""
insert into table
sup_chain_serve.daily_weekly_forecast_history
select
sku, 
store, 
week, 
baseline_forecast, 
forecast, 
discount_type, 
cast(run_date as date) as run_date 
from 
sup_chain_stage.daily_weekly_fcst_temp_nov_29
""")
 
# check the record count on the daily_weekly from r_forecasting_exports directory
spark.sql("""
select count(*) as daily_weekly_count,
cast(sum(forecast) as int) as daily_wkly_sum,
max(run_date) as weekly_run_date
from 
sup_chain_serve.daily_weekly_forecast_history 
where 
run_date = 
(select 
max(run_date) 
from 
sup_chain_serve.daily_weekly_forecast_history)""").show()
 
# check the record count on the weekly_forecast or daily_weekly from forecast_all directory
spark.sql("""
select 
count(*) as weekly_daily_count,
cast(sum(forecast) as int) as weekly_daily_sum,
max(run_date) as weekly_run_date
from 
sup_chain_serve.weekly_forecast_history 
where 
run_date = 
(select max(run_date) 
from 
sup_chain_serve.weekly_forecast_history)""").show()

# COMMAND ----------

#r2-fitting or back-cast transformed or massaged and stored to our r2fitting table in serve
from pyspark.sql.functions import *
 
r2Fitting = source_directory + "*_r2fitting.csv"

r2 = sqlContext.read.format("com.databricks.spark.csv")\
    .options(header=True, inferSchema=True, delimiter="|")\
            .load(r2Fitting)

r2_count = r2.count()
print(f"rfit : {r2_count} file rows")
# t_now=time.time()
# metric='dssc.pfp.rfit.file.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, r2.count()))
 

r2 = r2.selectExpr([
    "sku",
    "store",
    "to_date(week, 'yyyy-MM-dd') as week",
    "sku_daily_forecast as weekly_backcast",
    "units",
    "discount_type",
    "naive_fcst",
    "SST",
    "SSR"
])
 
runDate = r2.withColumn("run_date", date_format(lit(date_dash_format), "yyyy-MM-dd"))
 
runDate.show()
 
runDate = runDate.dropDuplicates()

runDate_count = runDate.count()
print(f"rfit stage: {runDate_count} table rows")
# t_now=time.time()
# metric='dssc.pfp.rfit.stage.table.rows'
# dd_log.debug(ddmetric_template.format(metric, t_now, runDate.count()))
 

runDate.registerTempTable("r2_fitting_temp")
 
spark.sql("drop table if exists sup_chain_stage.r2_fitting_stage_nov_29")
 
spark.sql("""
create table
if not exists
sup_chain_stage.r2_fitting_stage_nov_29
using parquet
partitioned by (run_date)
as
select
sku,
store,
to_date(week, 'yyyy-MM-dd') as week,
weekly_backcast,
units,
discount_type,
naive_fcst,
SST,
SSR,
cast(run_date as date) as run_date
from
r2_fitting_temp
""")
  
spark.sql("refresh table sup_chain_stage.r2_fitting_stage_nov_29")
 
spark.sql("show partitions sup_chain_stage.r2_fitting_stage_nov_29").show(50)
  
spark.sql("""
insert into table
sup_chain_serve.r2_fitting
partition(run_date)
select
sku,
store,
to_date(week, 'yyyy-MM-dd') as week,
weekly_backcast,
units,
discount_type,
naive_fcst,
SST,
SSR,
cast(run_date as date) as run_date
from
sup_chain_stage.r2_fitting_stage_nov_29
where run_date = 
(select max(run_date) from sup_chain_stage.r2_fitting_stage_nov_29)
""")
 
sqlContext.sql("select * from sup_chain_serve.r2_fitting limit 10").show()


# COMMAND ----------

#meta-store check repair on all related tables and then show partitions to show the latest stored partitions
#msck repairs and/or syncs up missing or new partitions with the hive metastore
 
spark.sql("msck repair table sup_chain_serve.wkly_fcst_hist")
 
spark.sql("msck repair table sup_chain_serve.dow_profile_serve")
 
spark.sql("msck repair table sup_chain_serve.store_promo_forecast_serve")
 
spark.sql("msck repair table sup_chain_serve.promo_forecast_coeffs")
 
spark.sql("msck repair table sup_chain_serve.r2_fitting")
 
spark.sql("msck repair table sup_chain_serve.daily_weekly_forecast_history")
 
# show the last 100 weeks of partitions if available on the tables
spark.sql("show partitions sup_chain_serve.wkly_fcst_hist").show(100)
 
spark.sql("show partitions sup_chain_serve.dow_profile_serve").show(100)
 
spark.sql("show partitions sup_chain_serve.store_promo_forecast_serve").show(100)
 
spark.sql("show partitions sup_chain_serve.promo_forecast_coeffs").show(100)
 
spark.sql("show partitions sup_chain_serve.r2_fitting").show(100)

spark.sql("show partitions sup_chain_serve.daily_weekly_forecast_history").show(100)

# COMMAND ----------

#writing done files
run_dateDF = spark.sql("select max(run_date) as  from sup_chain_serve.wkly_fcst_hist")
rn_dt = run_dateDF.head()
 

Data_Count = spark.sql("select count(*) from sup_chain_serve.wkly_fcst_hist where run_date =(select max(run_date) from sup_chain_serve.wkly_fcst_hist)")
dc= Data_Count.head()

dbutils.fs.put("/"+env+"/pfp"+"/pid/wkly_fcst_hist_sc_run_complete.txt", 
                "wkly_fcst_hist" + " " + str(rn_dt) + " " + str(dc) + " " + "DONE",
                True)

df = spark.read.option("header", "false").csv("/"+env+"/pfp"+"/pid/wkly_fcst_hist_sc_run_complete.txt")
display(df)

# COMMAND ----------

message = "The PFP (LONG) job has been successfully ran."
slack.post(text=message)

# COMMAND ----------


