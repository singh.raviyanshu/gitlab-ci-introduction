%python
from pyspark.sql.functions import *

env = dbutils.widgets.get("env")
tasksPath = dbutils.widgets.get("path")
fcst_local_path = dbutils.widgets.get("fcst_local_path")

def replaceCat(tcat):
  cat=tcat
  cat=cat.replace(" ","_")
  cat=cat.replace("&","_")
  cat=cat.replace("/","_")
  cat=cat.replace(",","_")
  cat=cat.replace("-","_")
  cat=cat.replace("___","_")
  cat=cat.replace("__","_")
  return cat

expectedFiles=["DOW_profile","ForecastCoeffs","SkuDayDOHCoeffs","SkuDayWICCoeffs","SkuStoreFitError","SkuStoreVariance","StoreLevel","SubclassStoreFitError","discount_range_exception","extreme_forecast_exception","fcst_factors","first_promotion_exception","historical_baseline","new_attr_exception","new_brandNew_promo_exception","new_discount_exception","r2fitting","sku_demand","store_demand","store_fcst_adjust","store_hist_simple","store_noise","weekly_history"]

all_tasks = spark.read.option("header",True).csv(tasksPath)

cats = all_tasks.rdd.map(lambda x:x[1]).collect()
filesMap = {}
categoriesCount = {}
for i in dbutils.fs.ls(fcst_local_path.replace("/dbfs","")):
  for ef in expectedFiles:
    if ef in i.name:
      catName = i.name.split("_"+ef)[0]
      filesMap[i.name] = catName
      if catName in categoriesCount:
        tc = categoriesCount[catName] + 1
        categoriesCount[catName] = tc
      else:
        categoriesCount[catName] = 1

failed = []

for cat in cats:
  c = cat
  cat = replaceCat(cat)
  if cat in categoriesCount and categoriesCount[cat] < 23:
    print(k+" "+categoriesCount[cat])
  elif cat not in categoriesCount:
    print("cat not in category")
    if spark.read.text(fcst_local_path.replace("/dbfs","")+"r_forecasting_log/"+cat+".txt").where(col("value").contains("Completed all processing")).count() == 0:
      failed.append(c)

for fail in failed:
  print(fail)

if len(failed) > 0:
  dbutils.notebook.exit("Failure")
else:
  dbutils.notebook.exit("Done")
