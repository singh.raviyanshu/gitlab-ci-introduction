// Databricks notebook source
import org.json4s.jackson.JsonMethods._
import org.json4s._

//Get env based on notebook path
implicit val formats = DefaultFormats
val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")
val configBasePath = "dbfs:/"
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
  if(currentEnv.size == 0){
    println("Required environment not available")
    throw new Exception("Required environment not available")
  } else if(currentEnv.size > 1) {
    println("More than one environment detected")
    throw new Exception("More than one environment detected")
  } else currentEnv(0)
}
dbutils.notebook.run("./run_weekly_all_sizes", 7*24*60*60, Map("size" -> "small","env" -> env,"tasksFile" -> "all_tasks.txt","failedTasksFile" -> "all_tasks_failed.txt"))

// COMMAND ----------


// comment for test