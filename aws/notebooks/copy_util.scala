// This notebook will copy data from a given an abfss source path and aws s3 destination
// This notebook can be called with the following command: 
// dbutils.notebook.run(
//   "./copy_util", 
//   200, 
//   Map("SOURCE" -> "abfss://dsfar@hebdatalake02.dfs.core.windows.net/encdata/data/promo/promo_forecast/cert/all_data/agg_all/dailySales.parq/",
//       "DESTINATION" -> "s3://heb-spme-far-promo-stg-databricks-root/dsfar/temp/file_validate/aws/data/dailySales.parq/"))
import java.nio.file.Paths
import org.apache.log4j.Logger

private val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
private val LOGGER = Logger.getLogger(s"notebook.$NOTEBOOK_NAME")

val start=System.currentTimeMillis()
LOGGER.info("Start Time: " + start)

val source = dbutils.widgets.get("SOURCE")
val destination = dbutils.widgets.get("DESTINATION")

val fileName = Paths.get(source).getFileName  
var format = fileName.toString.split("\\.").last  
var containsHeader = false

if(format == "parq") { 
  format = "parquet" 
} else if (format == "csv") {
  containsHeader = true
}

LOGGER.info("Data format: " + format)
LOGGER.info("Data Source: " + source)
LOGGER.info("Data Destination: " + destination)

if(format == "json") {
  dbutils.fs.cp(source,
              destination,
              true)
} else {
  val df = spark.read.format(format).load(source)
  df.write.format(format).option("header", containsHeader).save(destination)
}

LOGGER.info("Finished in: " + ((System.currentTimeMillis-start)/1000/60)+" min(s)")
dbutils.notebook.exit("SUCCESS")
