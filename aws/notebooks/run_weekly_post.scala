// Databricks notebook source
// MAGIC %python
// MAGIC dbutils.library.installPyPI('databricks_cli')
// MAGIC dbutils.library.installPyPI('slack-webhook')
// MAGIC '''
// MAGIC Imports, lambdas, and functions
// MAGIC SparkSession setup
// MAGIC '''
// MAGIC import os
// MAGIC import sys
// MAGIC import csv
// MAGIC import re
// MAGIC import json
// MAGIC
// MAGIC from collections import defaultdict as ddict
// MAGIC from operator import itemgetter
// MAGIC from datetime import datetime
// MAGIC from random import sample as samp
// MAGIC
// MAGIC from pyspark.sql import SparkSession
// MAGIC import pyspark.sql.functions as F
// MAGIC
// MAGIC def readJSONFile(pt):
// MAGIC   with open(pt) as f:
// MAGIC     o = json.load(f)
// MAGIC   return o
// MAGIC sc = SparkSession.builder.getOrCreate()
// MAGIC
// MAGIC notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
// MAGIC print(notebookPath)
// MAGIC pathParts = notebookPath.split("/")
// MAGIC print(pathParts)
// MAGIC
// MAGIC # Reading env from env_conf.json
// MAGIC envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
// MAGIC envJson = readJSONFile(envConfigFile)
// MAGIC environments = envJson['env']
// MAGIC currentEnv = [env for env in environments if env in pathParts]
// MAGIC if len(currentEnv) == 0:
// MAGIC   print("Required environment not available")
// MAGIC   raise ValueError("Required environment not available")
// MAGIC elif len(currentEnv) > 1:
// MAGIC   print("More than one environment detected")
// MAGIC   raise ValueError("More than one environment detected")
// MAGIC else: env = currentEnv[0]
// MAGIC print(env)
// MAGIC
// MAGIC configBasePath = "/dbfs/"
// MAGIC promoPath="/pfp"
// MAGIC inpath = (configBasePath+env+'%s') % promoPath
// MAGIC
// MAGIC storageConfigFile = readJSONFile(inpath+'/conf/conf_storage_prefix.json')
// MAGIC rconf = readJSONFile(inpath+'/r_run/conf/r_forecasting_conf.json')
// MAGIC
// MAGIC from slack_webhook import Slack
// MAGIC
// MAGIC slackUrl=rconf['slack_url']
// MAGIC slack = Slack(url=slackUrl)
// MAGIC
// MAGIC catValidationResult = dbutils.notebook.run("./run_category_validation",240,{"env":env,"path":"dbfs:/"+env+promoPath+"/r_run/data/sorttask/TaskByCluster.csv","fcst_local_path":rconf['fcst_local_path']})
// MAGIC if catValidationResult == "Failure":
// MAGIC   slack.post(text=':awkward_seal: Category validation issue - need to investigate which categories failed.')
// MAGIC
// MAGIC
// MAGIC dbutils.fs.rm("/"+env+promoPath+"/r_run/conf/conf_fcstappend.json")
// MAGIC # needs to be created the down path
// MAGIC dbutils.fs.cp(storageConfigFile['adlsPrefix']+"/"+env+promoPath+"/r_run/conf/conf_fcstappend.json", "/"+env+promoPath+"/r_run/conf/conf_fcstappend.json")
// MAGIC fcstAppendConf = readJSONFile(inpath+'/r_run/conf/conf_fcstappend.json')
// MAGIC fcstAppendConf['fcstInput'] = rconf['fcst_local_path'][5:len(rconf['fcst_local_path'])]
// MAGIC
// MAGIC with open(inpath+'/r_run/conf/conf_fcstappend.json', 'w') as outfile:
// MAGIC   outfile.write(json.dumps(fcstAppendConf, indent=4, sort_keys=True))
// MAGIC
// MAGIC dbutils.fs.rm(storageConfigFile['adlsPrefix']+"/"+env+promoPath+"/r_run/conf/conf_fcstappend.json")
// MAGIC dbutils.fs.cp("/"+env+promoPath+"/r_run/conf/conf_weekly_all.json",storageConfigFile['adlsPrefix']+"/"+env+promoPath+"/conf/")

// COMMAND ----------

import com.databricks.backend.daemon.dbutils.FileInfo
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats
import scala.io.Source
import org.apache.spark.sql.types._
import org.apache.log4j.{Logger, PropertyConfigurator}
import scala.util.{Success, Failure, Try}

/**

e131988@heb.com
  run_weekly_post processes notebook
  prepares configuration files and copies them to respective directories
  Runs org.cognira.DailyBatch
  Performs table counts on output files
  Moves forecast_domain.csv and threshold.csv to parq files

 **/

//Creating a logger for the notebook
private val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
private val LOGGER = Logger.getLogger(s"notebook.$NOTEBOOK_NAME")

val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")

val configBasePath = "dbfs:/"
val promoPath="/pfp/"
/*
Reading the env from env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
println(environments)
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
  if(currentEnv.size == 0){
    LOGGER.error("Required environment not available")
    throw new Exception("Required environment not available")
  }
  else if(currentEnv.size > 1) {
    LOGGER.error("More than one environment detected")
    throw new Exception("More than one environment detected")
  }
  else currentEnv(0)
}

// Get the adlsPrefix
val storageConfigFile = configBasePath + env + promoPath + "/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))
// implicit lazy val formats=org.json4s.DefaultFormats

val adlsPrefix = (storageJson\"adlsPrefix").extract[String]

//Configuring log4j properties

val envPath=adlsPrefix+"/"+promoPath+env+"/"
val configFile=adlsPrefix+"/"+env+promoPath+"/conf/conf_fcstappend.json"
var json: JValue = parse(dbutils.fs.head(configFile))
// implicit lazy val formats=org.json4s.DefaultFormats
val log4jPath=(json\"log4jPath").extract[String]

Try{
  PropertyConfigurator.configure(log4jPath)
} match {
  case Success(_) => LOGGER.info(s"Using Log configuration file from dbfs location: $log4jPath for installed library")
  case Failure(_) => LOGGER.warn("No Log Configuration file found.")
}


val ind="RUN_WEEKLY_POST_NOTEBOOK"
val pidPath="/"+env+promoPath+"pid/"
val pidToCheck="RUN_DAILY_NOTEBOOK"
val pollPid:Long = 300*1000
val slackUrl=(json\"slackUrl").extract[String]
org.cognira.CommControl.setSlackUrl(slackUrl)

def pidFile(pt:String,toCheck:String):Boolean = {
  LOGGER.info(s"Checking if $toCheck is running or not")
  var exists=false
  for(i <- dbutils.fs.ls(pt)){
    if(i.name.equals(toCheck)) {
      LOGGER.debug(s"$toCheck is running")
      exists=true
    }
  }
  LOGGER.debug(s"$toCheck is not running")
  exists
}

// First, check if daily is running
while(pidFile(pidPath,pidToCheck)){
  println("Checking if okay to run in "+(pollPid/60/1000)+" mins...")
  Thread.sleep(pollPid)
}

LOGGER.info(s"Setting $ind status to RUNNING")
dbutils.fs.put("/"+env+promoPath+"pid/"+ind,"RUNNING",true)

LOGGER.info(s"Reading Config File from path: $configFile")
var userpath=""
var zone=""
var isfullweekly=""
val setupConfig=true
val checkConfig=true
val prepareEnv=true
val createDirectories=true

if(setupConfig){
  LOGGER.info("Setting up configuration")

  LOGGER.debug(s"Extracting userpath from $configFile")
  userpath=(json\"userpath").extractOrElse("")
  LOGGER.debug(s"Extracting zone from $configFile")
  zone=(json\"zone").extractOrElse("")
  LOGGER.debug(s"Extracting isfullweekly from $configFile")
  isfullweekly = (json\"is_full_weekly").extractOrElse("")
}

if(checkConfig){
  if(userpath.equals("")) {
    LOGGER.error("Empty userpath.")
    dbutils.notebook.exit("Userpath is empty, please set")
  }
}

/**
 * inputPath - should be the all_data/ directory, the collect batch step reads from "fcst_all" and "agg_all" for location.parq
 * outputPath - In production will be the same as the inputPath, could be modified if manually wanting to read/write to different env
 */
var inputPath=""
var outputPath=""

if(prepareEnv){
  LOGGER.info("Preparing environment for forecast_generation.")
  inputPath=(json\"fcstInput").extractOrElse("")
  outputPath=userpath
  if(!zone.equals("")){
    LOGGER.info(s"Preparing environment for zone = $zone")
    outputPath=userpath+"fcst_zone_"+zone+"/"
  }
}

if(createDirectories){
  LOGGER.info(s"creating $outputPath directory.")
  scala.util.Try{
    dbutils.fs.mkdirs(outputPath)
  } recover {
    case e => LOGGER.warn(s"$outputPath exists...continuing")
  }
}

scala.util.Try{
  dbutils.fs.rm(userpath+"fcst_all/forecast_domain.csv", true)
} recover {
  case e => LOGGER.debug(s"fcst_all/forecast_domain.csv did not exist, no need to remove")
}
LOGGER.info("Copy forecast domain  to " +userpath+"fcst_all/forecast_domain.csv" )
dbutils.fs.cp(userpath+"merge_all/forecast_domain.csv", userpath+"fcst_all/forecast_domain.csv", true)


val batchArgs=""
dbutils.fs.mkdirs(outputPath)

/**
 * org.cognira.forecast_generation.collectFcstInput.main
 * Runs the forecast append as well as historical_baseline/weekly_history append. Maximum 4 args
 *
 * @param hdfs_collect_input  required. the input folder containing csv files by category
 * @param hdfs_collect_output required. the output folder where files wil be written to
 * @param is_full_weekly  optional, default true. Runs the category_append for all of the various parquet files, if false only does weekly_history and historical_baseline
 * @param histbsln_acc optinal, default false. Only needed for the weekly_history and historical_baseline, if true, appends in incremental mode, else overwrites the files
 */
//  run fcst append
LOGGER.info("Running forecast append.")
//org.cognira.forecast_generation.collectFcstInput.main((inputPath+" "+outputPath).split(" "))

//  run historical_baseline/ weekly history append
LOGGER.info("Running Historical Baseline and Weekly History append.")
println(inputPath)
println(outputPath)
//org.cognira.forecast_generation.collectFcstInput.main((inputPath+" "+outputPath+" false true").split(" "))

LOGGER.info(s"Finishing $ind")
dbutils.fs.rm("/"+env+promoPath+"pid/"+ind)

// COMMAND ----------

// MAGIC %python
// MAGIC
// MAGIC '''
// MAGIC Imports, lambdas, and functions
// MAGIC SparkSession setup
// MAGIC '''
// MAGIC import os
// MAGIC import sys
// MAGIC import csv
// MAGIC import re
// MAGIC
// MAGIC from collections import defaultdict as ddict
// MAGIC from operator import itemgetter
// MAGIC from datetime import datetime
// MAGIC from random import sample as samp
// MAGIC
// MAGIC from pyspark.sql import SparkSession
// MAGIC import pyspark.sql.functions as F
// MAGIC sc = SparkSession.builder.getOrCreate()
// MAGIC
// MAGIC notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
// MAGIC pathParts = notebookPath.split("/")
// MAGIC # environments = ["dev","cert","prod"]
// MAGIC def readJSONFile(pt):
// MAGIC   with open(pt) as f:
// MAGIC     o = json.load(f)
// MAGIC   return o
// MAGIC
// MAGIC # Reading the env from env_conf.json
// MAGIC envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
// MAGIC envJson = readJSONFile(envConfigFile)
// MAGIC environments = envJson['env']
// MAGIC currentEnv = [env for env in environments if env in pathParts]
// MAGIC if len(currentEnv) == 0:
// MAGIC   print("Required environment not available")
// MAGIC   raise ValueError("Required environment not available")
// MAGIC elif len(currentEnv) > 1:
// MAGIC   print("More than one environment detected")
// MAGIC   raise ValueError("More than one environment detected")
// MAGIC else: env = currentEnv[0]
// MAGIC '''
// MAGIC Extract times from Master Node log file of R process
// MAGIC sort ascending based on elapsed time of process
// MAGIC '''
// MAGIC ##### General setup: Configurable variables for log compilation
// MAGIC
// MAGIC # PROD/CERT Configuration (env = prod or cert) -- environmental variable defined in cell above
// MAGIC configBasePath = "/dbfs/"
// MAGIC promoPath="/pfp/"
// MAGIC inpath = (configBasePath+env+'%s') % promoPath
// MAGIC # Cluster maximum size
// MAGIC clusterD = {'small': 41, 'medium': 200, 'large': 434 * .9}
// MAGIC
// MAGIC storageConfigFile = readJSONFile(configBasePath+env+promoPath+'/conf/conf_storage_prefix.json')
// MAGIC rconf = readJSONFile(configBasePath+env+promoPath+'/r_run/conf/r_forecasting_conf.json')
// MAGIC
// MAGIC ##### To display certain processing information
// MAGIC verbose = True
// MAGIC
// MAGIC ##### Sorttask directory and files used for category memory and time configuration
// MAGIC ###       1) manual op; 2) testdir; 3) production
// MAGIC ## used previously for manual runs
// MAGIC # sortdir = '/dbfs/user/w786680/tmp/d.SORTTASK'
// MAGIC ## test directory
// MAGIC # sortdir = '/dbfs/user/w786680/tmp/d.TESTSORT'
// MAGIC ## prod or cert sortdir
// MAGIC sortdir = '%s/r_run/data/sorttask' % inpath
// MAGIC
// MAGIC # memory file input to daily prep
// MAGIC outMEM = '%s/MemTimeByCat.csv' % sortdir
// MAGIC
// MAGIC # rlogdir is path to category R logs containing time and memory usage
// MAGIC rlogdir = '%s/r_forecasting_log' % rconf['fcst_local_path']
// MAGIC
// MAGIC # logdir is path to logs output by weekly <small|medium|large> notebooks
// MAGIC #    used for error analysis (memory issue)
// MAGIC logdir = '%s/log/weekly' % inpath
// MAGIC
// MAGIC # blacklist may (should?) go away...but otherwise removes certains tasks from the runlist
// MAGIC # blacklist = '%s/r_run/conf/task.blacklist' % inpath
// MAGIC
// MAGIC ### setup for category repartitioning
// MAGIC ## test rdataprep directory for development (manually modified input cat_partition file)
// MAGIC ## normal cert or prod location
// MAGIC prep = rconf['userpath']
// MAGIC #
// MAGIC catfile = '%scat_partition.csv' % prep
// MAGIC #  A new cat_partitions file is written with "current" extension to rotate_file works as written
// MAGIC #     --- probably a slack message should be written to provide visibility when needed
// MAGIC outcatfile = '%s/cat_partition.current' % sortdir
// MAGIC
// MAGIC  ## Product2 can work with parquet or csv  --  change flag to toggle
// MAGIC useparq = True
// MAGIC dbfs = prep
// MAGIC abfss_root = storageConfigFile['adlsPrefix']
// MAGIC abfss = ('%s'+'/%s'+promoPath+'all_data/agg_all') % (abfss_root, env)
// MAGIC abfss_global_static = ('%s'+'/%s'+promoPath+'global_static') % (abfss_root, env)
// MAGIC
// MAGIC prod2f = 'product2'
// MAGIC prod2csv = '%s%s.csv' % (dbfs,prod2f)    #--> removed / between %s/%s because dbfs already contain / at end
// MAGIC prod2parq = '%s/%s.parq' % (abfss,prod2f)
// MAGIC # Can use the parquet file (abfss) or the csv file (dbfs)
// MAGIC
// MAGIC ### Functions and lambdas
// MAGIC def getdf(f):
// MAGIC   fixname = lambda x: re.sub('^/dbfs', '', x)
// MAGIC   df = sc.read.csv(fixname(f), sep = '|', header = True, inferSchema=True)
// MAGIC   return df
// MAGIC
// MAGIC catfile_gs = '%s/cat_partition_final.csv' % prep
// MAGIC prod2group = sc.read.parquet(prod2parq).withColumnRenamed("id_cat","cat_id").select("cat_id","dsc_cat")  # you only need these 2 columns
// MAGIC prod2qroup = prod2group.withColumn("cat_id", prod2group.cat_id.cast('integer'))
// MAGIC catpartgroup = getdf(catfile_gs)
// MAGIC catpartgroup = catpartgroup.select("cat_id", "group")
// MAGIC gd = prod2group.join(catpartgroup, "cat_id").select("dsc_cat", "group").distinct().orderBy("dsc_cat", "group")
// MAGIC
// MAGIC from collections import defaultdict as ddict
// MAGIC groupD = ddict(list)
// MAGIC for k,v in gd.toPandas().values.tolist():
// MAGIC   groupD[k].append(v)
// MAGIC
// MAGIC print("This is the groupD dictionary used:")
// MAGIC print(groupD)

// COMMAND ----------

// MAGIC %python
// MAGIC
// MAGIC ### Known errors which could be parsed, many of these should nolonger exist.
// MAGIC RLOG_ERRORS = {'Warning - Program will fail: sales data has 0 records',
// MAGIC                'Warning - Program will fail: ModelInputData has 0 records for subclass model'
// MAGIC               }
// MAGIC
// MAGIC LOG_ERRORS = {"Error in bmerge(i, x, leftcols, rightcols, xo, roll, rollends, nomatch, : typeof x.store (logical) != typeof i.store (integer)",
// MAGIC               "Error in bmerge(i, x, leftcols,rightcols, xo, roll, rollends, nomatch, : typeof x.store (logical) != typeof i.store (integer)",
// MAGIC               "Error in bmerge(i, x,leftcols, rightcols, xo, roll, rollends, nomatch, : typeof x.store (logical) != typeof i.store (integer)",
// MAGIC               "Error in eval(bysub, x, parent.frame()): object 'sku' not found",
// MAGIC               "Error in if (inactivity > SKU_THRESHOLD$inactivity) {: argument is of length zero",
// MAGIC               "Error in print.default(message_out): ignoring SIGPIPE signal" ,
// MAGIC               "cannot popen 'free|sed -n 2p|awk '{print $7}'', probable reason 'Cannot allocate memory'",
// MAGIC               "cannot allocate vector of size"
// MAGIC              }
// MAGIC
// MAGIC ### Memory errors (from LOG_ERRORS) can be dealt with
// MAGIC MEM_ERRORS = {"Error in print.default(message_out): ignoring SIGPIPE signal" ,
// MAGIC               "probable reason 'Cannot allocate memory'",
// MAGIC               "cannot allocate vector of size"
// MAGIC              }
// MAGIC
// MAGIC def proclog(d, f, warnD):
// MAGIC     '''
// MAGIC     Reads rlog and extracts time and size of process
// MAGIC     uses readlog
// MAGIC     maxmem <- max_mem (maximum size of R process)
// MAGIC     maxsize <- free_mem + mem (resident memory)
// MAGIC     '''
// MAGIC     D = {}
// MAGIC     header, dataL = readlog('%s/%s' % (d,f))
// MAGIC     itime = header.index('date')
// MAGIC     dataA = []
// MAGIC     for data in dataL:
// MAGIC         try:
// MAGIC             t1 = data[0][itime]
// MAGIC         except IndexError:
// MAGIC             continue
// MAGIC         lastline = dict(zip(header,data[-1]))
// MAGIC         xtime = getdiff(t1,lastline['date'])
// MAGIC         category = lastline['category']
// MAGIC         maxmem = int(lastline['max_mem'])
// MAGIC         complete = lastline['message'] == 'Completed all processing'
// MAGIC         # calculate maximum available memory of the worker
// MAGIC         # -- this is the maximum sum of current used and available memory
// MAGIC         ifree = header.index('free_mem')
// MAGIC         imem = header.index('mem')
// MAGIC         maxsize = max(int(x[ifree]) + int(x[imem]) for x in data)
// MAGIC         dataA.append((maxmem,xtime,maxsize,complete))
// MAGIC     dataT = list(zip(*dataA))
// MAGIC     maxmem = max(dataT[0])
// MAGIC     xtime = max(dataT[1])
// MAGIC     maxsize = min(dataT[2])
// MAGIC     complete = any(dataT[3])
// MAGIC     return (category, int(maxmem), xtime, maxsize, complete)
// MAGIC
// MAGIC def caltimes(L):
// MAGIC     D = {}
// MAGIC     for i,logname in enumerate(L):
// MAGIC         f = '%s/%s/%s' % (logparent, logdir, logname)
// MAGIC         unit = open(f)
// MAGIC         # skip header
// MAGIC         header = next(unit)
// MAGIC         # sys.exit()
// MAGIC         L = (x.strip().split('|') for x in unit)
// MAGIC         timeL = ((x, getdiff(y,z)) for x,y,z in ((ext(r) for r in L if r[-1] == '0')))
// MAGIC         D[i] = timeL
// MAGIC     return D
// MAGIC
// MAGIC def readlog(f):
// MAGIC     '''
// MAGIC     read log and return header and data
// MAGIC     if a log is contains multiple runs, extract add runs, this occurs:
// MAGIC         --sometimes with recoverable failure or prexisting log
// MAGIC         --sometimes with a bad run after good run
// MAGIC     '''
// MAGIC     D = {}
// MAGIC     dataL = []
// MAGIC     unit = open(f)
// MAGIC
// MAGIC     data = [x.strip().split('|') for x in unit]
// MAGIC     header = data[0]
// MAGIC     check = header[0]
// MAGIC     checkL = [i for i,x in enumerate(data) if x[0] == check]
// MAGIC     dataL = [data[x+1:y] for x,y in zip(checkL,checkL[1:])]
// MAGIC     dataL.append(data[checkL[-1] + 1:])
// MAGIC
// MAGIC     return header, dataL
// MAGIC
// MAGIC def getdiff(a,b):
// MAGIC     '''
// MAGIC     L is a pair of formatted times
// MAGIC     returns seconds between the two times
// MAGIC     '''
// MAGIC     times = [getsec(x) for x in (a,b)]
// MAGIC     return int((times[1] - times[0]))
// MAGIC
// MAGIC def makedir(outdir):
// MAGIC     try:
// MAGIC         os.mkdir(outdir)
// MAGIC         print('Made directory %s' % outdir)
// MAGIC     except FileExistsError:
// MAGIC         # if directory already exists, ignore error and continue
// MAGIC         pass
// MAGIC
// MAGIC def writecsv(f, L, header = None):
// MAGIC     # This writes output with unix eol not windoze eol
// MAGIC     #    uses pipe '|' as delimiter
// MAGIC     tostring = lambda row: '%s\n' % '|'.join([str(x) for x in row])
// MAGIC     makedir(os.path.dirname(f))
// MAGIC     outL = (tostring(x) for x in L)
// MAGIC     unit = open(f, 'w')
// MAGIC     if header:
// MAGIC        unit.write(tostring(header))
// MAGIC     unit.writelines(outL)
// MAGIC     unit.close()
// MAGIC     del unit
// MAGIC
// MAGIC def sparkgetlist(f):
// MAGIC     fixname = lambda x: re.sub('^/dbfs', '', x)
// MAGIC     df = sc.read.csv(fixname(f), sep='|', header=True, inferSchema=True)
// MAGIC     return (df.schema.names, (tuple(x) for x in df.collect()))
// MAGIC
// MAGIC def rotatefile(name):
// MAGIC     '''
// MAGIC     rotates filenames to keep older versions
// MAGIC     due to filecatching, this version uses a read/overwrite strategy rather than remove/rename
// MAGIC     '''
// MAGIC     import os
// MAGIC     import csv
// MAGIC     base = os.path.basename(name)
// MAGIC     basenm = '.'.join(name.split('.')[:-1])
// MAGIC
// MAGIC     # extension list, last to first
// MAGIC     extensions = ('oldest', 'older', 'old', 'current')
// MAGIC
// MAGIC     rn = lambda S,ext: '%s.%s' % (S, ext)
// MAGIC
// MAGIC     # rename extensions
// MAGIC     for a,b in zip(extensions[1:],extensions):
// MAGIC         try:
// MAGIC             with open(rn(basenm,a)) as filein:
// MAGIC                 reader = csv.reader(filein, lineterminator='\n')
// MAGIC                 with open(rn(basenm,b), 'w') as fileout:
// MAGIC                     writer = csv.writer(fileout, lineterminator = '\n')
// MAGIC                     writer.writerows(reader)
// MAGIC             print('File %s is renamed to %s' % (rn(basenm,a) , rn(basenm, b)))
// MAGIC         except FileNotFoundError:
// MAGIC             print('File %s is not found' % rn(basenm, a))
// MAGIC
// MAGIC     return name
// MAGIC
// MAGIC ### Lambdas
// MAGIC # Extracts 3 predetermined items from list -- "slow" but short list
// MAGIC ext = lambda L: (L[0],L[2],L[3])
// MAGIC # gets seconds from epoch
// MAGIC getsec = lambda t: datetime.strptime(t, '%Y-%m-%d %H:%M:%S').timestamp()

// COMMAND ----------

// MAGIC %python
// MAGIC # This is to read previous data (MemCatByTime from the sorttask directory)
// MAGIC poptime = ddict(list)
// MAGIC popsize = ddict(list)
// MAGIC
// MAGIC # I do not want the actual MemCatByTime.csv file which compiles multiple runs
// MAGIC flist = [x for x in os.listdir(sortdir) if x.startswith('Mem') and not x.endswith('csv')]
// MAGIC for f in flist:
// MAGIC     unit = open('%s/%s' % (sortdir,f))
// MAGIC     reader = csv.reader(unit)
// MAGIC     # no header
// MAGIC     #header = next(reader)
// MAGIC     #taskdat = dict((x[1],x[0]) for  in reader)
// MAGIC     trash = [(popsize[k].append(int(s)),poptime[k].append(int(t))) for k,s,t in reader]
// MAGIC
// MAGIC L = []
// MAGIC for k,v in poptime.items():
// MAGIC   L.append(len(v))
// MAGIC
// MAGIC if verbose:
// MAGIC   try:
// MAGIC     print('Using %s results from %s previous runs' % (len(L), max(L)))
// MAGIC   except:
// MAGIC     print('Starting with no previous MemTimeByCat files in %s' % sortdir)
// MAGIC
// MAGIC # Read current log files
// MAGIC flist = [x for x in os.listdir(rlogdir) if x.endswith('txt')]
// MAGIC resultL = []
// MAGIC
// MAGIC ## Read all logs
// MAGIC # warnD is populated by proclog with any warnings in rlog file
// MAGIC warnD = ddict(list)
// MAGIC
// MAGIC ## Why sample?? Just love to randomize when not too important
// MAGIC for f in samp(flist,len(flist)):
// MAGIC     resultL.append(proclog(rlogdir, f, warnD))
// MAGIC
// MAGIC # Add new data to poptime and popsize
// MAGIC trash = [(popsize[k].append(s),poptime[k].append(t)) for k,s,t in [x[:3] for x in resultL]]
// MAGIC
// MAGIC ## Show failures
// MAGIC # last element in list designates success or failure of category
// MAGIC #    last record of log contains 'Completed all'
// MAGIC checkL = [x for x in resultL if not x[-1]]
// MAGIC print( 'There are %s logfiles %s of which are incomplete runs' % (len(resultL), len(checkL)))
// MAGIC for x in checkL:
// MAGIC     print(x)
// MAGIC
// MAGIC # If incomplete runs, check on cause
// MAGIC #  if memory:
// MAGIC #     add additional one-time configuration
// MAGIC #         popsize (important to move category to next larger size cluster)
// MAGIC #         poptime (not so important -- time already present from logs)
// MAGIC #  ...
// MAGIC
// MAGIC runlogdir = '%s/log/weekly' % inpath
// MAGIC errorcatD = {x[0].replace('/', '-'):x[0] for x in checkL}
// MAGIC upclusterL = []
// MAGIC for path,dirL,files in os.walk(runlogdir):
// MAGIC     cluster = path.split('/')[-1]
// MAGIC     if cluster in {'small', 'medium', 'large'}:
// MAGIC         for f in files:
// MAGIC             catnm = f.split('.')[0].replace('rforecasting_', '')
// MAGIC             if catnm in errorcatD:
// MAGIC                 unit = open('%s/%s' % (path,f))
// MAGIC                 try:
// MAGIC                     msg = [x.strip() for x in unit if x.startswith('error')][-1]
// MAGIC                 except IndexError:
// MAGIC                     continue
// MAGIC                 if any(x in msg for x in MEM_ERRORS):
// MAGIC                     cat = errorcatD[catnm]
// MAGIC                     print('Category %s had memory issue, moving to next size cluster' % cat)
// MAGIC                     usesize = clusterD[cluster]*1024**2 + 7
// MAGIC                     popsize[cat].append(usesize)
// MAGIC                     poptime[cat].append(3)

// COMMAND ----------

// MAGIC %python
// MAGIC ### Check for new categories --
// MAGIC ###     if so, run splitgroup
// MAGIC ###     Also: output file (in sorttask) will need to go to global_static repository for deployment
// MAGIC ###          ...Need alert mechanism -- Slack???
// MAGIC
// MAGIC cat_head, cat_data = sparkgetlist(catfile)
// MAGIC cat_data = set(cat_data)
// MAGIC newgrps = [x for x in cat_data if x[2] == None]
// MAGIC splitgrps = set(x[0] for x in newgrps)
// MAGIC print( {0:'No new groups, splitgroup skipped'} \
// MAGIC      .get(len(splitgrps), 'There are new subcommodites, splitting will occur')
// MAGIC      )
// MAGIC
// MAGIC if splitgrps:
// MAGIC     # split category cell
// MAGIC     ## Read and compile product2 file and split predefined category into partitions
// MAGIC     #slack.post(text='There are NEW subcommodites. Therefore a new cat_partion.current has been generated and it needs to added to the repo.')
// MAGIC     print("There are NEW subcommodites. Therefore a new cat_partion.current has been generated and it needs to added to the repo.")  #Done by us change as above
// MAGIC     if useparq:
// MAGIC         prod2 = sc.read.parquet(prod2parq)
// MAGIC     else:
// MAGIC         prod2 = getdf(prod2csv)
// MAGIC
// MAGIC     prod2 = prod2.where(F.col('id_cat').isin(splitgrps))
// MAGIC     print('There are %s records in product2 with categories: %s' % (prod2.persist().count(),splitgrps))
// MAGIC
// MAGIC     # Make dictionary from prod2!!!
// MAGIC     prodCats = prod2.where(F.col('id_cat').isin(splitgrps)) \
// MAGIC         .select(['id_cat', 'dsc_cat', 'dsc_sub_comm']) \
// MAGIC         .groupBy('id_cat', 'dsc_cat', 'dsc_sub_comm') \
// MAGIC         .count().select('count', 'dsc_sub_comm', 'id_cat', 'dsc_cat') \
// MAGIC         .orderBy(F.desc('count')) \
// MAGIC         .toPandas()
// MAGIC
// MAGIC
// MAGIC     catLUT = dict(prodCats[['id_cat', 'dsc_cat']].values.tolist())
// MAGIC
// MAGIC     print(catLUT)
// MAGIC
// MAGIC     # setup assignment of newly split categories to the "large" cluster
// MAGIC     partitions = []
// MAGIC     usesize = clusterD['medium']*1024**2 + 17
// MAGIC     for cat in splitgrps:
// MAGIC         print("Processing category %s with id %s" % (catLUT[cat],cat))
// MAGIC         groups = groupD[catLUT[cat]]
// MAGIC         sumD = {x:0 for x in groups}
// MAGIC         L = prodCats[prodCats.id_cat == cat].values
// MAGIC         for r in L:
// MAGIC             out = sorted(sumD.items(),key = itemgetter(1))[0][0]
// MAGIC             sumD[out] += int(r[0])
// MAGIC             partitions.append([r[2], r[1], out])
// MAGIC         ## Add new categories to popsize and poptime (both required as there may be no entry.
// MAGIC         ##     This changes MemCa
// MAGIC         for name in ('%s%s' % (catLUT[cat],x) for x in sumD):
// MAGIC             popsize[name].append(usesize)
// MAGIC             poptime[name].append(17)
// MAGIC         print(sumD,cat)
// MAGIC
// MAGIC     # split category cell
// MAGIC     # Update cat_partition list with new splits on selected categories
// MAGIC     newcatL = partitions[:]
// MAGIC     idSet = set(list(zip(*newcatL))[0])
// MAGIC     if verbose:
// MAGIC         print("===========Sampling new cat_partition file for sanity's sake=========")
// MAGIC         sampsize = min(10,len(newcatL))
// MAGIC         for r in samp(newcatL,sampsize):
// MAGIC             print(r)
// MAGIC
// MAGIC     newcatL.extend(x for x in cat_data if x[0] not in idSet)
// MAGIC     newcatL.sort(key = itemgetter(0,2,1))
// MAGIC
// MAGIC     writecsv(rotatefile(outcatfile), newcatL, header=cat_head)
// MAGIC else:
// MAGIC     # Right to slack that no category splits happened
// MAGIC     slack.post(text='There are NO new subcommodites, so the cat_partition.current did not change.')

// COMMAND ----------

// MAGIC %python
// MAGIC # Sort list by memory size -- drop incomplete flag
// MAGIC #     ---"outnow": Mem/Time results from just completed week
// MAGIC #     ---"newlist": created from poptime and popsize which contain a dictionary keyed by
// MAGIC #                   category with values containing a list of time and memory size
// MAGIC # Output files to sorttask directory
// MAGIC
// MAGIC sortedlist = [tuple(x[:3]) for x in sorted(resultL, key=itemgetter(1))]
// MAGIC
// MAGIC # Output slack message with the 5 largest categories and their memory size in GB insead of Kilobytes
// MAGIC catmsg = '```Top 10 Categories based on Memory Use: \n'
// MAGIC for v in reversed(sortedlist[-10:]):
// MAGIC     catmsg = catmsg + str(round(v[1] / (1024 * 1024))) + " GB     " + v[0] + "\n"
// MAGIC catmsg = catmsg + "```"
// MAGIC slack.post(text=catmsg)
// MAGIC
// MAGIC
// MAGIC try:
// MAGIC     os.mkdir(sortdir)
// MAGIC     print('Made directory %s' % sortdir)
// MAGIC except FileExistsError:
// MAGIC     # if directory already exists, ignore error and continue
// MAGIC     pass
// MAGIC
// MAGIC # Write current list (last week's run)
// MAGIC #    rotate input files
// MAGIC outnow = '%s.current' % outMEM
// MAGIC with open(rotatefile(outnow), 'w') as f:
// MAGIC     writer = csv.writer(f, lineterminator='\n')
// MAGIC     writer.writerows(sortedlist)
// MAGIC
// MAGIC # Write newlist (picking maximum values: slowest time, largest memory usage) for use in next weekly run
// MAGIC #    ---sort by memory
// MAGIC newlist = sorted(((k,max(popsize[k]),max(poptime[k])) for k in popsize), key=itemgetter(1))
// MAGIC
// MAGIC
// MAGIC if verbose:
// MAGIC     print('Three smallest categories: %s' % newlist[:3])
// MAGIC     print('Three largest categories: %s' % newlist[-3:])
// MAGIC
// MAGIC # outMEM is the compilated MemTieByCat.csv used in production
// MAGIC with open(outMEM, 'w') as f:
// MAGIC     writer = csv.writer(f, lineterminator='\n')
// MAGIC     writer.writerows(newlist)
// MAGIC
// MAGIC ### Display some output one more time for fun
// MAGIC if verbose:
// MAGIC     ## This will show a random line in the output MemTimeByCat.csv
// MAGIC     ##    as well the data from which the are derived.
// MAGIC     l = samp(sortedlist,1)[0]
// MAGIC     print('Current: %s' % list(l))
// MAGIC     k = l[0]
// MAGIC     print('For prod: %s' % list([x for x in newlist if x[0] == k][0]))
// MAGIC     print('Input data: %s, %s %s' % (k, popsize[k], poptime[k]))

// COMMAND ----------

// MAGIC  %python
// MAGIC import json, sys
// MAGIC import os
// MAGIC import csv
// MAGIC import numpy
// MAGIC import pandas
// MAGIC import glob
// MAGIC import datetime
// MAGIC from pyspark.sql.functions import *
// MAGIC
// MAGIC xlong    = False            # True will use the LONGITUDINAL datatset. False will use the parq weekly file instead.
// MAGIC xlastr2  = '2020-01-20'    # This applies when (xlong = True) only
// MAGIC xdatetag = datetime.date.today().strftime("%Y%m%d")
// MAGIC xpath    = configBasePath + env + promoPath +'r_run/data/new_item_baseline/'
// MAGIC srcPath= "/" + env + promoPath + 'r_run/data/new_item_baseline/'
// MAGIC targetBaselineFile = abfss_root + "/" + env+ promoPath  + 'all_data/fcst_override/new_item.csv'
// MAGIC
// MAGIC # ==================================== LONGITUDINAL DATASET ==========================================================================
// MAGIC #dow_profile = "abfss://adlsfilesystem@hebdatalake01.dfs.core.windows.net/encdata/data/pna/scm/sup_chain_serve/dow_profile_serve"
// MAGIC #store_promo_forecast = "abfss://adlsfilesystem@hebdatalake01.dfs.core.windows.net/encdata/data/pna/scm/sup_chain_serve/store_promo_forecast_serve"
// MAGIC #promo_forecast_coeffs = "abfss://adlsfilesystem@hebdatalake01.dfs.core.windows.net/encdata/data/pna/scm/sup_chain_serve/promo_forecast_coeffs_serve"
// MAGIC #weekly_forecast_history = "abfss://adlsfilesystem@hebdatalake01.dfs.core.windows.net/encdata/data/pna/scm/sup_chain_serve/weekly_forecast_history_serve"
// MAGIC #r2_fitting = "abfss://adlsfilesystem@hebdatalake01.dfs.core.windows.net/encdata/data/pna/scm/sup_chain_serve/r2_fitting"
// MAGIC # ==================================== LONGITUDINAL DATASET ==========================================================================
// MAGIC
// MAGIC if True :
// MAGIC   # *** Automatic HDFS Method to get Brand New Products
// MAGIC   myfile = abfss_root+"/"+env+promoPath+'all_data/agg_all/sales_estimate.parq'
// MAGIC   sdfbase = spark.read.parquet(myfile)
// MAGIC
// MAGIC if xlong :
// MAGIC   r2fit = "s3://heb-spme-far-promo-prd-serve/PHAS1/pfp/pna/scm/sup_chain_serve/r2_fitting/"
// MAGIC   sdftot = spark.read.parquet(r2fit)
// MAGIC   sdftot = sdftot.select(['sku','store','week','weekly_backcast','units','discount_type','naive_fcst','run_date']).withColumnRenamed('weekly_backcast', 'forecast').where(col('run_date') == xlastr2)
// MAGIC else :
// MAGIC   r2fit = abfss_root+"/"+env+promoPath+"all_data/fcst_all/r2fitting.parq"
// MAGIC   sdftot = spark.read.parquet(r2fit)
// MAGIC   sdftot = sdftot.select(['sku','store','week','sku_daily_forecast','units','discount_type','naive_fcst']).withColumnRenamed('sku_daily_forecast','forecast')
// MAGIC
// MAGIC sdftot = sdftot.fillna(0, ['units','forecast']).where(col('units') > 0)
// MAGIC sdftot = sdftot.groupBy('sku','week').agg(sum('units'), count('*'), sum('forecast'))
// MAGIC sdftot = sdftot.withColumnRenamed('sum(units)', 'units').withColumnRenamed('count(1)', 'nstores').withColumnRenamed('sum(forecast)', 'forecast')
// MAGIC
// MAGIC sdftot = sdftot.groupBy('sku').agg(mean('units'), max('units'), count('*'), mean('forecast'), max('forecast'), mean('nstores'), max('nstores'))
// MAGIC sdftot = sdftot.withColumnRenamed('avg(units)', 'units_mean').withColumnRenamed('max(units)', 'units_max').withColumnRenamed('count(1)', 'nweeks').withColumnRenamed('avg(forecast)', 'forecast_mean').withColumnRenamed('max(forecast)', 'forecast_max').withColumnRenamed('avg(nstores)', 'nstores_mean').withColumnRenamed('max(nstores)', 'nstores_max')
// MAGIC
// MAGIC sdftot = sdftot.withColumn('newfcst', round(col('units_mean') * col('nstores_max') / col('nstores_mean'),2))
// MAGIC #print(sdftot.columns)
// MAGIC #sdftot.write.csv('/dbfs/FileStore/PFP-BLINE/HBASELINE.csv', sep=',', mode='overwrite')
// MAGIC totdf = sdftot.toPandas()
// MAGIC totdf.to_csv(xpath + 'HBASELINE-' + xdatetag + '.csv',index=None)
// MAGIC
// MAGIC # *** FINAL MERGE (sdfbase, sdftot) ***
// MAGIC sdfmerg = sdfbase.join(sdftot, ['sku'], 'left')
// MAGIC
// MAGIC if False :
// MAGIC   # *** ISFAR-2668
// MAGIC   dfmerg  = dfmerg[(dfmerg['nweeks'] >= 1)]
// MAGIC
// MAGIC sdfmerg = sdfmerg.fillna(1, ['newfcst']).drop('avg_estimate','subclass','nweeks','nstores_mean','nstores_max','units_mean','units_max','forecast_mean','forecast_max')
// MAGIC
// MAGIC #sdfmerg.select('newfcst').distinct().show()
// MAGIC #sdfmerg.write.csv('/dbfs/FileStore/PFP/hbaseline-fcst-' + xdatetag + '.csv', sep=',', mode='overwrite')
// MAGIC dfmerg = sdfmerg.toPandas()
// MAGIC outputFile=xpath + 'hbaseline-fcst-' + xdatetag + '.csv'
// MAGIC srcPath=srcPath + 'hbaseline-fcst-' + xdatetag + '.csv'
// MAGIC dfmerg.to_csv(outputFile,index=None)
// MAGIC
// MAGIC print("Moving "+srcPath + " to "+targetBaselineFile)
// MAGIC
// MAGIC spark.read.csv(srcPath).count
// MAGIC
// MAGIC dbutils.fs.rm(targetBaselineFile)
// MAGIC dbutils.fs.cp(srcPath,targetBaselineFile)
// MAGIC
// MAGIC # Triggering the run_longitudinal notebook with date as a parameter
// MAGIC run_longitudinal_process = dbutils.notebook.run("./run_longitudinal_process", 7*24*60*60, {"date": xdatetag})
// MAGIC if run_longitudinal_process == "Failure":
// MAGIC   slack.post(text='Failed to trigger run_longitudinal_process notebook from weekly_post.')

// COMMAND ----------


