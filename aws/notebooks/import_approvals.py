
import json

def readJSONFile(pt):
  with open(pt) as f:
    o = json.load(f)
  return o

notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
print(notebookPath)
pathParts = notebookPath.split("/")
print(pathParts)

config_base_path = "/dbfs/"
promo_path = "/pfp"

# Reading env from env_conf.json
envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
envJson = readJSONFile(envConfigFile)
environments = envJson['env']
currentEnv = [env for env in environments if env in pathParts]
if len(currentEnv) == 0:
  raise ValueError("Required environment not available")
elif len(currentEnv) > 1:
  raise ValueError("More than one environment detected")
else: env = currentEnv[0]

# Reading the connection configurations from json file
conf = config_base_path + env + promo_path + "/conf/db_connection.json"
confJson = readJSONFile(conf)
jdbc_url = confJson['jdbcUrl']
user = dbutils.secrets.get(scope = confJson['scope'] , key = confJson['userKey'])
password = dbutils.secrets.get(scope = confJson['scope'] , key = confJson['passwordKey'])

dbutils.library.installPyPI('slack-webhook')
from pyspark.sql.functions import *
from pyspark.sql.types import *
import logging

#Setting-up Logger
format = "%(asctime)s: %(message)s"
logging.getLogger("py4j").setLevel(logging.ERROR)
logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

driver = "oracle.jdbc.driver.OracleDriver"
schema = "FM"
approvalsTable = "STG_APPROVALS"
approvalsPath = config_base_path + env + promo_path + "/all_data/fm_ui/approvals.parq"

#Loading data from db
def load_data(jdbc_url, driver, schema, table, user,password):
  return spark.read \
  .format("jdbc") \
  .option("url", jdbc_url) \
  .option("driver", driver) \
  .option("dbtable", f"{schema}.{table}") \
  .option("user", user) \
  .option("password", password) \
  .load()

try:
  df = load_data(jdbc_url, driver, schema, approvalsTable, user, password)
except:
  dbutils.notebook.exit("Connection Error")


logging.info(f"Updating Approvals Datatypes for writing to {approvalsPath}")
transformedDf = df.withColumn("week", to_timestamp(unix_timestamp(col("week"))+ 6*60*60*1000/1000).cast(DateType()))\
  .withColumn("WEEK",col("week").cast(TimestampType())) \
  .withColumn("USER_APPROVED_DATE", col("USER_APPROVED_DATE").cast(TimestampType())) \
  .withColumn("BASE_FRCST_UNITS", col("BASE_FRCST_UNITS").cast(StringType())) \
  .withColumn("LAST_APPROVED_FORECAST", col("LAST_APPROVED_FORECAST").cast(StringType())) \
  .withColumn("APPROVED_SYSTEM_FORECAST", col("APPROVED_SYSTEM_FORECAST").cast(StringType())) \
  .withColumn("LEAD_TIME", col("LEAD_TIME").cast(StringType()))


# writing to approval.parq file
logging.info(f"Writing approvals with updated datatypes to {approvalsPath}")
while True:
  try:
    transformedDf.write \
     .mode("overwrite") \
     .parquet(approvalsPath.replace("/dbfs",""))
  except:
    logging.error(f"Error while inserting to {approvalsPath} file.")
    continue
  break


#Testing for data-mismatch
def compareDfs(expectedDf, actualDf):
  diff1 = expectedDf.subtract(actualDf)
  diff1_count = diff1.count()

  diff2 = actualDf.subtract(expectedDf)
  diff2_count = diff2.count()

  if diff1_count == 0 and diff2_count == 0 :
    return logging.info("Both dataframes have the same data")
  else:
    return logging.info("Data Mismatch.!")

# Comparing Dfs
expectedDf_tbl = df.agg(sum(col("BASE_FRCST_UNITS")).alias("BASE_FRCST_UNITS_SUM"))
actualDf_parquet = spark.read.parquet(approvalsPath.replace("/dbfs","")).withColumn("BASE_FRCST_UNITS",col("BASE_FRCST_UNITS").cast("decimal(38,10)")).agg(sum(col("BASE_FRCST_UNITS")).alias("BASE_FRCST_UNITS_SUM"))
compareDfs(expectedDf_tbl, actualDf_parquet)
