# Databricks notebook source
# MAGIC %md
# MAGIC Script to take given jira information and create a ticket via rest API
# MAGIC ### Example Usage
# MAGIC ```
# MAGIC projectKey = "KNOLDUS"
# MAGIC summary = "Automation Testing"
# MAGIC description = "Creating of an issue using project keys and issue type names using the REST API"
# MAGIC issueType = "Story"
# MAGIC 
# MAGIC dbutils.notebook.run("./jira_ticket_creator",7*24*60*60,{"PROJECTKEY":projectKey,"SUMMARY":summary,"DESCRIPTION":description,"ISSUETYPE": issueType})
# MAGIC ```

# COMMAND ----------

projectKey = dbutils.widgets.get("PROJECTKEY")
summary = dbutils.widgets.get("SUMMARY")
description = dbutils.widgets.get("DESCRIPTION")
issueType = dbutils.widgets.get("ISSUETYPE")
slackUrl = dbutils.widgets.get("SLACKURL")

# COMMAND ----------

import http.client
import json

jiraToken = dbutils.secrets.get(scope = "AWS-HSFP-PRD-PFP-SCOPE-001", key = "SRTS-JIRA-PRD-TOKN")

conn = http.client.HTTPSConnection("hebecom.atlassian.net")
payload = json.dumps({
  "fields": {
    "project": {
      "key": projectKey
    },
    "summary": summary,
    "description": description,
    "issuetype": {
      "name": issueType
    },
    "components": [
      {
        "name": "AWS"
      },
      {
        "name":"PFP"
      }
    ],
    "priority": {
      "name": "Highest"
    },
    "labels":[
      "notebook_failure"
    ]
  }
})
headers = {
  'Authorization': 'Basic ' + jiraToken,
  'Content-type': 'application/json'
}
conn.request("POST", "/rest/api/2/issue/", payload, headers)
res = conn.getresponse()
data = json.loads(res.read().decode("utf-8"))
link = "https://hebecom.atlassian.net/browse/" + data["key"]
print(link)

# COMMAND ----------

dbutils.library.installPyPI('slack-webhook')
from slack_webhook import Slack

msg = link + "\n" + description

slack = Slack(url=slackUrl)
slack.post(text=msg)
