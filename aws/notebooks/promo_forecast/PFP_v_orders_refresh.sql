-- Databricks notebook source
create table if not exists promo_decision_support.v_orders (
id_lob int,
dsc_lob string,
id_rgn int,
dsc_rgn string,
id_dist int,
dsc_dist string,
id_str int,
sw_str_like_lst_yr string,
dsc_retl_loc_frmt string,
dsc_retl_loc_segm string,
ID_SHOPPING_TRANS decimal(15,0),
ecom_ord_id string,
ID_UPC decimal(17,0),
id_prod bigint,
sw_ex string,
ID_TIME_PART smallint,
KEY_STORE int,
bi_prc_typ_cd string,
AMT_NET_SLS_REV decimal(9,2),
QTY_NET_PKG_ITMS decimal(15,4),
QTY_NET_UNTS_SOLD decimal(15,4),
AMT_NET_CST decimal(9,2),
AMT_EXT_REG_RETL_PRC decimal(15,4),
AMT_EXT_EXPCT_RETL_PRC decimal(15,4),
ID_PRC_TYP int,
SW_ON_AD string,
SW_SOLD_BY_WT string,
pss_subdept_id smallint,
AMT_NET_PRTNR_DISC decimal(9,2),
D_STMP_TAX_TYP_CD string,
channel string,
flflmt_channel string,
flfmt_by string,
sw_bm int,
sw_hebcom tinyint,
sw_bloomcc tinyint,
sw_bloomftd tinyint,
sw_bloomcom tinyint,
sw_instacart tinyint,
sw_shipt tinyint,
sw_pcr tinyint,
sw_vpp tinyint,
sw_dcc tinyint,
sw_ccrn tinyint,
sw_hebgo tinyint,
sw_curb_dlvr tinyint,
sw_curb_pickup tinyint,
sw_cm_curb_dlvr tinyint,
sw_cm_curb_pickup tinyint,
sw_cm_holiday tinyint,
sw_wic tinyint,
sw_ebt tinyint,
sw_favor tinyint,
pos_rgstr_grp_des string,
pos_rgstr_des string,
ts_ld timestamp,
net_spend_bskt decimal(20,4),
heb_coup_amt_bskt decimal(20,4),
instr_disc_amt_bskt decimal(20,4),
mfg_coup_amt_bskt decimal(20,4),
net_spend_itm decimal(20,4),
heb_coup_amt_itm decimal(20,4),
instr_disc_amt_itm decimal(20,4),
mfg_disc_amt_itm decimal(20,4),
sw_tpr int,
sw_str_ovrd int,
sw_byr_mrdn int,
sw_ad int,
sw_prtnr_disc int,
id_fscl_yr smallint,
id_fscl_qtr int,
id_fscl_prd int,
id_fscl_wk int,
ID_DATE string) using parquet;

-- COMMAND ----------

insert overwrite table promo_decision_support.v_orders SELECT id_lob, dsc_lob, id_rgn, dsc_rgn, id_dist, dsc_dist, id_str, sw_str_like_lst_yr, dsc_retl_loc_frmt, dsc_retl_loc_segm, sals_tran_id ID_SHOPPING_TRANS, ecom_ord_id, consm_unt_id ID_UPC, id_prod, sw_ex, tm_prt_id ID_TIME_PART, str_loc_id KEY_STORE, bi_prc_typ_cd, ext_retl_prc_amt AMT_NET_SLS_REV, pkg_items_qty QTY_NET_PKG_ITMS, net_units_sold_qty QTY_NET_UNTS_SOLD, ext_cst_amt AMT_NET_CST, ext_reg_retl_prc_amt AMT_EXT_REG_RETL_PRC, ext_expct_retl_prc_amt AMT_EXT_EXPCT_RETL_PRC, prc_typ_id ID_PRC_TYP, on_ad_sw SW_ON_AD, var_wt_sw SW_SOLD_BY_WT, pss_subdept_id, prtnr_disc_amt AMT_NET_PRTNR_DISC, fd_stmp_tax_typ_cd D_STMP_TAX_TYP_CD, channel, flflmt_channel, CASE WHEN channel = 'B&M' THEN 'Store' WHEN flflmt_channel = 'Curbside' THEN 'Curbside' WHEN (sw_bloomcc = 1 OR sw_bloomcom = 1) THEN 'Bloom Delivery' WHEN sw_instacart = 1 THEN 'Instacart' WHEN sw_shipt = 1 THEN 'Shipt' WHEN sw_favor = 1 THEN 'Favor' WHEN sw_bloomftd = 1 THEN 'FTD' WHEN flflmt_channel = 'Delivery' THEN 'Delivery' WHEN flflmt_channel = 'Store Pickup' THEN 'Make Ready' WHEN flflmt_channel = 'Ship-to-Home' THEN 'Ship To Home' ELSE 'Store' END flfmt_by, CASE WHEN channel = 'B&M' THEN 1 ELSE 0 END sw_bm, sw_hebcom, sw_bloomcc, sw_bloomftd, sw_bloomcom, sw_instacart, sw_shipt, sw_pcr, sw_vpp, sw_dcc, sw_ccrn, sw_hebgo, sw_curb_dlvr, sw_curb_pikcup sw_curb_pickup, sw_cm_curb_dlvr, sw_cm_curb_pkp sw_cm_curb_pickup, sw_cm_holiday, sw_wic, sw_ebt, sw_favor, pos_rgstr_grp_des, pos_rgstr_des, ts_ld, tot_aftr_disc_amt net_spend_bskt, heb_coup_amt_bskt, instr_disc_amt_bskt, mfg_coup_amt_bskt, net_spend_itm, heb_coup_amt_itm, instr_disc_amt_itm, mfg_disc_amt_itm, CASE WHEN bi_prc_typ_cd = 'TPR' THEN 1 ELSE 0 END sw_tpr, CASE WHEN bi_prc_typ_cd = 'STR_OVRD' THEN 1 ELSE 0 END sw_str_ovrd, CASE WHEN bi_prc_typ_cd = 'BYR_MRDN' THEN 1 ELSE 0 END sw_byr_mrdn, CASE WHEN bi_prc_typ_cd = 'AD' THEN 1 ELSE 0 END sw_ad, CASE WHEN prtnr_disc_amt > 0 THEN 1 ELSE 0 END sw_prtnr_disc, fscl_year id_fscl_yr, fscl_quarter id_fscl_qtr, fscl_period id_fscl_prd, fscl_wk_id id_fscl_wk, dt_id ID_DATE FROM svc_hdengjb.customerorders;

-- COMMAND ----------

refresh promo_decision_support.v_orders;
