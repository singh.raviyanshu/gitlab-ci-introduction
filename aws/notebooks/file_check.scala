// Databricks notebook source
val path=dbutils.widgets.get("path")

val fmuicats=path+"global_static/FMUI-CATS.csv"
val dohclass=path+"global_static/doh-class.csv"
val mexicostores = path + "global_static/mexico_stores.csv"
val regiontodc = path + "global_static/region_to_dc.csv"
val newitem = path + "all_data/fcst_override/new_item.csv"

val scn=path+"all_data/fcst_override/scn_cd_types.csv"
val codes=new java.util.ArrayList[String]
spark.read.option("header",true).csv(scn).take(spark.read.option("header",true).csv(scn).count.toInt).foreach(x=>
  codes.add(x.getString(0))
)
val closedstores = path + "all_data/fcst_override/closed_stores.csv"
val stores = new java.util.ArrayList[String]
spark.read.option("header",true).csv(closedstores).take(spark.read.option("header",true).csv(closedstores).count.toInt).foreach(x=>
  stores.add(x.getString(0))
)

val message = "*Current configuration:  * ```"+fmuicats + ": "+spark.read.csv(fmuicats).count()+"\n"+dohclass+": "+spark.read.csv(dohclass).count()+"\n"+mexicostores+": "+ spark.read.csv(mexicostores).count()+"\n"+regiontodc+": "+spark.read.csv(regiontodc).count()+"\n"+newitem+": "+spark.read.csv(newitem).count()+"\nSCN Codes: "+codes.toString+"\nClosed Stores: "+stores.toString + "```"

val slackUrl=dbutils.widgets.get("slackUrl")
org.cognira.CommControl.setSlackUrl(slackUrl)
org.cognira.CommControl.sendSlack(message)
