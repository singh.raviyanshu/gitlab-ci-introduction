// Databricks notebook source
import com.databricks.backend.daemon.dbutils.FileInfo
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats
import scala.io.Source
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.log4j.{Level, Logger, PropertyConfigurator}
import scala.util.{Failure, Success, Try}
import com.google.gson.JsonObject
import java.time.LocalDate
import java.time.format._
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter}
import java.net.URL
import javax.net.ssl.HttpsURLConnection
import java.nio.charset.{Charset, CharsetDecoder, CodingErrorAction}
import java.nio.{ByteBuffer, CharBuffer}
import java.text.SimpleDateFormat
import java.util.Date
import org.joda.time.DateTime
import scala.collection.mutable.WrappedArray
import scala.util.matching.Regex


def sendSlack(s: String, slackUrl: String): String = {
  var response=new StringBuffer()
  Try {
    val urlobj=new URL(slackUrl)
    val con=urlobj.openConnection().asInstanceOf[HttpsURLConnection]
    con.setRequestMethod("POST")
    con.setRequestProperty("Content-Type","application/json; charset=UTF-8")
    con.setDoOutput(true)
    val wr=new OutputStreamWriter(con.getOutputStream)
    val obj=new JsonObject()
    obj.addProperty("text",s)
    wr.write(obj.toString)
    wr.flush()
    wr.close()
    val responseCode=con.getResponseCode
    val in=new BufferedReader(new InputStreamReader(con.getInputStream))
    var inputLine=""
    while(inputLine!=null){
      inputLine = in.readLine()
      response.append(inputLine)
    }
    in.close()
    con.disconnect()
    response.append(",code: " + responseCode)
  } recover {
    case e=> e.printStackTrace()
  }
  response.toString
}

/**
Runs weekly forecast incremental accumulation for PDS
 **/

//Creating a logger for the notebook
val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
private val LOGGER = Logger.getLogger("notebook."+NOTEBOOK_NAME)

val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")

val configBasePath = "dbfs:/"
val promoPath="/pfp"

/*
Reading the env from env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
  if(currentEnv.size == 0){
    LOGGER.error("Required environment not available")
    throw new Exception("Required environment not available")
  }
  else if(currentEnv.size > 1) {
    LOGGER.error("More than one environment detected")
    throw new Exception("More than one environment detected")
  }
  else currentEnv(0)
}

val storageConfigFile = configBasePath+env+promoPath+"/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))

val adlsPrefix = (storageJson\"adlsPrefix").extract[String]

val configFile=adlsPrefix+"/"+env+promoPath+"/conf/conf_fcstacc.json"
val envPath=adlsPrefix+"/"+env+promoPath+"/"

lazy val json: JValue = parse(dbutils.fs.head(configFile))

val log4jPath = (json\"log4jPath").extract[String]
Try {
  PropertyConfigurator.configure(log4jPath)
} match {
  case Success(_) => LOGGER.info(s"Using Log configuration file from dbfs location: $log4jPath for installed library")
  case Failure(_) => LOGGER.warn("No Log Configuration file found.")
}

def readDf(path: String, header: Boolean = true, sep: String = ",", format: String = "any"): DataFrame = {
  // Try to extract extension
  val extensionPattern="(.parq|.parquet|.csv|.txt|.tsv)".r
  val ext=(extensionPattern findFirstIn path).mkString
  if(ext.equals(".tsv") && sep.equals(",")) {
    println(s"Extension is ${ext} and separator is ${sep} might want to set `sep=`")
  }
  if(ext.equals(".csv") || ext.equals(".txt") || ext.equals(".tsv")) {
    spark.read.option("header",header).option("sep",sep).csv(path)
  } else if(ext.equals(".parq") || ext.equals(".parquet")) {
    spark.read.parquet(path)
  } else {
    throw new Exception("Error occured, please ensure proper arguments are present:\n readDf(path,header=true [Optional],sep=, [Optional],format=any [Optional])")
  }
}


LOGGER.info("Running Forecast Accumulation Job...WITHOUT A JAR!! :)")
val slackUrl=(json\"slackUrl").extract[String]
val userpath=(json\"userpath").extract[String]

val day = new SimpleDateFormat("E").format(new Date())
if(!day.equals("Mon")) {
  sendSlack(s":caution: FcstAccumulate being run on ${day} instead of Mon", slackUrl)
  println(s":caution: FcstAccumulate being run on ${day} instead of Mon")
}
val fileToNumericalAdd = Map("one_week_fcst"->7,"four_week_fcst"->28,"eight_week_fcst"->56)

// Init variables
val forecast_domain = readDf(userpath+"../conf/forecast_domain.csv")
val fcst_start = forecast_domain.select("fcst_start").first().getString(0)
val calendar = readDf(userpath+"merge_all/calendar.csv")
val weekly_forecast = readDf(userpath+"fcst_all/weekly_forecast.parq")

val ad_week_date = calendar.filter(col("str_id_date") === fcst_start).select("ad_week").first().getString(0)

// Check if date exists already
val fcstFiles = List("one_week_fcst","four_week_fcst","eight_week_fcst")
val datesInFiles = scala.collection.mutable.Map[String,Long]()

for(currentFcst <- fcstFiles) {
  val dft = readDf(path=(userpath+"fcst_all/"+currentFcst+".parq")).withColumn("ad_week_date",lit(ad_week_date)).withColumn("new_week",date_add(col("ad_week_date"),fileToNumericalAdd.get(currentFcst).get))
  datesInFiles += (currentFcst -> dft.where(col("week")>=col("new_week")).count)
}
var alreadyContain = List[String]()
var proceed = true
for((k,v) <- datesInFiles) {
  if(v>0) {
    alreadyContain = k :: alreadyContain
    proceed=false
  }
}
if(!proceed) {
  sendSlack(s"These files ${alreadyContain} already have ${ad_week_date}, not running FcstAccumulate", slackUrl)
  dbutils.notebook.exit("Exiting job since some files already have the current AD WEEK date.")
}


val one_week_fcst = getXWeekFcst(weekly_forecast, ad_week_date, 1)
val four_week_fcst = getXWeekFcst(weekly_forecast, ad_week_date, 4)
val eight_week_fcst = getXWeekFcst(weekly_forecast, ad_week_date, 8)
val fcstMap = Map("one_week_fcst" -> one_week_fcst, "four_week_fcst" -> four_week_fcst, "eight_week_fcst" -> eight_week_fcst)


for(currentFcst <- fcstFiles) {
  val xweek_fcst_old = readDf(path=(userpath+"fcst_all/"+currentFcst+".parq"))
  xweek_fcst_old.write.mode("Overwrite").save(userpath+"fcst_all/"+currentFcst+"_old.parq")
  val new_xweek_fcst = readDf(path=(userpath+"fcst_all/"+currentFcst+"_old.parq")).union(fcstMap.get(currentFcst).get).filter(col("week") >= date_sub(lit(ad_week_date), 104*7))
  new_xweek_fcst.write.mode("OVERWRITE").save(userpath+"fcst_all/"+currentFcst+".parq")
}

def getXWeekFcst(weekly_forecast: DataFrame, ad_week_date: String, weeks_nbr: Integer): DataFrame = {
  weekly_forecast.filter(col("week") === date_add(lit(ad_week_date), 7*weeks_nbr))
    .withColumn("week", col("week").cast(DateType))
    .select("sku", "store", "week", "baseline_forecast", "forecast", "discount_type")
}

// COMMAND ----------

// Archive forecasts
import java.text.SimpleDateFormat
import java.util.Date

val filesToArchive = List("one_week_fcst.parq","four_week_fcst.parq","eight_week_fcst.parq")
val today = new SimpleDateFormat("yyyyMMdd").format(new Date())
val fromPath = s"${envPath}all_data/fcst_all/"
val archivePath = s"${envPath}all_data/archive/$today/fcst_all/"

for (file <- filesToArchive) {
  val fcst = spark.read.parquet(s"$fromPath$file")
  fcst.write.mode("OVERWRITE").parquet(s"$archivePath$file")
}
