/**
Run Approvals Notebook
 **/
import com.databricks.backend.daemon.dbutils.FileInfo
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats
import scala.io.Source
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import java.text.SimpleDateFormat
import java.util.Date
import scala.util.matching.Regex
import org.apache.log4j.{Level, Logger, PropertyConfigurator}
import scala.util.{Success, Failure, Try}

val notebookPath = dbutils.notebook.getContext.notebookPath.get
private val NOTEBOOK_NAME = notebookPath.split("/").last
private val LOGGER = Logger.getLogger(s"notebook.$NOTEBOOK_NAME")
val pathParts = notebookPath.split("/")

/**
Set Base Path for Configuration Files
Set PromoPath 
**/ 
val configBasePath = "dbfs:/"
val promoPath = "/pfp"

/*
Reading the env from env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
if(currentEnv.size == 0){
  LOGGER.error("Required environment not available")
  throw new Exception("Required environment not available")
} else if (currentEnv.size > 1) {
  LOGGER.error("More than one environment detected")
  throw new Exception("More than one environment detected")
}
  else currentEnv(0)
}

val storageConfigFile = configBasePath+env+promoPath+"/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))

val adlsPrefix = (storageJson\"adlsPrefix").extract[String]

val configFile = adlsPrefix+"/"+env+promoPath+"/conf/conf_EOMExport_all.json"

lazy val json: JValue = parse(dbutils.fs.head(configFile))
private val log4jPath = (json\"log4jPath").extract[String]


//Set Log Properties for the Logger
Try {
  PropertyConfigurator.configure(log4jPath)
} match {
  case Success(_) => LOGGER.info(s"Using Log configuration file from dbfs location: $log4jPath for the installed library.")
  case Failure(_) => LOGGER.warn("No Log Configuration file found.")
}

var exceptionMessage = ""
var failed = false
scala.util.Try {
  LOGGER.info("Starting Run Approvals Notebook")
  spark.catalog.clearCache()
  val slackUrl=(json\"slackUrl").extract[String]
  LOGGER.info("Setting slackUrl for receiving alerts")
  org.cognira.CommControl.setSlackUrl(slackUrl)
  org.cognira.eom_exports.getApprovals.setFailWithoutHistory(true)
  
  val envpath = adlsPrefix+"/"+env+promoPath+"/all_data/"
  var today = new SimpleDateFormat("yyyyMMdd").format(new Date)

  scala.util.Try{
    // rename approvals.parq to approvals_date.parq
    LOGGER.info(s"Renaming approvals.parq files to approvals_$today.parq")

    // Call run_copy with action = "appprovals-pre" to copy approvals.parq from hebdatalake01

    dbutils.fs.mv( envpath + "fm_ui/approvals.parq", envpath + "fm_ui/approvals_"  + today + ".parq", true)

  } recover {
    case e=> LOGGER.error("Failed to find approvals.parq files, it has either been renamed or not imported")
  }

  if(dbutils.fs.ls(envpath).length<4){
  LOGGER.error(s"Missing Directories in ${envpath}")
  dbutils.notebook.exit("Some directories may be missing, please check...")
  }

  LOGGER.info(s"Reading Config File from path: $configFile")

  // Argument parsing vals
  LOGGER.debug("Parsing argument values ")
  val argsToKeys=Map("userpath"->"userpath","zone"->"zone","clean"->"clean","overwrite"->"overwrite","steps"->"steps","stage"->"stage","approvalscurrentdate"->"approvalsCurrentDate", "approvalsprevdate"->"approvalsPrevDate")

  LOGGER.debug("Mapping directories: ")
  var dirs=Map("staticdir"->"static/","mergedir"->"merge_all/","filterdir"->"merge_all/","aggdir"->"agg_all/","fcstdir"->"fcst_all/","preparerfcstdatadir"->"prepare_fcst_R_all/","fmdatadir"->"fm_ui/","eomexportsdir"->"eom_export/")
  var updatedDirs=scala.collection.mutable.Map[String,String]()

  LOGGER.info(s"Extracting values from $configFile")
  LOGGER.debug(s"Extracting userpath from $configFile")
  val userpath=(json\(argsToKeys.get("userpath").get)).extractOrElse("")

  LOGGER.debug(s"Extracting zone from $configFile")
  val zone=(json\(argsToKeys.get("zone").get)).extractOrElse("")

  LOGGER.debug(s"Extracting approvalsCurrentDate from $configFile")
  val approvalsCurrentDate=(json\(argsToKeys.get("approvalscurrentdate").get)).extractOrElse("")

  LOGGER.debug(s"Extracting approvalsPrevDate from $configFile")
  val approvalsPrevDate=(json\(argsToKeys.get("approvalsprevdate").get)).extractOrElse("")
  val filtering=argsToKeys.contains("zone") && !zone.equals("")

  LOGGER.debug(s"Extracting stage from $configFile")
  val stage = (json\(argsToKeys.get("stage").get)).extractOrElse("")

  LOGGER.debug(s"Extracting overwrite from $configFile")
  val overwrite = (json\(argsToKeys.get("overwrite").get)).extractOrElse("")

  LOGGER.debug(s"Extracting steps from $configFile")
  val steps = (json\(argsToKeys.get("steps").get)).extractOrElse("")

  if(!userpath.contains(env)){
  LOGGER.error(s"$env not found in $userpath")
  dbutils.notebook.exit(env + " not detected in "+userpath)
  }

  if(!argsToKeys.contains("userpath")){
  LOGGER.error(s"userpath not found in $configFile")
  dbutils.notebook.exit("Please set userpath in "+configFile)
  }

  LOGGER.debug("Updating directory paths: ")
  for((k,v)<-dirs){
    LOGGER.debug(s"Setting $k -> ${userpath}+$v")
    updatedDirs += (k -> (userpath+v))
  }

  LOGGER.debug("Checking argument mapping to keys contains 'zone'")
  if(filtering){
    LOGGER.info(s"Filtering on zone: $zone")
    LOGGER.debug("Updating directories according to zone: $zone")

    LOGGER.debug(s"Setting filterdir -> ${userpath}merge_zone${zone}/")
    updatedDirs += ("filterdir" -> (userpath + "merge_zone" + zone + "/"))

    LOGGER.debug(s"Setting mergedir -> ${userpath}merge_zone${zone}/")
    updatedDirs += ("mergedir" -> (userpath + "merge_zone" + zone + "/"))

    LOGGER.debug(s"Setting aggdir -> ${userpath}agg_all${zone}/")
    updatedDirs += ("aggdir" -> (userpath + "agg_all" + zone + "/"))

    LOGGER.debug(s"Setting fcstdir -> ${userpath}fcst_zone${zone}/")
    updatedDirs += ("fcstdir" -> (userpath + "fcst_zone" + zone + "/"))

    LOGGER.debug(s"Setting preparerfcstdatadir -> ${userpath}prepare_fcst_R_zone${zone}/")
    updatedDirs += ("preparerfcstdatadir" -> (userpath + "prepare_fcst_R_zone" + zone + "/"))

    LOGGER.debug(s"Setting fmdatadir -> ${userpath}fm_ui_zone${zone}/")
    updatedDirs += ("fmdatadir" -> (userpath + "fm_ui_zone" + zone + "/"))

    LOGGER.debug(s"Setting eomexporstdir -> ${userpath}eom_export_zone${zone}/")
    updatedDirs += ("eomexporstdir" -> (userpath + "eom_export_zone" + zone + "/"))
  } else {
    LOGGER.debug("Running on all arguments")
  }


  //  construct batch args
  LOGGER.info("Constructing batch arguments using updated directories")
  var finalArgs="-batchName eomExports -mergeDir ".concat(updatedDirs.get("mergedir").get).concat(" -aggDir ").concat(updatedDirs.get("aggdir").get).concat(" -fcstDir ").concat(updatedDirs.get("fcstdir").get).concat(" -prepareRFcstDataDir ").concat(updatedDirs.get("preparerfcstdatadir").get).concat(" -fmDataDir ").concat(updatedDirs.get("fmdatadir").get).concat(" -eomExportsDir ").concat(updatedDirs.get("eomexportsdir").get).concat(" -staticDir ").concat(updatedDirs.get("staticdir").get)

  LOGGER.debug("Checking argument mapping to keys contains 'zone'")
  if(filtering) {
    finalArgs=finalArgs+" -filterDir "+updatedDirs.get("filterdir").get+" -zone "+zone
  }

  LOGGER.debug("Checking argument mapping to keys contains 'stage' with a value")
  if(!stage.equals("")) {
    finalArgs= finalArgs+" -stage " + stage
  }

  LOGGER.debug("Checking argument mapping to keys contains 'overwrite'")
  if(overwrite.equals("true")) {
    finalArgs= finalArgs + " -overwrite"
  }

  LOGGER.debug("Checking argument mapping to keys contains 'steps' with a value")
  if(!steps.equals("")) {
    finalArgs= finalArgs+" -steps " + steps
  }

  LOGGER.debug("Checking argument mapping to keys contains 'approvalsCurrentDate' and 'approvalsPrevDate' with a value")
  if( !approvalsCurrentDate.equals("") && !approvalsPrevDate.equals("")) {
    finalArgs=finalArgs+" -appCurrentDate " + approvalsCurrentDate + " -appPrevDate " + approvalsPrevDate
    today = approvalsCurrentDate
  } else {
    // If no approvals date, try and determine default previous date from previous days available
    LOGGER.debug("Determining default previous date from previous days available")
    def matchesPattern(str : String, pattern: Regex) : Boolean = {
  LOGGER.debug(s"Matching pattern for $str")
      str match {
        case pattern(found) => true
        case _ => false
      }
    }
    val previousBasePattern = "^previous_base_approvals_(\\d{8}).parq/$".r
    val files = dbutils.fs.ls(envpath + "eom_export")
    val dateOptions : List[String] =files.collect{case file if matchesPattern(file.name, previousBasePattern) => {
      val name = file.name
      val parqIndex = name.indexOf(".parq")
      name.substring(parqIndex -8, parqIndex)
    }}.toList
    if(!dateOptions.isEmpty)
      finalArgs = finalArgs + " -appPrevDate " + dateOptions.max
  }

  LOGGER.debug(s"Printing final argument: $finalArgs")

  for(c <- finalArgs.split(" ")){
    LOGGER.debug(s"Printing final argument: $c")
  }


  // call main class
  LOGGER.info("Calling main class of Eom Exports")
  org.cognira.EomExports.main(finalArgs.split(" "))

  LOGGER.info("Finished approvals, generating counts")

  // Send message with action code counts and approving users
  LOGGER.debug(s"Reading ${envpath}fm_ui/approvals_$today parquet file")
  val approvals = spark.read.parquet(envpath + "fm_ui/approvals_" + today + ".parq")

  LOGGER.info("Checking for duplicates,  If they exist rename the orginal file approvals_<today>_duplicates.parq, remove the duplicates and then write out the new file under the original name.")
  for(fileName <- List("fm_ui/approvals_"+today,"eom_export/approval_changes_"+today)) {
    val approvalsDataDuplicatesCheck = spark.read.parquet(envpath + fileName + ".parq")
    val nodupApprovals = approvals.dropDuplicates()
    if (approvals.count() > nodupApprovals.count()) {
      LOGGER.info("Duplicates were found and the file renamed with _duplicates appended to the name." )
      val dupMessage = s"DUPLICATES were found in ${fileName}.parq file for ${today}. ```${approvalsDataDuplicatesCheck.count() - nodupApprovals.count()} duplicate records were found and removed. ``` "
      org.cognira.CommControl.sendSlack(dupMessage)
      approvals.write.mode("OVERWRITE").save(envpath + fileName + "_duplicates.parq")
      LOGGER.info("New de-duped file was written out.")
      nodupApprovals.write.mode("OVERWRITE").save(envpath + fileName + ".parq")
    }
  }


  LOGGER.debug("Grouping approvals by approving_user")
  val grouped = approvals.groupBy("approving_user").count
  var userString = ""
  if (!grouped.isEmpty)
    grouped.collect.foreach(x => userString += s"${x(0)}: ${x(1)}\n")
  else
    userString = "No New Approval Data Present"

  val inboundMessage = s"Inbound Approvals:\nApproving users and their approval counts for $today\n```$userString```\n"
  val fileName = "eom_export/approval_changes_" + today + ".parq"

  LOGGER.debug(s"Reading ${envpath + fileName} parquet file")
  val approval_changes = spark.read.parquet(envpath + fileName)
  val outboundMessage = s"Outbound Approvals:\nApprovals file ```${envpath + fileName}``` was generated with a total count of ${approval_changes.count}\nAction Code Counts : \n```A: ${approval_changes.filter("ACTION_CD = 'A'").count} \nC: ${approval_changes.filter("ACTION_CD = 'C'").count} \nD: ${approval_changes.filter("ACTION_CD = 'D'").count} \n```"
  val slackMessage = inboundMessage + outboundMessage

  LOGGER.info("Sending message with Approving users, Approval counts, Action code counts and Approval files count")
  org.cognira.CommControl.sendSlack(slackMessage)

  // Delete approval_changes backup file from previous day, copy today's file into approval_changes.parq so the export pipeline can pick it up
  LOGGER.info("Deleting approval_changes backup parquet file from previous day")
  val deleted = dbutils.fs.rm(envpath + "eom_export/approval_changes.parq", true)
  LOGGER.info(s"Deleted existing approval_changes.parq? $deleted")

  LOGGER.info("Copying today's file into approval_changes parquet file")
  dbutils.fs.cp(envpath + "eom_export/approval_changes_" + today + ".parq", envpath + "eom_export/approval_changes.parq", true)

  val frows=new java.util.ArrayList[Row]
  val countsParq=envpath+"eom_export/approval_counts.parq"
  case class FileInformation(path: String, name: String)
  val approvals_path = "fm_ui/approvals_" + today + ".parq"
  val approval_changes_path = "eom_export/approval_changes.parq"
  val filesToCount = List(FileInformation(envpath + approvals_path, approvals_path.split("\\/")(1)),
    FileInformation(envpath + approval_changes_path, approval_changes_path.split("\\/")(1)))
  for(file <- filesToCount){
    var nm=""
    if(file.name.contains(".")){
  LOGGER.debug(s"Updating nm with 'STG_' and ${file.name} if approval path contains '.'")
      nm="STG_"+file.name.split("\\.")(0).toUpperCase
    }else{
  LOGGER.debug(s"Updating nm with ${file.name} ")
      nm=file.name
    }
    if(file.path.contains(".parq")){
  LOGGER.info(s"Counting the number of rows in ${file.name} that contains .parq files")
      frows.add(Row(file.path,nm,spark.read.parquet(file.path).count))
    } else if(file.path.contains(".csv")) {
  LOGGER.info(s"Counting the number of rows in ${file.name} that contains .csv files")
      frows.add(Row(file.path,nm,spark.read.csv(file.path).count))
    }
  }
  val someSchema = StructType(Seq(
    StructField("source", StringType, false),
    StructField("target", StringType, false),
    StructField("count", LongType, false)
  ))

  LOGGER.debug(s"Removing file: ${countsParq}")
  dbutils.fs.rm(countsParq,true)

  LOGGER.info(s"Writing counts to: $countsParq")
  spark.createDataFrame(frows,someSchema).write.mode("OVERWRITE").format("parquet").option("header","true").save(countsParq)


  //  Check for unexpected increases in approvals data
  val fmuiapprovals = spark.read.parquet(envpath + approvals_path).withColumn("last_approved_forecast",col("last_approved_forecast").cast(DoubleType))
  val approvalchanges = spark.read.parquet(envpath + approval_changes_path)
  val aggApprovals=approvalchanges.groupBy("prod_id").sum("total_fcst").withColumnRenamed("prod_id","product_id").join(fmuiapprovals.groupBy("product_id").sum("last_approved_forecast"),Seq("product_id"),"left")
  val anomalies = aggApprovals.withColumn("mult",col("sum(total_fcst)")/col("sum(last_approved_forecast)")).where(col("mult")>1.5)
  var astr=""
  Try {
    anomalies.take(anomalies.count.toInt).foreach(x=>{
      astr=astr.concat("\n"+x.getString(0)+" increased "+x.getDouble(3)+" times from FMUI approvals to approval_changes")
    })
    astr=astr.concat("\n*Please check*")
  } recover {
    case e=>{
	  astr="Anomalies detected in today's approvals process where total_fcst in approval_changes increased greater than 1.5 times from FMUI approvals, please check."
	}
  }
  if(anomalies.count > 0) {
    org.cognira.CommControl.sendSlack(astr)
  }
} recover {
  case e => {
    LOGGER.error(s"ERROR: ${e.getMessage.replace("\n"," ")}")
    failed = true
    exceptionMessage = e.getMessage
  }
}
if (failed) {
  LOGGER.error(s"Sending $exceptionMessage message to slack")
  org.cognira.CommControl.sendSlack(("run_approvals error:  "+exceptionMessage))
  throw new Exception(exceptionMessage)
}

