import json

def readJSONFile(pt):
  with open(pt) as f:
    o = json.load(f)
  return o

notebookPath = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
pathParts = notebookPath.split("/")

config_base_path = "/dbfs/"
promo_path = "pfp"
# Reading env from env_conf.json
envConfigFile = "/dbfs/"+"env"+"/env_conf.json"
envJson = readJSONFile(envConfigFile)
environments = envJson['env']
currentEnv = [env for env in environments if env in pathParts]
if len(currentEnv) == 0:
  raise ValueError("Required environment not available")
elif len(currentEnv) > 1:
  raise ValueError("More than one environment detected")
else: env = currentEnv[0]

# Getting User and Password for the DB connection
conf = config_base_path + env + "/" + promo_path + "/conf/db_connection.json"
confJson = readJSONFile(conf)
jdbc_url = confJson['jdbcUrl']
user = dbutils.secrets.get(scope = confJson['scope'] , key = confJson['userKey'])
password = dbutils.secrets.get(scope = confJson['scope'] , key = confJson['passwordKey'])

from pyspark.sql.functions import *
import logging

format = "%(asctime)s: %(message)s"
logging.getLogger("py4j").setLevel(logging.ERROR)
logging.basicConfig(format=format, level=logging.INFO,datefmt="%H:%M:%S")

driver = "oracle.jdbc.driver.OracleDriver"
sql_qry = ["call fm.trunc_table('STG_APPROVALS')", "INSERT INTO FM.STG_APPROVALS SELECT pw.product_id, pw.week, pwd.dc_id, pw.approving_user, pw.user_approved_date, pwd.base_frcst_units, pwd.last_approved_forecast, pwd.approved_system_forecast, pwd.lead_time FROM FM.OUT_PRODDCWEEK pwd INNER JOIN FM.OUT_PRODWEEK pw ON pwd.product_id = pw.product_id AND pwd.week = pw.week AND pwd.last_approved_forecast IS NOT NULL", "call fm.trunc_table('OUT_PRODDCWEEK')", "call fm.trunc_table('OUT_PRODWEEK')"]

try:
  spark.read \
    .format("jdbc") \
    .option("url", jdbc_url) \
    .option("dbtable", "FM.OUT_PRODWEEK") \
    .option("user", user) \
    .option("password", password) \
    .option("driver", driver) \
    .load()
  con = spark._sc._gateway.jvm.java.sql.DriverManager.getConnection(jdbc_url, user, password)
except Exception as e:
  dbutils.notebook.exit(f"Connection Error with exception {e}")


def execute_qry(queries):
  try:
    for qry in queries:
      exec_statement = con.prepareCall(qry)
      exec_statement.execute()
    logging.info("Successfully inserted data into STG_APPROVALS")
  except Exception as e:
    logging.error(f"Error in executing the sql queries with exception {e}")

execute_qry(sql_qry)

con.close()
dbutils.notebook.exit("Success")