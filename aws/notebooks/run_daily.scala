// Databricks notebook source
import com.databricks.backend.daemon.dbutils.FileInfo
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.log4j.{Level, Logger, PropertyConfigurator}
import org.joda.time.DateTime
import java.time.LocalDate
import java.time.format._
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._
import org.json4s._
import org.json4s.Formats.write
import org.json4s.Formats

import scala.collection.mutable.WrappedArray
import scala.io.Source
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

/**

e131988@heb.com
  run_daily nocotebook
  prepares configuration files and copies them to respective directories
  Runs org.cognira.DailyBatch
  Performs table counts on output files
  Moves forecast_domain.csv and threshold.csv to parq files

 **/

//Creating a logger for the notebook
val NOTEBOOK_NAME = dbutils.notebook.getContext.notebookPath.mkString("").split("/").last
private val LOGGER = Logger.getLogger("notebook."+NOTEBOOK_NAME)

val notebookPath = dbutils.notebook.getContext.notebookPath.get
val pathParts = notebookPath.split("/")

/**
Set Base Path for Configuration Files
Set PromoPath 
**/ 
val configBasePath = "dbfs:/"
val promoPath="/pfp/"
val dateFormat="YYYY-MM-dd"
val fmtter = DateTimeFormatter.ofPattern(dateFormat)
val yesterday=fmtter.format(LocalDate.now().minusDays(2))
val dtarch=LocalDate.now().plusDays(1).format(DateTimeFormatter.BASIC_ISO_DATE)
val database="serve"

/*
Reading the env from  env_conf.json
*/
implicit lazy val formats=org.json4s.DefaultFormats
val envConfigFile = configBasePath+"env"+"/env_conf.json"
lazy val envJson: JValue = parse(dbutils.fs.head(envConfigFile))
val environments = (envJson\"env").extract[List[String]]
val currentEnv = for(env <- environments if pathParts.contains(env)) yield env
val env = {
if(currentEnv.size == 0){ 
  LOGGER.error("Required environment not available")
  throw new Exception("Required environment not available")
}
else if(currentEnv.size > 1) {
  LOGGER.error("More than one environment detected")
  throw new Exception("More than one environment detected")
}
else currentEnv(0)
}


/*
Read in the Storage Account Prefix configuration file and set adlsPrefix
*/
val storageConfigFile = configBasePath+env+promoPath+"/conf/conf_storage_prefix.json"
lazy val storageJson: JValue = parse(dbutils.fs.head(storageConfigFile))

val adlsPrefix = (storageJson\"adlsPrefix").extract[String]
val runCopy = (storageJson\"runCopy").extract[Boolean]
val runCopyNotebook = (storageJson\"runCopyNotebook").extract[String]

val configFile=adlsPrefix+ "/" + env + promoPath + "conf/conf_daily_all.json"
lazy val json: JValue = parse(dbutils.fs.head(configFile))

val log4jPath = (json\"log4jPath").extract[String]

Try {
  PropertyConfigurator.configure(log4jPath)
} match {
  case Success(_) => LOGGER.info(s"Using Log configuration file from dbfs location: $log4jPath for installed library")
  case Failure(_) => LOGGER.warn("No Log Configuration file found.")
}

Try {
  /* Copy AZ-PSG-PDS-App-Gen.csv and AZ-Production-FAR-PDS-Admin.csv from transport location to working location */
  val users = spark.read.csv("/mnt/promo/"+env+"/pid/AZ-PSG-PDS-App-Gen.csv")
  users.write.mode("OVERWRITE").csv(adlsPrefix+"/"+env+promoPath+"all_data/pds_ui/AZ-PSG-PDS-App-Gen.csv")

  val admins = spark.read.csv("/mnt/promo/"+env+promoPath+"pid/AZ-Production-FAR-PDS-Admin.csv")
  admins.write.mode("OVERWRITE").csv(adlsPrefix+env+promoPath+"/all_data/pds_ui/AZ-Production-FAR-PDS-Admin.csv")
} match {
  case Success(_) => LOGGER.info(s"Copied AZ-PSG-PDS-App-Gen.csv and AZ-Production-FAR-PDS-Admin.csv from transport location to working location.")
  case Failure(_) => LOGGER.warn(s"Failed to copy AZ-PSG-PDS-App-Gen.csv and AZ-Production-FAR-PDS-Admin.csv from transport location to working location.")
}

LOGGER.info("Running Daily")
var failed=false
var te=""

var c=""  //  to be used in loops
val doCopy=true
val verbose=false
val envpath=adlsPrefix + "/" + env + "/pfp"
val ind="RUN_DAILY_NOTEBOOK"
val slackUrl=(json\"slackUrl").extract[String]
LOGGER.info("Setting slackUrl for receiving alerts")
org.cognira.CommControl.setSlackUrl(slackUrl)

//  PLEASE REVIEW THESE BEFORE RUNNING
val prepareExport=true  //  does counts of tables/fmui output
val doDelete=false  //  deletes load, agg and some gen files in ADLS to prevent network errors
var finalArgs = ""
scala.util.Try {
  LOGGER.info(s"Setting $ind status to RUNNING")
  dbutils.fs.put("/"+env+promoPath+"pid/"+ind,"RUNNING",true)
 
  if (doDelete){
    //  DELETE - Add dirs here
    LOGGER.info("Removing load, agg and some gen files to avoid network errors")
    val dirsToDelete = List(envpath+"/all_data/agg_all/",envpath+"/all_data/merge_all/",envpath+"/all_data/pds_ui/")
    val filesToNotDelete = List("smmry_grp.parq/", "smmry_non_grp.parq/", "smmry_grp_couplvl.parq/", "AZ-PSG-PDS-App-Gen.csv/", "AZ-Production-FAR-PDS-Admin.csv/", "dailyInventory.parq/", "chain_weekly_price.parq/","sales_hist_dsu_table.parq/","fact_str_psa_table.parq/")
    for (dir <- dirsToDelete) {
      for(c <- dbutils.fs.ls(dir)) {
        if (!filesToNotDelete.contains(c.name)) {
          LOGGER.debug(s"Removing ${c.name}")
          dbutils.fs.rm(c.path,true)
        }
      }
    }

    val otherFilesToDelete = List("fcst_all/final_daily_forecast.parq", "fcst_all/store_hist_simple_tmp.parq", "fcst_all/StoreLevel_tmp.parq", "fcst_all/weekly_forecast_tmp1.parq", "fcst_all/weekly_forecast_tmp2.parq", "fcst_all/weekly_forecast_tmp3.parq", "fcst_all/weekly_initial_forecast.parq", "fcst_all/store_fcst.parq", "fcst_all/store_fcst_domain.parq", "fcst_all/daily_forecast.parq", "fcst_all/weekly_forecast.parq", "prepare_fcst_R_all/weekly_forecast.csv","fm_ui/location.parq","fm_ui/system_date_table.parq","fm_ui/threshold.parq","fm_ui/fm_skus_schema.parq","fm_ui/exception_inter.parq","fm_ui/sku_join.parq")

    for (filePath <- otherFilesToDelete) {
      if(verbose){
        println(s"Removing: $filePath")
      }
      LOGGER.debug(s"Removing $filePath")
      dbutils.fs.rm(envpath + "/all_data/" + filePath,true)
    }
  }


  if(dbutils.fs.ls(envpath+"/all_data/").length<4){
    LOGGER.error(s"Missing Directories in ${envpath}+/all_data/")
    dbutils.notebook.exit("Some directories may be missing, please check...")
  }

  // perform dir hierarchy checks
  LOGGER.info("Performing directory hierarchy checks.")
  val files=dbutils.fs.ls(adlsPrefix+"/"+env+promoPath)
  var f: FileInfo = null
  var f1: FileInfo = null

  LOGGER.info(s"Reading Config File from path: $configFile")

  LOGGER.debug(s"Extracting adlsPrefix pulled from from $storageConfigFile")
  // Argument parsing vals
  val argsToKeys=Map("userpath"->"userpath","database"->"database","fcstdomainpartitions"->"fcstDomainPartitions","loglevel"->"logLevel","sleeptime"->"sleepTime",
    "maxproductcount"->"maxProductCount","maxproductcountsys"->"maxProductCountsys","maxpromostartdate"->"maxPromoStartDate","maxpromoduration"->"maxPromoDuration",
    "maxpromodurationsys"->"maxPromoDurationSys","printSchemas"->"printSchemas","generateDotFiles"->"generateDotFiles","dotFilesPath"->"dotFilesPath",
    "weeksToExport"->"weeksToExport","zone"->"zone","clean"->"clean","overwrite"->"overwrite","steps"->"steps","stage"->"stage",
    "globalstaticdir"->"global_static","fcstdomain"->"forecast_domain","database"->"database", "redemptionweeks" -> "redemptionWeeks", "redemptionmode" -> "redemptionMode")
  var dirs=Map("staticdir"->"static/","mergedir"->"merge_all/","filterdir"->"merge_all/","aggdir"->"agg_all/","fcstdir"->"fcst_all/","preparerfcstdatadir"->"prepare_fcst_R_all/","fmdatadir"->"fm_ui/","pdsExportDir"->"pds_ui/","fcstoverridedir"->"fcst_override/", "loggingDir"->"logs/")
  var updatedDirs=scala.collection.mutable.Map[String,String]()

  LOGGER.debug(s"Extracting userpath from $configFile")
  val userpath=(json\argsToKeys("userpath")).extractOrElse("")

  LOGGER.debug(s"Extracting zone from $configFile")
  val zone=(json\argsToKeys("zone")).extractOrElse("")

  LOGGER.debug(s"Extracting global_static from $configFile")
  val globalStatic=(json\argsToKeys("globalstaticdir")).extractOrElse("")

  LOGGER.debug(s"Extracting forecast_domain from $configFile")
  val forecastDomain=(json\argsToKeys("fcstdomain")).extractOrElse("")

  var filtering=false

  if(!userpath.contains(env)){
    LOGGER.error(s"$env not found in $userpath")
    dbutils.notebook.exit(env + " not detected in "+userpath)
  }

  if(!argsToKeys.contains("userpath")){
    LOGGER.error(s"userpath not set in $configFile")
    dbutils.notebook.exit("Please set userpath in "+configFile)
  }

  LOGGER.info("Updating Directory paths")
  for((k,v)<-dirs){
    LOGGER.debug(s"Setting $k -> ${userpath}$v")
    updatedDirs += (k -> (userpath+v))
  }
  if(argsToKeys.contains("zone")&&zone.equals("")){
    LOGGER.info("zone contains no value. Running on full data")
    if(verbose){println("Running on full data")}
  }else if(argsToKeys.contains("zone") && !zone.equals("")){
    LOGGER.info(s"Filtering on zone = $zone")
    filtering=true

    LOGGER.info(s"Updating Directories according to zone = $zone")

    LOGGER.debug(s"Setting filterdir -> ${userpath}merge_zone${zone}/")
    updatedDirs += ("filterdir" -> (userpath+"merge_zone"+zone+"/"))

    LOGGER.debug(s"Setting aggdir -> ${userpath}agg_all${zone}/")
    updatedDirs += ("aggdir" -> (userpath+"agg_zone"+zone+"/"))

    LOGGER.debug(s"Setting fcstdir -> ${userpath}fcst_zone${zone}/")
    updatedDirs += ("fcstdir" -> (userpath+"fcst_zone"+zone+"/"))

    LOGGER.debug(s"Setting preparerfcstdatadir -> ${userpath}prepare_fcst_R_zone${zone}/")
    updatedDirs += ("preparerfcstdatadir" -> (userpath+"prepare_fcst_R_zone"+zone+"/"))

    LOGGER.debug(s"Setting fmdatadir -> ${userpath}fm_ui_zone${zone}/")
    updatedDirs += ("fmdatadir" -> (userpath+"fm_ui_zone"+zone+"/"))
    LOGGER.debug(s"Setting pdsExportDir -> ${userpath}pds_ui_zone${zone}/")
    updatedDirs += ("pdsExportDir" -> (userpath+"pds_ui_zone"+zone+"/"))
  }

  // copy static files to userpath
  LOGGER.info(s"Copying Static Files from ${globalStatic} to ${updatedDirs("staticdir")}")
  c=""
  for(c <- Array("BAD_COUPON_FIXED.csv","calendar.csv","categories_master.csv","region_to_dc.csv","threshold.csv",
    "treatment_mapping.csv","cat_partition.csv","mexico_stores.csv","valid_fmui_cat.csv","active_dept.csv",
    "dim_coup_reason.csv","important_dates.csv", "theme_dates.csv","coupon_status.csv","fm_dept_list.csv",
    "user_to_bdm.csv", "helpful_links.csv", "trial_repeat_data.csv", "promotion_cda_data.csv",
    "pds_cat_deny_list.csv", "buds_onepassids.csv", "static_user_hierarchy.csv")){
    if(verbose){println(globalStatic+c + " to " + updatedDirs("staticdir"))}
    if(doCopy){
      LOGGER.debug(s"Copying ${globalStatic}$c to ${updatedDirs("staticdir")}$c")
      dbutils.fs.rm(updatedDirs("staticdir")+c, true)
      dbutils.fs.cp(globalStatic+c,updatedDirs("staticdir")+c, true)
    }
  }
  for(c <- Array("scn_cd_types.csv", "closed_stores.csv","task_denylist.csv")){
    dbutils.fs.rm(envpath+"/all_data/fcst_override/"+c, true)
    dbutils.fs.cp(envpath+"/global_static/"+c, envpath+"/all_data/fcst_override/")
  }


  // set right dir and copy forecast domain
  var rightDir=""
  if(filtering){
    LOGGER.info(s"Setting rightDir to ${updatedDirs("filterdir")}")
    rightDir=updatedDirs("filterdir")
  }else{
    LOGGER.info(s"Setting rightDir to ${updatedDirs("filterdir")}")
    rightDir=updatedDirs("mergedir")
  }
  if(doCopy){
    LOGGER.info(s"Copying forecast domain to ${updatedDirs("filterdir")}")
    LOGGER.debug(s"Copying ${forecastDomain} to ${updatedDirs("filterdir")}")
    dbutils.fs.rm(updatedDirs("filterdir")+"forecast_domain.csv", true)
    dbutils.fs.cp(forecastDomain,updatedDirs("filterdir"), true)
  }

  //  copyStaticFilesToDirs
  LOGGER.info(s"Copying static files from ${updatedDirs("staticdir")} to rightDir: $rightDir")
  for(c <- Array("calendar.csv","treatment_mapping.csv","BAD_COUPON_FIXED.csv","region_to_dc.csv")) {
    if(verbose){println("Copying " + updatedDirs("staticdir")+c + " to "+ rightDir)}
    if(doCopy){
      LOGGER.debug(s"Copying ${updatedDirs("staticdir")}$c to ${rightDir}")
      dbutils.fs.rm(rightDir+c, true)
      dbutils.fs.cp(updatedDirs("staticdir")+c,rightDir, true)
    }
  }

  LOGGER.info(s"Copying static files from ${updatedDirs("staticdir")} to ${updatedDirs("mergedir")}")
  for(c <- Array("categories_master.csv","mexico_stores.csv","dim_coup_reason.csv")) {
    if(verbose){println("Copying " + updatedDirs("staticdir")+c + " to "+ updatedDirs("mergedir"))}
    if(doCopy){
      LOGGER.debug(s"Copying ${updatedDirs("staticdir")}$c to ${updatedDirs("mergedir")}")
      dbutils.fs.rm(updatedDirs("mergedir")+c, true)
      dbutils.fs.cp(updatedDirs("staticdir")+c,updatedDirs("mergedir"), true)
    }
  }

  LOGGER.info(s"Copying static files from ${updatedDirs("staticdir")} to ${updatedDirs("fmdatadir")}")
  for(c <- Array("threshold.csv","valid_fmui_cat.csv","fm_dept_list.csv")) {
    if(verbose){println("Copying " + updatedDirs("staticdir")+c + " to "+ updatedDirs("fmdatadir"))}
    if(doCopy){
      LOGGER.debug(s"Copying ${updatedDirs("staticdir")}${c} to ${updatedDirs("fmdatadir")}")
      dbutils.fs.rm(updatedDirs("fmdatadir")+c, true)
      dbutils.fs.cp(updatedDirs("staticdir")+c,updatedDirs("fmdatadir"), true)
    }
  }
  
  LOGGER.info(s"Copying static files from ${updatedDirs("staticdir")} to ${updatedDirs("pdsExportDir")}")
  for(c <- Array("static_user_hierarchy.csv","important_dates.csv","theme_dates.csv", "coupon_status.csv", "user_to_bdm.csv", "helpful_links.csv", "trial_repeat_data.csv", "promotion_cda_data.csv", "pds_cat_deny_list.csv", "buds_onepassids.csv")) {
    if(verbose){println("Copying " + updatedDirs("staticdir")+c + " to "+ updatedDirs("pdsExportDir"))}
    if(doCopy){
      dbutils.fs.rm(updatedDirs("pdsExportDir")+c, true)
      dbutils.fs.cp(updatedDirs("staticdir")+c,updatedDirs("pdsExportDir")+c, true)
    }
  }

  //  construct batch args
  LOGGER.info(s"Constructing batch args for DailyBatch from updatedDirs ${updatedDirs}")
  finalArgs="-batchName DailyBatch -mergeDir ".concat(updatedDirs("mergedir")).concat(" -aggDir ").concat(updatedDirs("aggdir"))
    .concat(" -fcstDir ").concat(updatedDirs("fcstdir")).concat(" -prepareRFcstDataDir ").concat(updatedDirs("preparerfcstdatadir"))
    .concat(" -fmDataDir ").concat(updatedDirs("fmdatadir")).concat(" -fcstOverrideDir ").concat(updatedDirs("fcstoverridedir"))
    .concat(" -staticDir ").concat(updatedDirs("staticdir")).concat(" -pdsExportDir ").concat(updatedDirs("pdsExportDir"))
    .concat(" -loggingDir ").concat(updatedDirs("loggingDir"))
  if(filtering) {
    LOGGER.debug(s"Adding filterDir and zone to finalArgs")
    finalArgs=finalArgs+" -filterDir "+updatedDirs("filterdir")+" -zone "+zone
  }
  if(!(json\ argsToKeys("stage")).extractOrElse("").equals("")) {
    LOGGER.debug(s"Adding stage to finalArgs")
    finalArgs=finalArgs+" -stage "+(json\ argsToKeys("stage")).extractOrElse("")
  }
  if((json\ argsToKeys("overwrite")).extractOrElse("").equals("true")) {
    LOGGER.debug("Adding overwrite to finalArgs")
    finalArgs=finalArgs+" -overwrite"
  }
  if(!(json\ argsToKeys("steps")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding steps to finalArgs")
    finalArgs=finalArgs+" -steps "+(json\ argsToKeys("steps")).extractOrElse("")
  }
  if(!(json\ argsToKeys("database")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding database to finalArgs")
    finalArgs=finalArgs+" -database "+(json\ argsToKeys("database")).extractOrElse("")+" "
  }
  if(!(json\ argsToKeys("fcstdomainpartitions")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding fcstDomainPartitions to finalArgs")
    finalArgs=finalArgs+" -fcstDomainPartitions "+(json\ argsToKeys("fcstdomainpartitions")).extractOrElse("")+" "
  }
  if(!(json\ argsToKeys("loglevel")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding logLevel to finalArgs")
    finalArgs=finalArgs+" -logLevel "+(json\ argsToKeys("logLevel")).extractOrElse("")+" "
  }
  if(!(json\ argsToKeys("sleeptime")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding sleepTime to finalArgs")
    finalArgs=finalArgs+" -sleepTime "+(json\ argsToKeys("sleeptime")).extractOrElse("")+" "
  }
  if(!(json\ argsToKeys("maxproductcount")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding maxProductCount to finalArgs")
    finalArgs=finalArgs+" -maxProductCount "+(json\ argsToKeys("maxproductcount")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("maxproductcountsys")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding maxProductCountSys to finalArgs")
    finalArgs=finalArgs+" -maxProductCountSys "+(json\ argsToKeys("maxproductcountsys")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("maxpromostartdate")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding maxPromoStartDate to finalArgs")
    finalArgs=finalArgs+" -maxPromoStartDate "+(json\ argsToKeys("maxpromostartdate")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("maxpromoduration")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding maxPromoDuration to finalArgs")
    finalArgs=finalArgs+" -maxPromoDuration "+(json\ argsToKeys("maxpromoduration")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("maxpromodurationsys")).extractOrElse("").equals("")) {
    LOGGER.debug("Adding maxPromoDurationSys to finalArgs")
    finalArgs=finalArgs+" -maxPromoDurationSys "+(json\ argsToKeys("maxpromodurationsys")).extractOrElse("")+" "
  }


  if(!(json\ argsToKeys("printSchemas")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+" -printSchemas "+(json\ argsToKeys("printSchemas")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("generateDotFiles")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+" -generateDotFiles "+(json\ argsToKeys("generateDotFiles")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("dotFilesPath")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+" -dotFilesPath "+(json\ argsToKeys("dotFilesPath")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("weeksToExport")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+"-weeksToExport "+(json\ argsToKeys("weeksToExport")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("redemptionweeks")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+"-redemptionWeeks "+(json\ argsToKeys("redemptionweeks")).extractOrElse("")+" "
  }

  if(!(json\ argsToKeys("redemptionmode")).extractOrElse("").equals("")) {
    finalArgs=finalArgs+"-redemptionMode "+(json\ argsToKeys("redemptionmode")).extractOrElse("")+" "
  }
  
  /**
	Check for missing sales in serve.sales_hist_dsu
  **/
  val salesHistoryData=spark.table(database+".sales_hist_dsu")
  val salesHistoryCount=salesHistoryData.where(col("dt_id")===yesterday).count
  if(salesHistoryCount < 100) {
    LOGGER.error(salesHistoryCount + " rows of sales history data found for " + yesterday + ".  Need to exit immediately.")
	throw new Exception(salesHistoryCount + " rows of sales history data found for " + yesterday + ".  Need to exit immediately.")
  } else {
    LOGGER.debug(salesHistoryCount + " rows of sales history detected for " + yesterday)
  }
  
  /**
	Check for null data in serve.dim_product_all
  **/
  val nullCount = spark.table(database+".dim_product_all").where(col("id_cat").isNull || col("dsc_cat").isNull || col("nm_cat_mgr").isNull).count
  val useSnapshotIfDetected = true
  if(nullCount > 0 && useSnapshotIfDetected) {
	LOGGER.warn("Using snapshot for dim_product_all")
    finalArgs = finalArgs + "-useDimProductAllSnapshot "
  }
  
  // Update Configurable Seasons date files for daily batch to read
  spark.read.option("header",true).csv(envpath+"/global_static/seasonsDates.csv").write.mode("Overwrite").format("csv").option("header",true).save(envpath+"/all_data/fcst_override/season_dates.csv")
  spark.read.option("header",true).csv(envpath+"/global_static/seasonsProducts.csv").write.mode("Overwrite").format("csv").option("header",true).save(envpath+"/all_data/fcst_override/season_products.csv")
  
  //finalArgs=finalArgs+"-maxPromoDurationSys 0 -maxProductCountSys 0 -maxPromoDuration 62 -redemptionWeeks 10 -histWeeksNum 80 -stage load,agg"

  println(finalArgs)
     for(c <- finalArgs.split(" ")){
    println(c)
  }

  // call main class
  LOGGER.info("Calling DailyBatch")
  org.cognira.DailyBatch.main(finalArgs.split(" "))

  if(prepareExport){
    val frows=new java.util.ArrayList[Row]
    val countsCsv=envpath+"/all_data/fm_ui/daily_counts.parq"

    LOGGER.info(s"Exporting parq files to ${envpath}/all_data/fm_ui")

    LOGGER.debug(s"Writing ${envpath}/all_data/fm_ui/productlist.parq to ${envpath}/all_data/fm_ui/productlistt.parq")
    spark.read.option("header","true").parquet(envpath+"/all_data/fm_ui/productlist.parq").coalesce(1).withColumnRenamed("dt_frst_recd","DT_FIRST_RECD").write.mode("OVERWRITE").save(envpath+"/all_data/fm_ui/productlistt.parq")
    LOGGER.debug(s"Removing ${envpath}/all_data/fm_ui/productlist.parq")
    dbutils.fs.rm(envpath+"/all_data/fm_ui/productlist.parq",true)
    LOGGER.debug(s"Moving ${envpath}/all_data/fm_ui/productlist.parq to ${envpath}/all_data/fm_ui/productlist.parq")
    dbutils.fs.mv(envpath+"/all_data/fm_ui/productlistt.parq",envpath+"/all_data/fm_ui/productlist.parq",true)

    LOGGER.debug(s"Exporting ${envpath}/all_data/agg_all/location.parq to ${envpath}/all_data/fm_ui/location.parq")
    spark.read.parquet(envpath+"/all_data/agg_all/location.parq").withColumnRenamed("store","key_store").coalesce(1).write.mode("OVERWRITE").format("parquet").save(envpath+"/all_data/fm_ui/location.parq")

    LOGGER.debug(s"Exporting ${updatedDirs("fmdatadir")}threshold.csv to ${envpath}/all_data/fm_ui/threshold.parq}")
    spark.read.option("header","true").csv(updatedDirs("fmdatadir")+"threshold.csv").withColumnRenamed("ExceptionType","exception_type").withColumnRenamed("ExceptionReason","exception_reason").withColumnRenamed("threshold_value","threshold_score").write.mode("OVERWRITE").format("parquet").save(envpath+"/all_data/fm_ui/threshold.parq")

    LOGGER.debug(s"Exporting ${updatedDirs("filterdir")}forecast_domain.csv to ${envpath}/all_data/fm_ui/system_date_table.parq}")
    spark.read.option("header","true").csv(updatedDirs("filterdir")+"forecast_domain.csv").withColumnRenamed("hist_end","last_load_date").withColumnRenamed("fcst_start","forecast_start_date").withColumnRenamed("fcst_duration","dummy").write.mode("OVERWRITE").format("parquet").save(envpath+"/all_data/fm_ui/system_date_table.parq")

    LOGGER.info(s"Counting tables at location ${envpath}/all_data/fm_ui/")
    for(c <- List("calendar.parq","dclist.parq","exception.parq","proddcpromo.parq","proddcweek.parq","productlist.parq","location.parq","promolist.parq","system_date_table.parq","threshold.parq")){
      val pc=envpath+"/all_data/fm_ui/"+c
      var nm=""
      if(c.contains(".")){
        LOGGER.debug(s"Appending STG_ to $c")
        nm="STG_"+c.split("\\.")(0).toUpperCase
      }else{
        nm=c
      }
      if(c.contains(".parq")){
        scala.util.Try{
          LOGGER.debug(s"Counting the number of rows in ${c}")
          frows.add(Row(pc,nm,spark.read.parquet(pc).count))
        }recover{
          case e=>{
            LOGGER.error(s"ERROR: ${e.getMessage.replace("\n"," ")}")
            frows.add(Row(pc,nm,-1.toLong))
          }
        }
      } else if(c.contains(".csv")) {
        LOGGER.debug(s"Counting the number of rows in ${c}")
        frows.add(Row(pc,nm,spark.read.csv(pc).count.toLong))
      }
    }
    val someSchema = StructType(Seq(
      StructField("source", StringType, false),
      StructField("target", StringType, false),
      StructField("count", LongType, false)
    ))
    LOGGER.debug(s"Removing file: ${countsCsv}")
    dbutils.fs.rm(countsCsv,true)
    val countsDF = spark.createDataFrame(frows,someSchema)
    LOGGER.info(s"Writing counts to ${countsCsv}")
    countsDF.write.mode("OVERWRITE").format("parquet").option("header","true").save(countsCsv)

    // Send slack message with counts of parquet files
    LOGGER.info("Sending slack message with counts of parquet files")
    val daily_map = countsDF.collect.map(row => {
      row.getString(1) -> row.getLong(2)
    }).toMap
    val order = List("STG_THRESHOLD","STG_SYSTEM_DATE_TABLE","STG_PROMOLIST","STG_LOCATION","STG_PRODUCTLIST","STG_PRODDCWEEK","STG_PRODDCPROMO","STG_EXCEPTION","STG_DCLIST","STG_CALENDAR")
    var message:String = "Daily Counts:\n```";
    for (table_name <- order) {
      message += s"$table_name = ${daily_map(table_name)} \n"
    }
    message += "```"
    org.cognira.CommControl.sendSlack(message)
  }
} recover {
  case e=> {
    val user = dbutils.notebook.getContext.tags("user")
    val userMessage = "Notebook run by " + user
    val errorMessage = "run_daily error:  "+e.getMessage+". "+userMessage
    org.cognira.CommControl.main(errorMessage.split(" "))
    failed=true
    te=e.getMessage
  }
}
if(failed){
    LOGGER.error(s"FAILED: ${te.replace("\n"," ")}")
  throw new Exception("FAILED:  "+te)
}

scala.util.Try{
	LOGGER.info("Archiving files")
	val files=List("calendar.parq","dclist.parq","exception.parq","proddcpromo.parq","proddcweek.parq","productlist.parq","location.parq","promolist.parq","system_date_table.parq","threshold.parq")
	var arc=true
	val archivePath = s"$envpath/all_data/archive/$dtarch"
	dbutils.fs.rm(archivePath,true)
	val fmUIPath = s"$envpath/all_data/fm_ui"
	val pdsPath = s"$envpath/all_data/pds_ui"
    val fcstPath = s"$envpath/all_data/fcst_all"
	LOGGER.debug(s"Making Archive folder with name $dtarch")
	dbutils.fs.mkdirs(archivePath)
	LOGGER.info(s"Archiving files to $archivePath")
	for(file <- files){
	  LOGGER.debug(s"Copying file from $fmUIPath/$file to $archivePath/$file")
	  spark.read.parquet(s"$fmUIPath/$file").write.parquet(s"$archivePath/$file")
	}
	// Copy pds_ui files as well
	val pdsFilesToCopy = List("bundled_promo_coup_metrics.parq", "calendar_pds.parq", "catWeek.parq", "category_reaction.parq",
    "curve.parq", "department_origins.parq", "discount_elasticity.parq", "forecast_domain.parq", "helpful_links.parq",
    "important_dates.parq", "md_weekly_metrics.parq", "meal_deals_metrics.parq", "price_elasticity.parq", "prodWeek.parq",
    "product_attributes.parq", "product_hierarchy.parq", "promo_attributes_mapping.parq", "promo_prod.parq", "promo_segment.parq",
    "promo_week_expense.parq", "promotion.parq", "promotion_attributes.parq", "promotion_by_discount_type.parq", "theme_dates.parq",
    "user_filters_hierarchy.parq", "user_merchandise_hierarchy.parq", "user_to_category.parq", "bdm_score_card.parq","funding_type.parq",
    "user_hierarchy.parq","account_billing.parq","thresholds.parq","location_hierarchy.parq","location_attributes.parq",
    "groups_atts_mapping.parq","groups.parq", "price_cost.parq", "prod_price.parq","purpose_data.parq","sku_demand_loader.parq",
    "coup_upc_deletion.parq","alternate_upc_list.parq","reward_demand_share.parq")
	for (pdsFile <- pdsFilesToCopy) {
	  LOGGER.debug(s"Copying file from $pdsPath/$pdsFile to $archivePath/pds_ui/$pdsFile")
	  spark.read.parquet(s"$pdsPath/$pdsFile").write.parquet(s"$archivePath/pds_ui/$pdsFile")
	}

  val fcstFiles = List("forecast_coeffs_loader.parq", "store_fcst_adjust_loader.parq","weekly_baseline_changes.parq",
    "dow_percent_changes.parq","discount_linear_fit.parq","category_scaling.parq","final_weights.parq","median_error.parq")
  for (fcstFile <- fcstFiles) {
    LOGGER.debug(s"Copying file from $fcstPath/$fcstFile to $archivePath/pds_ui/$fcstFile")
    spark.read.parquet(s"$fcstPath/$fcstFile").write.parquet(s"$archivePath/pds_ui/$fcstFile")
  }
	
	//  Snapshot serve.dim_product_all in case product hierarchy changes are being introduced to serve.dim_product_all
    if(!finalArgs.contains("useDimProductAllSnapshot")) {
      spark.table(database+".dim_product_all").write.mode("overwrite").save(archivePath+"/dim_product_all.parq")
      spark.table(database+".dim_product_all").write.mode("overwrite").save(envpath+"/all_data/fcst_override/dim_product_all.parq")
    }
} recover {
  case e=>{LOGGER.error(s"ERROR: ${e.getMessage.replace("\n"," ")}")}
}

if(runCopy) {
    LOGGER.info("Calling run_copy with action set to daily to copy files from hebdatalake02 to hebdatalake01 for expprt to Export")
    val runCopyResults=dbutils.notebook.run("run_copy", 7*24*60*60, Map("action" -> "daily", "env" -> env, "slackUrl" -> slackUrl))
    if(!runCopyResults.equals("DONE")) {
      LOGGER.error("Error Occured in run_copy - daily script, please check!")
      org.cognira.CommControl.sendSlack("ERROR Occured in run_copy - daily script, please check!")
    } else {
      LOGGER.info("Files copied over from hebdatalake02 to hebdatalake01")
    }
}

// Check for missing product to promotions data, usually indicated by a 0 fcst lift
val zeroPromoLiftAlertsSettings=(json\"zeroPromoLiftAlert")
val failureThreshold = (zeroPromoLiftAlertsSettings\"failureThreshold").extract[Int]
val warningThreshold = (zeroPromoLiftAlertsSettings\"warningThreshold").extract[Int]

val promoProdFile = spark.read.parquet(envpath+"/all_data/pds_ui/promo_prod.parq")
val promotionFile = spark.read.parquet(envpath+"/all_data/pds_ui/promotion.parq")
val promosNoLift = promoProdFile.join(promotionFile, "promo_id").where("coupon_ids is not null AND forecast_baseline_sales_units > 0 AND forecast_lift_sales_units=0 AND promo_start_date > current_date()").select("promo_id").distinct.select(collect_list("promo_id").as("promo_id")).withColumn("size",size(col("promo_id")))
var totalPromosWithZeroLift = 0L
if(promosNoLift.where(col("size") > warningThreshold).count > 0) {
  val msg=promosNoLift.first().get(0).asInstanceOf[WrappedArray[String]]
  var html="Promos Identified with 0 Promo Forecast/Missing Products, please check\n```"
  msg.foreach(i => {
    html=html+i+"\n"
    totalPromosWithZeroLift = totalPromosWithZeroLift + 1
  })
  org.cognira.CommControl.sendSlack(html+"```")
}

if(totalPromosWithZeroLift >= failureThreshold) {
  throw new Exception(totalPromosWithZeroLift + " promos detected with 0 lift!  Failing job to prevent FMUI export.")
}

LOGGER.info(s"Finishing $ind")
dbutils.fs.rm("/"+env+promoPath+"/pid/"+ind)

// COMMAND ----------

import org.json4s.jackson.JsonMethods._
import org.json4s._

// Copy to PDS
val pfpPath=s"$adlsPrefix/$env$promoPath"
val configFilePath=s"$pfpPath/conf/conf_daily_all.json"
lazy val jsonConfig: JValue = parse(dbutils.fs.head(configFilePath))
val pdsUIPath = pfpPath + "all_data/pds_ui/"
val fcstPath = pfpPath + "all_data/fcst_all/"
val pdsLoaderPath = (jsonConfig\"pdsLoaderPath").extractOrElse("")
val filesToCopy = List("bundled_promo_coup_metrics.parq", "calendar_pds.parq", "catWeek.parq", "category_reaction.parq",
  "curve.parq", "department_origins.parq", "discount_elasticity.parq", "forecast_domain.parq", "helpful_links.parq",
  "important_dates.parq", "md_weekly_metrics.parq", "meal_deals_metrics.parq", "price_elasticity.parq", "prodWeek.parq",
  "product_attributes.parq", "product_hierarchy.parq", "promo_attributes_mapping.parq", "promo_prod.parq", "promo_segment.parq",
  "promo_week_expense.parq", "promotion.parq", "promotion_attributes.parq", "promotion_by_discount_type.parq", "theme_dates.parq",
  "user_filters_hierarchy.parq", "user_merchandise_hierarchy.parq", "user_to_category.parq", "bdm_score_card.parq","funding_type.parq",
  "user_hierarchy.parq","account_billing.parq","thresholds.parq","location_hierarchy.parq","location_attributes.parq",
  "groups_atts_mapping.parq","groups.parq", "price_cost.parq", "prod_price.parq","purpose_data.parq","sku_demand_loader.parq",
  "coup_upc_deletion.parq","alternate_upc_list.parq","reward_demand_share.parq")
for (file <- filesToCopy) {
  println(s"from: ${pdsUIPath + file} to: ${pdsLoaderPath + file}")
  scala.util.Try {
    dbutils.fs.rm(s"${pdsLoaderPath}${file}",true)
  } recover {
    case e=>
      println(s"$file does not exit")
  }
  spark.read.parquet(pdsUIPath + file ).write.mode("overwrite").parquet(pdsLoaderPath + file)
}
val fcstFiles = List("forecast_coeffs_loader.parq", "store_fcst_adjust_loader.parq","weekly_baseline_changes.parq",
  "dow_percent_changes.parq", "discount_linear_fit.parq","category_scaling.parq","final_weights.parq","median_error.parq")
for (fcstFile <- fcstFiles) {
  println(s"from: ${fcstPath}${fcstFile }to: ${pdsLoaderPath}${fcstFile}")
  scala.util.Try {
    dbutils.fs.rm(s"${pdsLoaderPath}${fcstFile}",true)
  } recover {
    case e=>
      println(s"$fcstFile does not exit")
  }
  spark.read.parquet(fcstPath + fcstFile).write.mode("overwrite").parquet(pdsLoaderPath + fcstFile)
}

// COMMAND ----------


