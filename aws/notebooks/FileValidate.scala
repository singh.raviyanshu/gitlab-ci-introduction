// Databricks notebook source
package com.heb.validate

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import java.io.File
import com.databricks.dbutils_v1.DBUtilsHolder.dbutils


object FileValidation {

    def compareFiles(srcFile:String,tgtFile:String,spark: SparkSession):Unit = {
      println(srcFile)
      println(tgtFile)
      val sourceDf = buildDataFrame(srcFile,spark)
      val targetDf = buildDataFrame(tgtFile,spark)
scala.util.Try {
      if(sourceDf.count() != targetDf.count()) {
        println("Count doesn't match for %s with count:%d and %s with count:%d".format(srcFile,sourceDf.count(),tgtFile,targetDf.count()))
      } else {
        //generate row checksum
        var sha_source_Df = sourceDf.withColumn("sha2", sha2(concat_ws("__", sourceDf.columns.map(col):_*),256)).select("sha2")
        val sha_target_DF = targetDf.withColumn("sha2", sha2(concat_ws("__", targetDf.columns.map(col):_*),256)).select("sha2")
        
        println("Source Count: " + sha_source_Df.count())
        println("Target Count: " + sha_target_DF.count())

        //val diff_Df = sha_source_Df.except(sha_target_DF)
        val diff_Df = sha_source_Df.join(sha_target_DF, Seq("sha2"), "left_anti")
        if (0 != diff_Df.count()) {
          println("########################################################### File Mismatch: %s".format(srcFile))
        } else {
          println("########################################################### File Matches %s ".format(srcFile))
        }
      }
} recover {
  case e=>{println(s"ERROR: ${e.getMessage.replace("\n"," ")}")}
}

    }

    def buildDataFrame(filePath:String,spark: SparkSession):DataFrame = {

      
      var df = spark.emptyDataFrame
scala.util.Try {  
      val ext = filePath.substring(filePath.lastIndexOf(".")+1)
  
     println("Number of files: " + CheckPathExists(filePath))
  
      if(CheckPathExists(filePath) > 0) {
        if ("csv".equalsIgnoreCase(ext)) {
          df = spark.read.csv(filePath)
        } else if ("parq".equalsIgnoreCase(ext)) {
          df = spark.read.parquet(filePath)
        }
      }
      } recover {
        case e=>{println(s"ERROR: ${e.getMessage.replace("\n"," ")}")}
      }
      return df
    }

    def CheckPathExists(path:String): Int =
    {
      try  {
        val files :Seq[String] = dbutils.fs.ls(path).map(_.name)
        return files.length
      }
      catch  {
        case e : Throwable => return -1
      }
    }
}

// COMMAND ----------

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import java.io.File
import scala.util.{Failure, Success, Try}
import com.heb.validate.FileValidation._


scala.util.Try {
var app_name = "FileValidate"
val spark = SparkSession.builder().appName(app_name).enableHiveSupport().getOrCreate()
spark.sqlContext.clearCache()

      lazy val configJson: JValue = parse(dbutils.fs.head("s3://heb-spme-far-promo-stg-databricks-root/dsfar/temp/file_validate/config/compare_config.json"))
      implicit lazy val formats=org.json4s.DefaultFormats

      //ToDO:
      //Take Json config as an argument

      val fileList = (configJson\"files_to_compare").extract[List[_]]

      fileList.foreach(fileset => {
        val filesetMap:Map[String,Any] = fileset.asInstanceOf[Map[String,Any]]
        val src_dir:String = filesetMap.get("src_dir").get.asInstanceOf[String]
        val tgt_dir:String = filesetMap.get("tgt_dir").get.asInstanceOf[String]
        var files:List[String] = filesetMap.get("src_files").get.asInstanceOf[List[String]]
        
        
        if (files  != null && files.length == 1 && "*".equals(files.head)) {
          println("Step1 : " + CheckPathExists(src_dir))
          if(CheckPathExists(src_dir) > 0) {
            files = dbutils.fs.ls(src_dir).map(_.name).toList
          }

        }        

        files.foreach(file => {compareFiles(src_dir + File.separator + file,tgt_dir + File.separator + file,spark)})
      })
  
} recover {
  case e=>{println(s"ERROR: ${e.getMessage.replace("\n"," ")}")}
}

//    }
  
  


// COMMAND ----------


