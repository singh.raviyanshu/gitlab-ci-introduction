# Databricks notebook source
# MAGIC %scala
# MAGIC
# MAGIC val size = dbutils.widgets.get("size")
# MAGIC val validSizes = List("small","medium","large")
# MAGIC if (!validSizes.contains(size)) {
# MAGIC   val user = dbutils.notebook.getContext.tags("user")
# MAGIC   val errorMsg = "ERROR: Invalid category size " + size + ". Valid sizes are small, medium, and large. Notebook run by " + user + "."
# MAGIC   throw new Exception(errorMsg)
# MAGIC }

# COMMAND ----------

library(SparkR)

size <- dbutils.widgets.get("size")
env <- dbutils.widgets.get("env")
tasksFile <- dbutils.widgets.get("tasksFile")
failedTasksFile <- dbutils.widgets.get("failedTasksFile")

rPath <- paste0("/dbfs/",env,"/pfp/r_run/")
currentPFMVersion <<- list.files(paste0(rPath,"build/"),pattern="^PFM_")
pkgLocation <<- paste0(rPath,"build/",currentPFMVersion[1])
confPath <<- paste0(rPath,"conf/")
conf_file <<- paste0(confPath,"r_forecasting_conf.json")
tasksFileName <<- paste0(rPath,"conf/",tasksFile)
numexecs <<- 1:19
configFile <- jsonlite::read_json(path=conf_file)
logFileDir <- configFile$log_file
logFile <- paste0(logFileDir,size,"_",Sys.Date(),".txt")
slackUrl <<- configFile$slack_url
dataPath <<- configFile$fcst_local_path
if (size=="small") {
  #numexecs <<- 1:16
  numexecs <<- 1:300
  numuninstalls <<- 1:36
} else if (size=="medium") {
  #numexecs <<- 1:20
  numexecs <<- 1:200
  numuninstalls <<- 1:30
} else {
   #numexecs <<- 1:10
  numexecs <<- 1:100
  numuninstalls <<- 1:20
}

spark.lapply(numexecs,function(x){
  while(TRUE){
    if(!"rjson" %in% installed.packages() || !"assertive" %in% installed.packages() || !"forecast" %in% installed.packages() || !"rms" %in% installed.packages() || !"rmarkdown" %in% installed.packages() || !"VarBundle" %in% installed.packages() || !"log4r" %in% installed.packages()){
      Sys.sleep(10)
    } else {
      break
    }
  }
})

# COMMAND ----------

library(SparkR)

spark.lapply(numuninstalls,function(x){
  if("PFM" %in% installed.packages()){
    tryCatch({
               remove.packages(c("PFM"),lib="/usr/lib/R/library")
             },error=function(e){})
    tryCatch({
               remove.packages(c("PFM"))
             },error=function(e){})
    tryCatch({
               remove.packages(c("PFM"),lib="/usr/lib/R/site-library")
             },error=function(e){})
    tryCatch({
               remove.packages(c("PFM"),lib="/databricks/spark/R/lib")
             },error=function(e){})
  }
})

spark.lapply(numexecs,function(x){
 install.packages(c(pkgLocation),repos=NULL,type="source")
})

# COMMAND ----------

library(SparkR)
library(sparklyr)
library(fs)
library(data.table)
library(rjson)
sparkR.session()
sc <- spark_connect(method = "databricks")

log_to_catfile <- function(logfile,line,catnm) {
 write(line,file=paste0(logfile,"/",size,"/rforecasting_",catnm,".txt"),append=TRUE)
}

run_seq <- FALSE
run_lapply <- TRUE
cats <- c()
index <- 1

content <- read.csv(tasksFileName,header=TRUE)
cats <- as.vector(content[content$cluster==size,c('category')])
cats <- rev(cats)

category_to_filename <- function(x) {
    x=gsub(pattern = " ", replacement = "_", x)
    x=gsub(pattern = "&", replacement = "_", x)
    x=gsub(pattern = "/", replacement = "_", x)
    x=gsub(pattern = ",", replacement = "_", x)
    x=gsub(pattern = "-", replacement = "_", x)
    x=gsub(pattern = "___", replacement = "_", x)
    x=gsub(pattern = "__", replacement = "_", x)
  x
}

sendSlack <- function(msg) {
  system(paste0("curl -X POST -H 'Content-type: application/json' --data '{\"text\":\"",msg,"\"}' ",slackUrl))
}

runLApply <- function(categories) {
    slr <- spark.lapply(categories,function(x){
		##  loop through necessary packages
		pkgs <- c("rjson","assertive","forecast","zoo","rms","rmarkdown","VarBundle","log4r","VarBundle","Hmisc","logging","pacman")
		for(pkg in pkgs){
			if(!pkg %in% installed.packages()) {
				sendSlack(paste0(pkg," not installed on ",size," cluster, may want to check st
ability."))
				install.packages(pkg,repos="https://cran.microsoft.com/snapshot/2021-09-01/",dependencies=TRUE)
			} else {
				print(paste0(pkg," installed"))
			}
		}

      library(log4r)
      log4r_file <- paste0(configFile$log4r_path,category_to_filename(x),".log")
      file_logger <- logger(configFile$log_level, appenders = file_appender(log4r_file))
      debug(file_logger, paste0("Conf file:  ",configFile))
      debug(file_logger, paste0("Category:  ",size))
      debug(file_logger, paste0("Waiting for all libraries to be installed..."))
	  if(!"PFM" %in% installed.packages()){
        warn(file_logger, "PFM package found as not installed, installing.")
 		install.packages(c(pkgLocation),repos=NULL,type="source",dependencies=TRUE)
      }
	  library(assertive)
	  library(forecast)
	  library(rjson)
	  library(zoo)
      library(PFM)
      library(data.table)

      logFile <- jsonlite::read_json(path=conf_file)
      logFile <- logFile$log_file
      log_to_catfile(logFile,paste0("start,",x,",",Sys.time(),",starting"),gsub("/","-",x))
      tryCatch({
		info(file_logger,paste0("Executing exec_heb(",x,",",conf_file,")"))
                 exec_heb(cat_regex = x,
                          conf_file = conf_file,
                          create_new = TRUE,
                          save_data_load = FALSE,
                          save_thick = FALSE,
                          store_thin = TRUE,
                          download_data = FALSE)
               },error=function(e){
        log_to_catfile(logFile,paste0("error,",x,",",Sys.time(),",",e),gsub("/","-",x))
        msg <- paste0(x," failed with error:  ",e," on ",size)
        sendSlack(msg)
		error(file_logger,paste0(size,",",msg))
      })
      log_to_catfile(logFile,paste0("end,",x,",",Sys.time(),",end"),gsub("/","-",x))
    })
}


if(run_lapply){
  ## This will call the PFM package
  ## on each node within the cluster and run the weekly category processing
  ## Set run_lapply <- FALSE if you don't want to do this
  ## and just need to re-gen all_tasks_failed.txt, etc.
  runLApply(cats)
}

outputFiles <- c("DOW_profile","ForecastCoeffs","SkuDayDOHCoeffs","SkuDayWICCoeffs","SkuStoreFitError","SkuStoreVariance","StoreLevel","SubclassStoreFitError","discount_range_exception","extreme_forecast_exception","fcst_factors","first_promotion_exception","historical_baseline","new_attr_exception","new_brandNew_promo_exception","new_discount_exception","r2fitting","sku_demand","store_demand","store_fcst_adjust","store_hist_simple","store_noise","weekly_history")
filesCounts <- c()
for(c in cats) {
  filesCounts[[category_to_filename(c)]] <- 0
}

# COMMAND ----------

## Check for categories that had <23 output files and add them to all_tasks_failed.txt to be reprocessed by large
## Also check for empty files to be re-processed
emptyFiles = c()
for(i in list.files(path=dataPath,recursive=FALSE,include.dirs=FALSE,full.names=FALSE)) {
  for(fn in outputFiles) {
    if(grepl(fn, i, fixed=TRUE)) {
      nc <- strsplit(i,paste0("_",fn,".csv"))[[1]]
      tryCatch({
        filesCounts[[nc]] <- filesCounts[[nc]]+1
      },error=function(e){
        filesCounts[[nc]] <- 0
      })
    }
  }

  ## Check for empty files
  if(file_test("-f",paste0(dataPath,i))) {
    empty = (file.size(paste0(dataPath,i)) == 0L)
    if(empty) {
      emptyFiles = append(paste0(dataPath,i), emptyFiles)
    }
  }
}

## Create the failed categories dataframe
failedFrame <- NULL
for(c in cats) {
  nm <- category_to_filename(c)
  if(filesCounts[[nm]] != 23) {
    if(is.data.frame(failedFrame)) {
      failedFrame <- rbind(failedFrame,data.frame("cluster"="large","category"=c,"fc"=filesCounts[[nm]]))
    } else {
      failedFrame <- data.frame("cluster"="large","category"=c,"fc"=filesCounts[[nm]])
    }
  }
}

## Add empty files into all_tasks_failed.txt
for(i in emptyFiles) {
  sendSlack(paste0("Empty file detected: ",i))
  fn <- gsub("^.*/","",i)  ## Get the filename from the full path
  c <- fn
  for(outputFile in outputFiles) {
    if(grepl(outputFile, fn, fixed=TRUE)) {
      c <- gsub("^.*/","",strsplit(i,paste0("_",outputFile,".csv"))[[1]]) ## Get the category name from the filename
    }
  }
  for(category in cats) {
    nm <- category_to_filename(category)
    if(nm == c){
      failedFrame <- rbind(failedFrame, data.frame("cluster"="large","category" = category,"fc"=0))
    }
  }
}
if(is.data.frame(failedFrame) && nrow(failedFrame)>0) {
  if(file.exists(paste0(confPath,failedTasksFile))) {
    failedFrame <- rbind(read.csv(paste0(confPath,failedTasksFile),header=TRUE),failedFrame)
  }
  write.csv(failedFrame,paste0(confPath,failedTasksFile),row.names=FALSE)
}
if(size == "large") {
  if(file.exists(paste0(confPath,failedTasksFile))){
	print("Rerunning failed categories...")
	content <- read.csv(paste0(confPath,failedTasksFile),header=TRUE)
	runLApply(as.vector(content[content$cluster==size,c('category')]))
  }
}

# COMMAND ----------


