# Databricks notebook source
from pyspark.sql.functions import *

promoPath=dbutils.widgets.get("adlsEnv")
targetPath=dbutils.widgets.get("prepareRDataPrepPath")
dbutils.fs.mkdirs(targetPath)

subCommoditiesToSkip = spark.read.option("sep","|").csv(promoPath+"/global_static/task_denylist.csv").where(col("_c2")!="ALL").select("_c2").rdd.flatMap(lambda x:x).collect()
prod2 = spark.read.parquet(promoPath+"agg_all/product2.parq")
partitionsData = spark.read.option("header",True).option("sep",",").csv(promoPath+"../global_static/cat_partition.csv")

grouped = partitionsData.withColumnRenamed("cat_id","id_cat").join(prod2.select("id_cat","dsc_cat","dsc_sub_comm").distinct(),["id_cat"],"left").orderBy("id_cat").collect()

tgtFile = targetPath+"cat_partition_final.csv"
dbutils.fs.rm(tgtFile,True)

finalStr="cat_id|dsc_sub_comm|group\n"

def writeNewCatFile(l, tgt):
  with open("/dbfs"+tgt,"w") as myfile:
    myfile.truncate()
    myfile.write(l)

newCat=False
lastCat=grouped[0]['id_cat']
splitNum = 10 if (int(lastCat) < 100) else 0

for k in grouped:
  idCat=k['id_cat']
  dscCat=k['dsc_cat']
  subCat=k['dsc_sub_comm']
  if subCat not in subCommoditiesToSkip:
    if idCat != lastCat:
      newCat = True
      lastCat = idCat
    if newCat == False:
      finalStr=finalStr+str(idCat)+"|"+subCat+"|"+str(splitNum)+"\n"
      splitNum = splitNum+1
    if newCat == True:
      splitNum = 10 if (int(lastCat) < 100) else 0
      newCat = False
      finalStr = finalStr+str(idCat)+"|"+subCat+"|"+str(splitNum)+"\n"
      splitNum = splitNum+1

writeNewCatFile(finalStr, tgtFile)
dbutils.fs.cp(tgtFile,targetPath+"cat_partition.csv")

dbutils.notebook.exit("0")

# COMMAND ----------


