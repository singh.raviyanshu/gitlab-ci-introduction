# Databricks notebook source
import pandas as pd
import seaborn as sn
import numpy as np
import json


from datetime import timedelta
from datetime import datetime
from datetime import date
from statsmodels.tsa.seasonal import seasonal_decompose 

from pyspark.sql import functions as F
from pyspark.sql.functions import *
from pyspark.sql.types import *
from pyspark.sql import DataFrame

from typing import Iterable 
from itertools import chain

from pyspark.ml.evaluation import RegressionEvaluator
from sklearn.linear_model import LinearRegression, Lasso, LassoCV

import pyspark
from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate() 
spark

# COMMAND ----------

#*****************************************************************************************************************************
# Testing

# dbutils.notebook.run("./sku_chain_day",7*24*60*60,{"xdatetag":xdatetag,"env":env,"xpath":xpath,"adlsPrefix":adlsPrefix}

# dowDir                        = "dow/seasonality/"
# xdatetag                      = "210712" #date.today().strftime("%y%m%d") #
# env                           = "cert"  # "cert"
# prefix                        = "abfss://dsfar@hebdatalake02.dfs.core.windows.net/encdata/data/promo/promo_forecast/"
# xpath                         = prefix + env + '/all_data/fcstmetrics/PFP-METRICS/' + dowDir
# fcstPath                      = prefix + env + "/all/data/fcst_override/"

#*****************************************************************************************************************************
# Parameters 

xdatetag                      = dbutils.widgets.get("xdatetag")        # Current date (Monday TAG)  (191202
env                           = dbutils.widgets.get("env")             # dev, prod, cert
prefix                        = dbutils.widgets.get("prefix")          # s3://heb-spme-far-promo-prd-serve/PHAS1/pfp
xpath                         = dbutils.widgets.get("xpath")           # adlsPrefix + env + '/all_data/fcstmetrics/PFP-METRICS/' + dowDir
fcstPath                      = dbutils.widgets.get("fcstPath")        # prefix + env + "all/data/fcst_override/"

# ## REMOVE
# dowDir             = "dow/seasonality/"
# # xdatetag           = datetime.date.today().strftime("%y%m%d") 	
# xdatetag           = "220313"
# env                = "PHAS1"
# prefix             = "s3://heb-spme-far-promo-prd-serve/"
# xpath              = prefix + env + "/pfp/all_data/fcstmetrics/PFP-METRICS/" + dowDir
# fcstPath           = prefix + env + "/pfp/all_data/fcst_override/"

#*****************************************************************************************************************************
# Pull configuration for "user_local_path" from the file /dbfs/ + env + /pfp/r_run/conf/r_forecasting_conf.json

configpath                     = "/dbfs/" + env + "/pfp/r_run/conf/r_forecasting_conf.json"

# read config file
with open(configpath, 'r') as f:
    config = json.load(f)
userpath = config['userpath']


# Initialize Variables

basePath                      = prefix + env + "/pfp"
initfcstpath                  = basePath + "/all_data/fcst_all/"
rprep                         = userpath
all_sku_store_path            = xpath + 'all_sku_store.parq'
xdate                         = datetime.strptime(xdatetag, "%y%m%d").date() 
a, b, c                       = 16, 3, 8 # the number of weeks in current year, number of year in previous year, number of weeks for alternative forecast and ensambling
initiate_date                 = (xdate - timedelta(weeks=52+a+b+3))
final_date                    = (xdate + timedelta(weeks=26+2))
save_option                   = True
long_data                     = 's3://heb-spme-far-promo-prd-serve/PHAS1/pfp/pna/scm/sup_chain_serve/promo_forecast_coeffs_serve/'
#*****************************************************************************************************************************

# COMMAND ----------

# MAGIC %md
# MAGIC # 1. Data Preparation

# COMMAND ----------

# MAGIC %md
# MAGIC ## 1.1 Dates and sku combinations

# COMMAND ----------

def construct_sku_date(initiate_date, final_date):
  """
  **note**: to construct the data set to fill the missing dates the start date should set to 'initiate_date'
            but to just construct the forecast dates the start can be set as 'start_date'
  """
  print('--> STARTING: construct_sku_date {} TO {}'.format(str(initiate_date), str(final_date)))
  
  dates = pd.date_range(start=initiate_date, end=final_date, freq='D')
  
  sdf = spark.read.parquet(initfcstpath + 'weekly_initial_forecast.parq')\
                  .select('sku')\
                  .distinct()
  sdf = sdf.withColumn('date', F.array([F.lit(string) for string in dates]))
  sdf = sdf.select('*', explode(col('date')).alias('day')).drop('date').withColumn('day', col('day').cast(DateType()))

  return sdf

# COMMAND ----------

# MAGIC %md
# MAGIC ## 1.2 Filter daily sales, groupby SKU level, add gategories and calendar

# COMMAND ----------

def daily_sales(xpath, initiate_date, final_date,save_option): 
  """
  Calling daily sales for the specific period and add calendar and product details to it.
  """  
  print('STARTING: daily_sales')
  
  # make the needed sku-week that needed
  df_sku_date = construct_sku_date(initiate_date, final_date)
  
  print('--> reading daily sales from {} to {}'.format(initiate_date, final_date))
  # read daily sales and make the proper changes 
  df_daily_sales = spark.read.parquet(basePath + "/all_data/agg_all/dailySales.parq")\
                              .withColumnRenamed('date','day')\
                              .dropDuplicates(['sku', 'store', 'day'])\
                              .filter( (col('day') >= initiate_date) & (col('day') <= final_date) ) \
                                                              

  print('--> groupby to sku level')
  df_daily_sales = df_daily_sales.groupBy('sku','day').agg(sum(col('units')).alias('d_units'))
  # adding forecast dates to daily sales, fill the missing dates on training data
  df_daily_sales = df_daily_sales.join(df_sku_date, ['sku', 'day'], 'right') #trim
  #========================================================================================================================================
  print('--> reading product table (UPC to SKU) and join it to training data')
  # reading product table and get needed columns
  df_UPC_to_SKU = spark.read.parquet(basePath + "/all_data/agg_all/product.parq")
  df_UPC_to_SKU = df_UPC_to_SKU.withColumn('dow_subclass', F.split(col('subclass'), '_').getItem(0)).select('sku', 'id_item', 'upcs', 'description', 'subclass', 'class', 'department', 'item_class',  'dow_subclass')
  
  # join product table with training data
  df_daily_sales = df_daily_sales.join(df_UPC_to_SKU, ['sku'], how='left')
  
  #========================================================================================================================================
  print('--> add calender ')
  # read calendar for needed period
  Calendar = basePath + "/all_data/static/calendar.csv"
  df_calendar = spark.read.csv(Calendar, header='true', inferSchema='true')\
                      .withColumn('week', col('week').cast(DateType()))\
                      .withColumn('day', col('day').cast(DateType()))\
                      .filter( (col('day') >= initiate_date) & (col('day') <= final_date) )
  
  df_daily_sales = df_daily_sales.join(df_calendar, ['day'], how='left')\
                                 .withColumn('run_date', F.date_add(col('week'), -9))
  #========================================================================================================================================
  if save_option:
    print('--> saving the file')
    df_daily_sales.write.parquet(xpath + 'daily_sales.parq', mode='overwrite')
    
  else: 
    return df_daily_sales 
  
  print('DONE daily_sales')
  

# COMMAND ----------

# MAGIC %md
# MAGIC ## 1.3. Coefficients DOH, WIC

# COMMAND ----------

def integrate_doh_WIC(xpath, initiate_date, final_date, save_option):
  """
  This function adding DOH and WIC to daily sales 
  """
    
  # get the daily sales from previous function 
  if save_option:
    daily_sales(xpath, initiate_date, final_date, save_option)
    df = spark.read.parquet(xpath + 'daily_sales.parq')
  else: 
    df = daily_sales(xpath, initiate_date, final_date, save_option)
  
  print('STARTING: integrate_doh_WIC')
 #=================================================================================================================================================
 #apply Day of Holiday promo multiplier
 #=================================================================================================================================================
  print('--> getting DOH coefficients')
  # getting DOH
  df_coef = spark.read.parquet(long_data)
  df_doh = df_coef.withColumn('doh', concat(F.split(col('factor'), '_').getItem(1), lit('_'), F.split(col('factor'), '_').getItem(2)))\
                  .select('sku', 'mult', 'run_date', 'doh')\
                  .withColumnRenamed('mult', 'doh_mult')\
                  .where(col('doh').isNotNull())\
                  .dropDuplicates(['sku', 'doh', 'run_date']) #.filter(col('run_date') == '2020-08-16')

  print('--> join DOH')
  # joining DOH to daily sales
  df = df.join(df_doh, ['sku', 'doh', 'run_date'], 'left')

 #=================================================================================================================================================
 # Apply WIC coefficients (dom)
 #=================================================================================================================================================
  print('--> getting DOM (WIC) coefficients')
  # getting WIC
  df_WIC = spark.read.parquet(basePath + "/all_data/fcst_all/SkuDayWICCoeffs.parq")
  df_WIC = df_WIC.withColumnRenamed('SkuDayWICCoeffs.factor', 'factor')\
                  .withColumnRenamed('SkuDayWICCoeffs.sku', 'sku')\
                  .withColumnRenamed('SkuDayWICCoeffs.norm_seas', 'wic_norm_seas')\
                  .withColumnRenamed('SkuDayWICCoeffs.value', 'wic_value')\
                  .withColumnRenamed('SkuDayWICCoeffs.mult', 'wic_mult')
  
  #++++++++added 5/4/2021 because of change of schema++++++++++++++
  df_WIC = df_WIC.withColumnRenamed('norm_seas', 'wic_norm_seas')\
                  .withColumnRenamed('value', 'wic_value')\
                  .withColumnRenamed('mult', 'wic_mult')
  #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 

  # transform WIC
  df_WIC = df_WIC.withColumn('dom', F.split(col('factor'), '_').getItem(1).cast('integer'))\
                  .withColumn('sku', F.split(col('sku'), '_').getItem(0).cast('integer'))\
                  .select('sku', 'dom','wic_mult', 'wic_norm_seas','wic_value')

  print('--> join DOH')
  # joining WIC to daily sales
  df = df.join(df_WIC, ['sku', 'dom'], 'left')
  
  # fill the missing week values 
  # d_units_temp makes the daily sales with no inventory to zero
  from pyspark.sql import Window
  w = Window.partitionBy([col('day')])
  df = df.withColumn('week', when(col('week').isNull(), max(col('week')).over(w)).otherwise(col('week')))
  
  print('--> Final saving in progress')
  # save the output 
  df.write.parquet(xpath + 'actl_daily_doh_wic_inv.parq', mode='overwrite')  
  
  print("integrate_doh_WIC IS DONE")
  

# COMMAND ----------

# MAGIC %md
# MAGIC ## 1.4. Make unit profile

# COMMAND ----------

def decomp_sales_groupby_make_profile():
  """
  Purpose: 1. Decompose the effect of DOH, DOM from daily sales 
           2. Make actual sales distribution (profile) for sku-store-week level
  """
  
  # run to save the file
  integrate_doh_WIC(xpath, initiate_date, final_date, save_option)
  
  print('STARTING: decomp_sales_groupby_make_profile')
  # read saved file
  df = spark.read.parquet(xpath+'actl_daily_doh_wic_inv.parq')

  # correct missing sales, negative sales
  df = df.withColumn('d_units_temp', \
                      when(((col('d_units') < 0) | (col('d_units').isNull())) , 0)\
                      .otherwise(col('d_units')))
  
  
  # correcting WIC, DOH;  when they are missing they are equal to 1
  df = df.withColumn('wic_norm_seas', when(col('wic_norm_seas').isNull(), 1).otherwise(col('wic_norm_seas')))\
          .withColumn('doh_mult', when(col('doh_mult').isNull(), 1).otherwise(col('doh_mult')))
  
  # decompose the effect of Holidays and WIC from daily sales
  df = df.withColumn('unit_decom_doh_dom', col('d_units_temp')/(col('wic_norm_seas') * col('doh_mult')))

  # distribution (profile) of sales per week
  from pyspark.sql import Window
  w = Window().partitionBy(col("sku"), col('week'))
  df = df.withColumn('w_unit_decom_doh_dom', sum(col('unit_decom_doh_dom')).over(w))\
           .withColumn('unit_profile_dom_doh', \
                        when(((col('unit_decom_doh_dom') != 0)  & (col('w_unit_decom_doh_dom') != 0)), \
                             (col('unit_decom_doh_dom')/col('w_unit_decom_doh_dom')))\
                       .otherwise(lit(0)))\
            .orderBy('sku', 'day')
  
  
  print('--> save unit profiles')
  df.write.parquet(xpath + 'unit_decom_doh_wic.parq', mode='overwrite')

# COMMAND ----------

# MAGIC %md
# MAGIC ## 1.5. Consolidating all data for training

# COMMAND ----------

decomp_sales_groupby_make_profile()

# COMMAND ----------

# MAGIC %md
# MAGIC # 2. Model Training

# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.1. Remove outliers and make current year and last year profile components

# COMMAND ----------

def remove_outliers_average_profile(n,m):
  """
  This function does: 
    1. Read training data
    2. Adjust unit_decom_doh_dom after current date to null so that the average won't decline over the weeks
    3. Calculate n weeks average and remove outliers
    4. Calculate m weeks average and remove outliers and then lag it to last year
    5. Make the profile for each one
  
  n is the number of weeks for averaging
  inv if True means days without enough inventory will be excluded
  """
  
  print('STARTING: remove_outliers_average_profile')
  df = spark.read.parquet(xpath + 'unit_decom_doh_wic.parq')\
            .select('sku',
                     'week',
                     'day',
                     'd_units',
                     'dom',
                     'doh',
                     'description',
                     'subclass',
                     'class',
                     'dow_subclass',
                     'year',
                     'woy',
                     'dow',
                     'cal_year',
                     'doh_mult',
                     'wic_norm_seas',
                     'd_units_temp',
                     'unit_decom_doh_dom',
                     'w_unit_decom_doh_dom',
                     'unit_profile_dom_doh')
  
  # ---------------------------------------------------------------------------------------------------------
  # preventing profiles to decline in the feature
  # ---------------------------------------------------------------------------------------------------------
  tarin_date_cut = df.select('day', 'unit_decom_doh_dom')\
                                .where( (col('unit_decom_doh_dom').isNotNull())  &  (col('unit_decom_doh_dom')!=0)  )\
                                .agg(max('day')).toPandas().iloc[0,0]
  
  # the follwing line makes the future column to null and hence in the rolling average it doesnt decline
  df = df.withColumn('unit_decom_doh_dom', when(col('day')<=tarin_date_cut, col('unit_decom_doh_dom')))
  # ---------------------------------------------------------------------------------------------------------
  
  print('--> {} weeks current year average and removing averages outliers'.format(n))
  from pyspark.sql import Window
  days = lambda i: i * 86400 

  w = (Window()
     .partitionBy(col("sku"), col('dow'))
     .orderBy(col("day").cast("timestamp").cast("long"))
     .rangeBetween(-days(7*n),-days(1)))

  #******************NEW*****OUTLIER***********************************************

  df = df.withColumn('%25' , F.expr('percentile(unit_decom_doh_dom, array(0.25))').over(w)[0] )\
         .withColumn('%50' , F.expr('percentile(unit_decom_doh_dom, array(0.50))').over(w)[0] )\
         .withColumn('%75' , F.expr('percentile(unit_decom_doh_dom, array(0.75))').over(w)[0] )\
         .withColumn('iqr_value', col('%75')-col('%25'))\
         .withColumn('lower_bound', (col('%25') - 1.5*col('iqr_value')))\
         .withColumn('upper_bound', (col('%75') + 1.5*col('iqr_value')))

#   print("# of all counts",df.count())
  print("# of filtered items in current year", df.where((col("unit_decom_doh_dom") <  col('lower_bound')) & (col("unit_decom_doh_dom") >  col('upper_bound'))).count())
  df = df.withColumn(str(n)+'w_unit_mean', mean( df[(col("unit_decom_doh_dom") >=  col('lower_bound')) & (col("unit_decom_doh_dom") <=  col('upper_bound'))]['unit_decom_doh_dom']  ).over(w))

  #+++++ deactivate the drops and check the results for lower_bound and upper bound
  columns_to_drop = ['%25', '%50', '%75', 'iqr_value', 'lower_bound', 'upper_bound']
  df = df.drop(*columns_to_drop)
  #---------------------------------------------------------------------------------------------------------
  print('--> {} weeks last year average and removing averages outliers'.format(m))
  w2 = (Window()
         .partitionBy(col("sku"), col('dow'))
         .orderBy(col("day").cast("timestamp").cast("long"))
         .rangeBetween(-days(7*m + 1),days(7*m + 1)))

  df = df.withColumn('%25' , F.expr('percentile(unit_decom_doh_dom, array(0.25))').over(w2)[0] )\
         .withColumn('%50' , F.expr('percentile(unit_decom_doh_dom, array(0.50))').over(w2)[0] )\
         .withColumn('%75' , F.expr('percentile(unit_decom_doh_dom, array(0.75))').over(w2)[0] )\
         .withColumn('iqr_value', col('%75')-col('%25'))\
         .withColumn('lower_bound', (col('%25') - 1.5*col('iqr_value')))\
         .withColumn('upper_bound', (col('%75') + 1.5*col('iqr_value'))) 

  print("# of filtered items in last year", df.where((col("unit_decom_doh_dom") <  col('lower_bound')) & (col("unit_decom_doh_dom") >  col('upper_bound'))).count())
  df = df.withColumn('current_'+str(m)+'w_unit_mean', mean( df[(col("unit_decom_doh_dom") >=  col('lower_bound')) & (col("unit_decom_doh_dom") <=  col('upper_bound'))]['unit_decom_doh_dom']  ).over(w2))

  columns_to_drop = ['%25', '%50', '%75', 'iqr_value', 'lower_bound', 'upper_bound']
  df = df.drop(*columns_to_drop)
  #---------------------------------------------------------------------------------------------------------
  w3 = Window.partitionBy([col("sku"), col("woy"), col('dow')]).orderBy(col("year"))

  df = df.withColumn('ly_'+str(m)+'w_unit_mean',  lag(col('current_'+str(m)+'w_unit_mean'),1).over(w3) )

  print('--> Make the profile for current and last year averages')
  # Normalizing current year and last year profiles (ly_3w_prof_mean, 8w_prof_mean)
  from pyspark.sql import Window
  w4 = Window().partitionBy(col("sku"), col('week'))
  df = df.withColumn('ly_'+str(m)+'w_prof_mean', col('ly_'+str(m)+'w_unit_mean') / (sum(col('ly_'+str(m)+'w_unit_mean')).over(w4)))\
          .withColumn(str(n)+'w_prof_mean', col(str(n)+'w_unit_mean') / (sum(col(str(n)+'w_unit_mean')).over(w4)))
        
  return df

# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.2. Ensamble current and last year profiles

# COMMAND ----------

def UDF_ensamble_units(n=a,m=b):
  """
  This function does: 
    1. Run remove_outliers_average_profile to get data
    2. Using c weeks to train ensamble two profiles
    3. UDF
      3.1. The model should only generate positive coefficients without intercept
      NOTE: Since the version of sklearn is out of date (0.20.3) it is incapable of generating only positive coefficients with LinearRegression. 
            Therefore, the Lasso is used with no penalty ratio
      3.2. Use one of the profile in case it couldn't generate profiles
    4. Adjusting Christmas day, New year day, and Easter
    5. Calculate spread-to-day
    6. Normalize all
    7. Add partition_id from product2.parq
  """  
  df = remove_outliers_average_profile(n,m).orderBy('sku','day') #n=a,m=b,training_period=a
  
  print('STARTING: UDF_ensamble_units')
  tarin_date_cut = df.select('week', 'unit_decom_doh_dom')\
                                .where( (col('unit_decom_doh_dom').isNotNull())  &  (col('unit_decom_doh_dom')!=0)  )\
                                .agg(max('week')).toPandas().iloc[0,0] + timedelta(weeks=1)
  train_start_date = tarin_date_cut  - timedelta(weeks=c)
  future_change_date = tarin_date_cut  + timedelta(weeks=a-1)
  print('The ensambling is being done for the range of {} to {}'.format(train_start_date, tarin_date_cut))
  
  # using pandas udf for bucketizing
  output_schema = StructType(df.schema.fields + [StructField('final_unit_sku_day', DoubleType(), True)])

  @pandas_udf(output_schema, PandasUDFType.GROUPED_MAP)
  # pdf: pandas dataframe
  def ensamble_lyProf_cyProf(pdf):
    try:
      train_df = pdf[(pdf.week<=tarin_date_cut) & (pdf.week>=train_start_date)].fillna(0)
      X_train = train_df[['ly_'+str(b)+'w_unit_mean',str(a)+'w_unit_mean']]
      y_train = train_df['unit_decom_doh_dom']
      X = pdf[['ly_'+str(b)+'w_unit_mean',str(a)+'w_unit_mean']].fillna(0)
      y = pdf['unit_decom_doh_dom']
      
      # the sklearn version is 0.20.3 which doesnt contain positive attribute in the LinearRegression
      #lr = LinearRegression(fit_intercept=False, positive=True)
      # instead using Lasso with NO regularization
      lr = Lasso(alpha=0, fit_intercept=False, positive=True)
      lr.fit(X_train, y_train)
      pdf['final_unit_sku_day'] = lr.predict(X)
    
    except: 
      pdf['final_unit_sku_day'] = np.where(pdf[str(n)+'w_unit_mean']!=0, pdf[str(n)+'w_unit_mean'], pdf['ly_'+str(m)+'w_unit_mean'])
#       pdf['final_unit_sku_day'] = 0 
    return pdf

  print('--> runnig UDF')
  df = df.groupby('sku').apply(ensamble_lyProf_cyProf)
  
  print('--> Adjusting Christmas day, New year day, and Easter')
  # the forecast after 15 weeks into future will be only based on last year average
  df = df.withColumn('final_unit_sku_day', when(col('day')<=future_change_date, col('final_unit_sku_day')).otherwise(col('ly_'+str(m)+'w_unit_mean')))
  
  # correcting special dates to zero
  df = df.withColumn('final_unit_sku_day', when(col('doh').isin(['XMAS_0', 'EASTER_0']), 0).otherwise(col('final_unit_sku_day')))\
         .withColumn('final_unit_sku_day', when(col('doh')=='XMAS_7', 0.1*col('final_unit_sku_day') ).otherwise(col('final_unit_sku_day')))
  
  # for spread_to_day
  df = df.withColumn('spread_to_day_units', col('final_unit_sku_day') * col('doh_mult') * col('wic_norm_seas'))
  
  # normalizing 
  from pyspark.sql import Window
  w = Window().partitionBy(col("sku"), col('week'))
  df = df.withColumn('final_prof_sku_day', col('final_unit_sku_day') / sum(col('final_unit_sku_day')).over(w))\
         .withColumn('spread_to_day', col('spread_to_day_units') / sum(col('spread_to_day_units')).over(w))\
         .fillna(0, subset=['final_prof_sku_day', 'spread_to_day']) 

  #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  # Adding partition ID for RShiny
  ### Configuration
  print('--> Adding partition_id')
  prod2file = '%s/product2' % rprep
  layer = 'dailySales'

  def getdf(f, sep):
      import re
      fixname = lambda x: re.sub('^/dbfs', '', x)
      df = spark \
        .read \
        .format("csv") \
        .option("sep", sep) \
        .option("header", True) \
        .option("inferSchema", True) \
        .load("%s.csv" % fixname(f))
      return df

  prod2 = getdf(prod2file, sep='|') \
          .select(['sku', 'partition_id']) \
          .distinct()

  df = df.join(prod2, ['sku'], 'inner')
  
  df.write.parquet(xpath + 'dow_main_profile.parq', mode='overwrite')
  
  print('DONE: UDF_ensamble_units')
#   return df



# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.3. Create alternative profiles 

# COMMAND ----------

def add_alternative_profile():
  """
  This function does: 
    1. Run UDF_ensamble_units to get base profile
    2. UDF for alternative profile using seasonal attributes in statsmodels seasonal_decompose
    3. Add the profile on dow columns to main data
    4. Normalize alternative profile
  """
  
  UDF_ensamble_units()
  
  print('STARTING: add_alternative_profile')
  df = spark.read.parquet(xpath + 'dow_main_profile.parq')
  
  tarin_date_cut = df.select('week', 'unit_decom_doh_dom')\
                                .where( (col('unit_decom_doh_dom').isNotNull())  &  (col('unit_decom_doh_dom')!=0)  )\
                                .agg(max('week')).toPandas().iloc[0,0] - timedelta(weeks=1)
  end = str(tarin_date_cut)
  start = str(tarin_date_cut  - timedelta(weeks=c))

  print('The alternative profile from decomposition is in progress for the range of {} to {}'.format(start, end))
  
#   end = str(dfg.select('day').agg(max(col('day'))).collect()[0]['max(day)'])
#   start = str((datetime.strptime(end, '%Y-%m-%d') - timedelta(weeks=a)).date())

  dfg = df.where((col('week') >= start) & (col('week') <= end))

  print("--> preparing shcema")
  def udf_schema(df):
    """
    Creating the schema for Pandas UDF
    df: training data
    """

    fit_schema = [(StructField('sku',IntegerType(),True))]
    end = df.select('day').agg(max(col('day'))).collect()[0]['max(day)']
    start = str((datetime.strptime(end, '%Y-%m-%d') - timedelta(days=6)).date())

    date_list = pd.date_range(start, end).date.astype('str')

    for date in date_list:
      fit_schema.append(StructField(date, DoubleType(), True))

    # Create schema as StructType object
    fit_schema = StructType(fit_schema)
    return fit_schema
  fit_schema = udf_schema(dfg.select(col('day').cast(StringType())))
  
  from pyspark.sql.functions import pandas_udf, PandasUDFType

  # Use pandas_udf to define a Pandas UDF
  @pandas_udf(fit_schema, PandasUDFType.GROUPED_MAP)
  # Input/output are both a pandas.Series of doubles

  def pandas_plus_one(pdf):
    sku = pdf['sku'].iloc[0]
    pdf['day'] = pd.to_datetime(pdf['day'])
    pdf.set_index('day', inplace=True)
    pdf.sort_index(inplace=True)
    pdf['unit_decom_doh_dom'] = np.where(pdf['unit_decom_doh_dom']<=0, 0.1, pdf['unit_decom_doh_dom'])

    try:
      result = seasonal_decompose(pdf['unit_decom_doh_dom'],model='multiplicative')#
      return_df = pd.DataFrame(np.append(sku, result.seasonal[-7:].values)).transpose()
    except:
      return_df = pd.DataFrame(np.append(sku, [0,0,0,0,0,0,0])).transpose()

    return return_df
  
  print("--> Pandas UDF is in the run")
  df_val = dfg\
              .groupby('sku')\
              .apply(pandas_plus_one)
  
  print("--> melting the results from Pandas UDF")
  # pyspark function to melt the dataframe (prevent using Pandas)
  def melt(
        df: DataFrame, 
        id_vars: Iterable[str], value_vars: Iterable[str], 
        var_name: str="variable", value_name: str="value") -> DataFrame:
    """Convert :class:`DataFrame` from wide to long format."""

    # Create map<key: value>
    _vars_and_vals = create_map(
        list(chain.from_iterable([
            [lit(c), col(c)] for c in value_vars]
        ))
    )

    _tmp = df.select(*id_vars, explode(_vars_and_vals)) \
        .withColumnRenamed('key', var_name) \
        .withColumnRenamed('value', value_name)

    return _tmp
  
  # melting using pandas
#   df_val = spark.createDataFrame(pd.melt(df_val.toPandas(), id_vars=['sku'], value_name='prof_cy' , var_name='day')).orderBy('sku', 'day')
  # melting using pyspark
  df_val_2 = melt(df_val, id_vars=['sku'], value_vars=df_val.columns[1:], value_name='prof_cy' , var_name='day')\
                          .withColumn('day', col('day').cast(DateType()))#\
#                           .orderBy('sku', 'day')
  
  print('--> Read Calendar')
  Calendar = basePath + "/all_data/static/calendar.csv"
  df_calendar = spark.read.csv(Calendar, header='true', inferSchema='true')
  #Calendar transform
  df_calendar = df_calendar.select('day', 'dow')\
                          .withColumn('day', col('day').cast(DateType()))

  print('--> Calendar join')
  df_val_2 = df_val_2.join(df_calendar, ['day'], 'left').drop('day')
  
  print('--> join alternative profile to main dataframe')
  df = df.join(df_val_2, ['sku', 'dow'], 'left')
  
  print('--> normalize the alternative forecast')
  from pyspark.sql import Window
  w = Window().partitionBy(col("sku"), col('week'))
  df = df.withColumn('alt_prof_decomp', col('prof_cy') / sum(col('prof_cy')).over(w))\
         .fillna(0, subset=['alt_prof_decomp', 'prof_cy']) 

  
  print('DONE: add_alternative_profile')
  return df


# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.4. Compensate zero profiles

# COMMAND ----------

def compansate_zero_profiles():
  """
  This fucntion does;
    1. Rename columns to names used before
      1.1. base_profile_sku_day: for base profile before filling the zeros
      1.2. final_profile_sku_day: for base profile after filling zeros
      1.3. alt_profile_sku_day: for alternative profile (decomp)
    2. Fill zero profiles when the whole week is zero for main profile
      2.1. with alternative profiles if exists
      2.2. with average of all profiles
    3. Fill spread-to-day zero profiles same as above
    4. Normalized all since the averages could be not normal
  """
  df = add_alternative_profile()
  
  print('STARTING: compansate_zero_profiles')
  # renaming the outputs to previous schema names
  df = df.withColumnRenamed('alt_prof_decomp', 'alt_profile_sku_day')\
         .withColumnRenamed('final_prof_sku_day', 'base_profile_sku_day')

  # compansate the zero profiles by 
  # 1. replacing missing profiles with alterntive profiles
  # 2. put the average profile for those with no profile
  from pyspark.sql import Window
  w = Window.partitionBy([col('sku'),col('week')])
  w2 = Window.partitionBy([col('dow')])

  # Use alternative profile for filling zeros
  df = df.withColumn('cnt_prof',  count(when(col('base_profile_sku_day')!=0, True)).over(w) )\
         .withColumn('final_profile_sku_day', when(col('cnt_prof')==0, col('alt_profile_sku_day')).otherwise(col('base_profile_sku_day')))
  
  # Use average to fill the remaining zeros 
  df = df.withColumn('cnt_prof_2',  count(when(col('final_profile_sku_day')!=0, True)).over(w) )\
         .withColumn('final_profile_sku_day', \
                     when(col('cnt_prof_2')==0,  mean(col('alt_profile_sku_day')).over(w2))\
                     .otherwise(col('final_profile_sku_day')))
         
  # Use alt profile for missing spread-to-day
  df = df.withColumn('spread_to_day',\
                    when(col('cnt_prof')==0, col('alt_profile_sku_day')* col('doh_mult') * col('wic_norm_seas'))\
                     .otherwise(col('spread_to_day')))
  
  # Use average to fill the remaining zeros
  df = df.withColumn('spread_to_day',\
                    when(col('cnt_prof_2')==0, mean(col('spread_to_day')).over(w2))\
                     .otherwise(col('spread_to_day')))
  
  # Normalize , cnt_prof_3 should be zero
  df = df.withColumn('final_profile_sku_day', col('final_profile_sku_day')/sum(col('final_profile_sku_day')).over(w) )\
         .withColumn('spread_to_day', col('spread_to_day') / sum(col('spread_to_day')).over(w))\
         .withColumn('cnt_prof_3',  count(when(col('final_profile_sku_day')!=0, True)).over(w) )

  print('DONE: compansate_zero_profiles')
  return df

# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.5. Trim and export results

# COMMAND ----------

def dow_trim_n_export():
  """
  This function does:
    1. Save the whole training data and forecasts for validation
    2. Trim the dates to only include forecasts
    3. Select proper columns that are identical to previous schema
  """
  
  df = compansate_zero_profiles()
  
  print('STARTING: dow_trim_n_export')
  # for exporting validation set
  df = df.select('sku',
                 'dow',
                 'day',
                 'week',
                 'base_profile_sku_day',
                 'final_profile_sku_day',
                 'spread_to_day',
                 'alt_profile_sku_day',
                 'partition_id',
                 'description',
                 'd_units_temp',
                 'unit_decom_doh_dom',
                 'unit_profile_dom_doh',
                 'ly_3w_prof_mean',
                 '16w_prof_mean' )
  df = df.withColumn('run_date', lit(str(xdate)))
  
  print('--> saving validation set')
  df.write.parquet(xpath + 'dow_sku_day_valid.parq', mode='overwrite')
  # -------------------------------------------------------------------------------------
  # CORRECTING BEGIN AND END DATES
  cut_date_begin = xdate + timedelta(days=(7 - xdate.weekday())) + timedelta(days=2) - timedelta(weeks=1) # the first timedelta push it to Moday and the adding 2 days makes the start on Wednesday
  cut_date_end = df.select(max(col('week')).alias('max')).collect()[0]['max'] 
  df = df.where((col('day') >= cut_date_begin) & (col('week') < cut_date_end))
  # -------------------------------------------------------------------------------------
  print('--> FINAL saving results')
  # Final output save
  df = df.select('sku',
                 'dow',
                 'day',
                 'week',
                 'base_profile_sku_day',
                 'final_profile_sku_day',
                 'spread_to_day',
                 'alt_profile_sku_day',
                 'partition_id',
                 'run_date'
                 )
  df.write.parquet(xpath + 'dow_sku_day.parq', mode='overwrite')
  df.write.parquet(fcstPath + 'dow_sku_day.parq', mode='overwrite')

# COMMAND ----------

# MAGIC %md
# MAGIC ## 2.6. Running the model generation

# COMMAND ----------

dow_trim_n_export()

# COMMAND ----------

df = spark.read.parquet(xpath + 'dow_sku_day_valid.parq')\
          .select('sku',
                           'dow',
                           'day',
                           'week',
                           col('base_profile_sku_day').alias('Base_profile'),
                           col('final_profile_sku_day').alias('final_profile'),
                           'spread_to_day',
                           col('alt_profile_sku_day').alias('alt_profile'),
                           'partition_id',
                           'description',
                           'd_units_temp',
                           'unit_decom_doh_dom',
                           col('unit_profile_dom_doh').alias('unit_profile'),
                           'ly_3w_prof_mean',
                           '16w_prof_mean' )
display(df)

# COMMAND ----------

display(df.describe())
